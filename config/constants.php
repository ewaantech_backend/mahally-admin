<?php

return [
    'frontdisk_order_status' => [
        1 => "Order pending",
        2 => "Order confirmed",
        3 => "In Kitchen",
        4 => "Ready for pickup",
        5 => "Delivered",
        6 => "Cancel order",
    ],
    'kitchen_order_status' => [
        2 => "Order confirmed",
        3 => "In Kitchen",
        4 => "Ready for pickup",
    ],
    'transaction_type' => [
        1 => "Bank Deposit",
        2 => "Online Transfer",
    ],
    'change_order_status' => [
        2 => "MARK AS ORDER PENDING",
        3 => "MARK AS IN KITCHEN",
        4 => "MARK AS READY FOR PICKUP",
        5 => "MARK AS DELIVERED",
    ],
    'userapp_order_status' => [
        1 => "Order Pending",
        2 => "Order Accepted",
        3 => "In Kitchen",
        4 => "Ready for pickup",
        5 => "Delivered",
        6 => "Cancel order",
    ],
];
