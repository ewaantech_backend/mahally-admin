<?php

namespace App\Exports;


use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class ActiveOrderExport implements FromCollection,WithHeadings, WithMapping
{
    use Exportable;
    /**
    * @return \Illuminate\Support\Collection
    */
    public $order;

    public function collection()
    {
        $rest_id = Auth::user()->restuarants_id;
        $order_status =[2,3,4];
        $order=Order::whereIn('order_status',$order_status)->where('restaurant_id', $rest_id)->with('customer')->get();
      
        return $order;
    }

    public function headings(): array {
        return [
           "order Id",
           "Customer",
           "Customer Phone",
           "Amount",
           "Item Count",
           "Payment Method",

        ];
    }
    public function map($order): array {
        // $sub_total = $order->sub_total + ($order->sub_total * $order->vat / 100);
        $grand_total = $order->grand_total;
        return [
            $order->order_id,
            $order->customer[0]->name,
            $order->customer[0]->phone_number,
            $order->grand_total = $grand_total,
            $order->item_count,
            $order->payment_mode,
           
        ];
    }
    // public function view(): view
    // {
    //     $order=Order::where('status','delivered')->get();
    //     return view('order.excel', [
    //         'order' => $order
    //     ]);
    // }
}
