<?php

namespace App\Exports;


use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class AdminDeliveredOrderExport implements FromCollection, WithHeadings, WithMapping
{
    use Exportable;
    /**
     * @return \Illuminate\Support\Collection
     */
    public $order;

    public function collection()
    {
        $rest_id = Auth::user()->restuarants_id;
        $order_status = [5];
        $order = Order::whereIn('order_status', $order_status)->with(['restaurant' => function ($q) {
            $q->with('lang');
        }])->with('customer')->get();

        return $order;
    }

    public function headings(): array
    {
        return [
            "order Id",
            "Restaurant",
            "Customer",
            "Customer Phone",
            "Amount",
            "Item Count",
            "Payment Method",

        ];
    }
    public function map($order): array
    {
        $grand_total = $order->grand_total;
        return [
            $order->order_id,
            $order->restaurant->lang[0]->name,
            $order->customer[0]->name,
            $order->customer[0]->phone_number,
            $order->grand_total = $grand_total,
            $order->item_count,
            $order->payment_mode,

        ];
    }
}
