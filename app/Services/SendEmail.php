<?php

namespace App\Services;

use App\Mail\OrderSendEmail;

use Illuminate\Support\Facades\Mail;

class SendEmail {
    public static function generateOrderEmail($to, $name = '', $subject = '', $o_details = '', $ord_status = '') {
        $emai_send = Mail::to($to)->send(new OrderSendEmail($name, $subject, $o_details, $ord_status));
        return $emai_send;
    }
   

}
