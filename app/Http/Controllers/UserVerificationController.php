<?php

namespace App\Http\Controllers;

use App\User;
use App\Events\SendToken;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class UserVerificationController extends Controller
{
    public function resetUserPassword(Request $request)
    {
        $email = encrypt($request->email);
        $user_email = User::where('email', $request->email)->first();
        if ($user_email) {
            $var = '123456';
            $user = User::where('email', $request->email)->update([
                'verify_token' => $var,
            ]);

            // event(new SendToken($request->email, $var));
            Session::flash('success', 'Verification token send successfully.');
            return view('auth.passwords.reset2', compact('email'));
        } else

            return Redirect::back()->withErrors(["We can't find a user with that e-mail address"]);
    }

    public function verify(Request $request)
    {
        // dd($request);

        $email1 = decrypt($request->email);
        $email = $request->email;

        $user = User::where('email', $email1)->where('verify_token', $request->code)->first();

        if ($user) {
            Session::flash('success', 'Token Verified Successfully.');
            return view('auth.passwords.reset', compact('email'));
        } else {
            Session::flash('error', 'Enter valid token');
            return view('auth.passwords.reset2', compact('email'));
        }
    }
    public function passwordUpdating(Request $request)
    {
        $email = decrypt($request->email);
        $user = User::where('email', $email)->update(
            [
                'password' => Hash::make($request->password),
            ]
        );
        Session::flash('success', 'Password reset successfully');
        return redirect()->route('login');
    }
    public function logout()
    {
       if(Auth::user()->role_id == 2) {
        Auth::logout();
        Session::flash('success', 'Logout successfully.');
        return redirect('/salesagent/login');
       }else if(Auth::user()->role_id == 3) {
        Auth::logout();
        Session::flash('success', 'Logout successfully.');
        return redirect('/owner/login');
       }else{
         Auth::logout();
        Session::flash('success', 'Logout successfully.');
        return redirect()->route('login');  
       }
    }
}
