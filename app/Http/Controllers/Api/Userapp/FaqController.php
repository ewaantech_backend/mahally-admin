<?php

namespace App\Http\Controllers\Api\Userapp;

use App\Models\Faqs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Traits\FormatLanguage;

class FaqController extends Controller
{
    use FormatLanguage ;

    protected $faq;

    public function __construct(Faqs $faq)
    {
        $this->faq = $faq ;
    }
    public function get()
    {
        $faq = $this->faq
            ->with('lang:id,faq_id,question,answer,language')
            ->where('available_for','user')
            ->get(['id']);
        $this->keyByNested($faq, 'lang', 'language');

        return new SuccessWithData($faq);
    }
}
