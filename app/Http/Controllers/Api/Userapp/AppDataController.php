<?php

namespace App\Http\Controllers\Api\Userapp;

use stdClass;
use App\Models\Pages;
use App\Models\HowtoUse;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use Illuminate\Support\Facades\Storage;

class AppDataController extends Controller
{
    use FormatLanguage ;

    protected $pages ;

    const PRIVACY_POLICY = 2;
    const TERMS_AND_CONDITION = 3;
    const ABOUT_AS = 1;
       
    public function __construct(Pages $pages)
    {
        $this->pages = $pages;
    }
    public function privacy()
    {
        $privacy = $this->getAppData(static::PRIVACY_POLICY);
        return new SuccessWithData($privacy);
    }
    public function terms()
    {
        $terms = $this->getAppData(static::TERMS_AND_CONDITION);
        return new SuccessWithData($terms);
    }
    public function aboutas()
    {
        $about = $this->getAppData(static::ABOUT_AS);
        return new SuccessWithData($about);
    }
    private function getAppData($key)
    {
        $data = $this->pages
            ->with('lang:page_id,title,content,language')
            ->where('type', $key)
            ->where('available_for','user')
            ->select('id')
            ->first();

        if ($data) {
            $data->lang = $data->lang->map(function ($lang) {
                $lang->content = html_entity_decode($lang->content);
                // $lang->content = strip_tags($lang->content);
                return $lang;
            });
            
            $this->keyBy($data, 'lang', 'language');
        }

        return $data ?? new stdClass;
    }

    public function getUseAppData()
    {
        $useapp = HowtoUse::with('lang:id,how_to_id,title,content,language,screen_shot')
        
        ->where('status','active')
        ->where('available_for','user')
        ->get();
        $useapp->each(function($useapp){
            $useapp->lang[0]->screen_shot =  $useapp->lang[0]->screen_shot ? asset(Storage::url($useapp->lang[0]->screen_shot)) : null ;
            $useapp->lang[1]->screen_shot =  $useapp->lang[1]->screen_shot ? asset(Storage::url($useapp->lang[1]->screen_shot)) : null ;
            $useapp->lang[0]->content = $useapp->lang[0]->content ? strip_tags(htmlspecialchars_decode($useapp->lang[0]->content)):null;
            $useapp->lang[1]->content = $useapp->lang[1]->content ? strip_tags(htmlspecialchars_decode($useapp->lang[1]->content)):null;
            $this->keyBy($useapp,'lang', 'language');
        });
        

        return new SuccessWithData($useapp);
    }
}
