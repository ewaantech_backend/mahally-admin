<?php

namespace App\Http\Controllers\Api\Userapp;

use Carbon\Carbon;
use App\Models\Reviews;
use App\Models\FreeMeals;
use App\Models\MenuItems;
use App\Models\OrderItems;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Models\OfferLoyality;
use App\Models\ReviewSegments;
use App\Traits\FormatLanguage;
use App\Models\ProductLoyality;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Http\Requests\Api\Userapp\RestaurentDetailsRequest;

class RestaurentDetailsController extends Controller
{
    use FormatLanguage;

    protected $restaurent;

    public function __construct(Restaurant $restaurent)
    {
        $this->restaurent = $restaurent;
    }

    public function details($id, RestaurentDetailsRequest $req)
    {
        $latitude = $req->latitude ?? 24.7136;
        $longitude = $req->longitude ?? 46.6753;
        $details = $this->restaurent->where('id', $id)
            ->select('restuarants.id', 'restuarants.logo', 'restuarants.cover_photo', 'restuarants.location', 'restuarants.latitude', 'restuarants.longitude','restuarants.opening_time','restuarants.closing_time')
            ->with('lang:id,name,language,restuarants_id,description')
            // ->with('contact:id,restuarants_id,contact_person,designation,')
            ->with('restaurant_images:id,restuarants_id,image_path')
            ->with(['type' => function ($q) {
                $q->where('type', 'restaurant_type')
                    // ->where('status', 'active')
                    ->withPivot(["restuarants_id",
                    'restuarant_type_id','deleted_at'])
                   ->where('restuarants_restuarant_type.deleted_at', null)
                    ->with('lang:id,autosuggest_id,language,name');
            }])
            ->with(['restaurent_terms' => function ($q) {
                $q->where('type', 'terms')
                    // ->where('status', 'active')
                    ->withPivot(['restuarants_id', 'terms_id','deleted_at'])
                    ->where('restuarants_terms.deleted_at', null)
                    ->with('lang:id,autosuggest_id,language,name');
            }])
            ->with(['restaurent_facilities' => function ($q) {
                $q->where('type', 'facility')
                ->withPivot(['restuarants_id', 'facilities_id','deleted_at'])
                   ->where('restuarants_facilities.deleted_at', null)
                    // ->where('status', 'active')
                    ->with('lang:id,autosuggest_id,language,name');
            }])
            ->with(['offers' => function ($q) {
                $q->with('lang:id,offer_id,image_path,language,name')
                ->with('items')
                ->where('status','active')
                ->whereDate('expire_in', '>=', Carbon::now());
            }])

            ->where('status', 'active')
            ->selectRaw(
                'ROUND( 6371 * acos( cos( radians(?) ) *
                    cos( radians( latitude ) )
                    * cos( radians( longitude ) - radians(?)
                    ) + sin( radians(?) ) *
                    sin( radians( latitude ) ) )
                    ,2) AS distance',
                [$latitude, $longitude, $latitude]
            )
            ->firstOrFail();
            $details->restaurent_terms->each(function($terms){
                $terms->image_path = $terms->image_path ? asset('/uploads/autosuggest/'.$terms->image_path) : null;

            });
            $details->restaurent_facilities->each(function($facilities){
                $facilities->image_path = $facilities->image_path ? asset('/uploads/autosuggest/'.$facilities->image_path) : null;

            });
                // $time_open =Carbon::parse($details->opening_time)->format('H:i:s') ;
                // $current_time = Carbon::now()->format('H:i:s');
                // $time_close =Carbon::parse($details->closing_time)->format('H:i:s') ;
                // $details->closing_time = $time_close;
                // $details->opening_time =$time_open ;
                $time_open =optional($details->opening_time)->format('H:i:s');
        
        $current_time = Carbon::now()->timezone('Asia/Riyadh')->format('H:i:s');
                $time_close =optional($details->closing_time)->format('H:i:s');
        
                $details->closing_time = $time_close;
                $details->opening_time =$time_open ;
                if($current_time >= $time_open && $current_time < $time_close)
                {
                    $diff_min = Carbon::parse($details->closing_time)->diffInMinutes(Carbon::now()) ;
                    if( $diff_min <= 30 ){

                        $details->time_status = 'Closing in '.$diff_min .' minutes';
                    }else{
                        $details->time_status = 'OPEN';
                    }
                   
                }else{
                    $details->time_status = 'CLOSED';
                }
                
            [$rating, $reviewCount] = $this->getRestaurantRating($id);

            $segments = ReviewSegments::where('reviews.restaurant_id', $id)
                ->leftjoin('reviews', 'review_segment.review_id', '=', 'reviews.id')
                ->selectRaw('ROUND(AVG(review_segment.rating), 1) as rating')
                ->select('review_segment.segment_id','reviews.id as review_id','reviews.restaurant_id as restaurant_id','review_segment.segment_id as seg_id')
                ->with(['segment' => function ($query) {
                    $query->with('lang:name,language,autosuggest_id')
                        ->select('id');
                }])
                ->get()
                ->groupBy('seg_id');
            
              
        $menuItems = MenuItems::
                where('status','active')
                ->where('restuarants_id',$id)
            ->with('lang')
            // ->with('timing')
            ->with(['available_time' => function($q){
                $q ->with('time');
            }])
            ->with(['menu_category' => function ($q) {
                $q->with('lang');
            }])
            ->with(['item_addons'=>function($q){
                $q->with('lang:id,name,language,addon_id')
               ->withPivot(['menu_item_id', 'addon_id','deleted_at'])
               ->where('menu_items_addons.deleted_at', null);
                    // ->where('status','active');
            }])
           
            ->get()
            ->groupBy('menu_cate_id');
        $details->logo =  $details->logo ? asset('uploads/' . $details->logo) : null;
        $details->cover_photo =  $details->logo ? asset('uploads/' . $details->cover_photo) : null;
        $details->restaurant_images && $details->restaurant_images->each(function ($pic) {
            $pic->image_path = $pic->image_path ?  asset('uploads/' . $pic->image_path) : '';
        });
      
        $details->offers && $details->offers->each(function ($pic) {
            $pic->lang[0]->image_path = $pic->lang[0]->image_path ?  asset('uploads/restaurant/offer_management/en/' . $pic->lang[0]->image_path) : null;
            $pic->lang[1]->image_path = $pic->lang[1]->image_path ?  asset('uploads/restaurant/offer_management/ar,' . $pic->lang[1]->image_path) : null;
            $subscription_end = new Carbon($pic->expire_in);
            $days = $subscription_end->diffInDays(Carbon::now());
            $pic->remaining_days = $days + 1;
        });
       
        $menuItems && $menuItems->each(function ($pic) {
            $pic->map(function ($data) {
                $data->cover_photo = $data->cover_photo ? asset('uploads/' . $data->cover_photo) : null;
            });
            // dd($pic[0]->item_addons);
            $pic[0]->item_addons->each(function ($item_ingre){
                if($item_ingre->type == 'By Restaurant')
                {
                    $item_ingre->image_path  = $item_ingre->image_path ? asset('/uploads/restaurant/addons/'.$item_ingre->image_path) : null;

                }else{
                    $item_ingre->image_path  = $item_ingre->image_path ? asset('/uploads/autosuggest/'.$item_ingre->image_path) : null;
                }
               
              
           });
        });
        $meals = FreeMeals::with('lang')
                    ->with(['item'=>function($q){
                        $q->with('lang');
                    }])
                ->whereDate('expire_in','>=',Carbon::now())
                ->where('restuarants_id',$id)
                ->get();
                $meals->map(function ($data){
                    $data->item->cover_photo = $data->item->cover_photo ? asset('uploads/' . $data->item->cover_photo) : null;
                });
        $meals->each(function($meal) use($req){
           
            if($req->cust_id){
                // $already_done = OrderItems::join('orders','orders.id','=','order_items.order_id')
                // ->where('orders.customer_id',$req->cust_id)
                // ->where('orders.restaurant_id',$meal->restuarants_id)
                // ->where('order_items.item_id',$meal->menu_item_id)
                // ->count();
                // $remain = $meal->order - $already_done ;
                // $meal->remain = $remain ;
                // $meal->already_done = $already_done;
                // $meal->remaining_orders = $remain > 0 ? trans('userapp-messages.restaurant.loyality_meal',[
                //     'remaining'=>$remain
                // ]) : null ;
                $val_loya =
                ProductLoyality::where([
                    'customer_id' =>$req->cust_id,
                    'restaurant_id'=>$meal->restuarants_id,
                    'product_id'=>$meal->menu_item_id
                ])
                ->first();
                $already_done = $val_loya ? $val_loya->product_count : 0 ;
                $laoyality_offer = OfferLoyality::where([
                  'product_id'=>$meal->menu_item_id,
                  'offer_id'=>$meal->id,
                  'customer_id' =>$req->cust_id,
              ])->first();
              //    OrderItems::join('orders','orders.id','=','order_items.order_id')
              //     ->where('orders.customer_id',$req->cust_id)
              //     ->where('orders.restaurant_id',$meal->restuarants_id)
              //     ->where('order_items.item_id',$meal->menu_item_id)
              //     ->count();
                  
                  // $meal->order_alrady_done = ($already_done <= $meal->order ) ? $already_done : "completed ";
                  $offer = (int)($already_done / $meal->order) ;
                 
                  if($laoyality_offer){
                      $taken =  $laoyality_offer->loyality_taken ?? 0 ;
                      $laoyality_offer->update([
                          'loyality_count_available'=>  $offer - $taken,
                      ]);
                  }else{
                      DB::table('offer_loyality')->insert([
                          'product_id'=>$meal->menu_item_id,
                          'offer_id'=>$meal->id,
                          'customer_id' =>$req->cust_id,
                          'loyality_count_available'=> $offer ?? 0
                      ]);
                  }
                  
                  $remain = $already_done % $meal->order ;
                  // $meal->order - $already_done ;
                  $meal->remain = $remain ;
                  $meal->already_done = $already_done;
                  $alreadt_take_loyality = $laoyality_offer ? $laoyality_offer->loyality_taken : 0 ;
                  $already_available = $laoyality_offer ? $laoyality_offer->loyality_count_available : 0 ;
                  $meal->no_offer_available =  $already_available ;
                  $meal->remaining_orders = ($remain > 0  &&  $remain <= $meal->order )? trans('userapp-messages.restaurant.loyality_meal',[
                      'remaining'=>$meal->order - $remain
                  ]) : null ;
              
            }
        
        });
        $this->keyBy($details, 'lang', 'language');
        $this->keyByNested($details->restaurent_terms, 'lang', 'language');
        
        $details->rating = round($rating, 1);
        $details->rated_count = $reviewCount->count;
        $details->segments = $segments;
        $details->meals = $meals ;

        return new SuccessWithData([
            'details' => $details,
            'menuitems' => $menuItems
        ]);
    }

    private function getRestaurantRating($restaurantId)
    {
        $rating = Reviews::where('restaurant_id', $restaurantId)
            ->avg('rating');

        $reviewCount = Reviews::where('restaurant_id', $restaurantId)
            ->selectRaw('COUNT(DISTINCT customer_id) as count')
            ->first();

        return [$rating, $reviewCount];
    }
}
