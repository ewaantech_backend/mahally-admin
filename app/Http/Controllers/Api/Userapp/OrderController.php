<?php

namespace App\Http\Controllers\Api\Userapp;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Reviews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use App\Http\Responses\SuccessWithData;

class OrderController extends Controller
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order ;
    }
    public function orderList()
    {
        $order = $this->order
                ->with(['restaurant'=>function($q){
                    $q->with('lang:name,id,restuarants_id');
                }])
                
                ->orderBy('updated_at','desc')
                ->where('customer_id',auth()->user()->id)
                ->whereIn('order_status',[6,5])
                ->paginate(10);
        $order->map(function($restaurant){
            $restaurant->order_date = Carbon::parse($restaurant->created_at)->formatLocalized('%d %b %Y');
            $restaurant->restaurant->logos = $restaurant->restaurant->logo ? asset('/uploads/'.$restaurant->restaurant->logo) : null;
        });
        return new SuccessWithData($order);
    }
    public function activeOrderList(Request $req)
    {
        $latitude = $req->latitude ?? 24.7136;
        $longitude = $req->longitude ?? 46.6753;
        $order = $this->order
                ->with(['restaurant'=>function($q){
                    $q->with('lang:name,id,restuarants_id');
                }])
                
                ->orderBy('updated_at','desc')
                ->where('customer_id',auth()->user()->id)
                ->whereNotIn('order_status',[6,5])
                ->get();

        $order->each(function($restaurant) use($latitude ,$longitude){
            $restaurant->order_status_id = $restaurant->order_status ;
            $ord_status = Config::get('constants.userapp_order_status');
            $stat = $ord_status[$restaurant->order_status] ;
            $restaurant->order_status = trans("userapp-messages.order_status.{$stat}");
            $restaurant->order_date = Carbon::parse($restaurant->created_at)->formatLocalized('%d %b %Y');
            $restaurant->restaurant->logos = $restaurant->restaurant->logo ? asset('/uploads/'. $restaurant->restaurant->logo) : null ;
            $restaurant->distance = $this->distance($latitude ,$longitude,$restaurant->restaurant->latitude,$restaurant->longitude) ;
        });
        return new SuccessWithData($order);
    }
    protected function distance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
          return 0;
        }
        else {
          $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
            return round(($miles * 1.609344),2);
        }
      }
    public function orderDetails($id)
    {
        $details = $this->order
                    ->with(['restaurant'=>function($q){
                        $q->with('lang:name,id,restuarants_id');
                    }])
                    ->with(['order_item'=>function($item){
                        $item->with(['menu_items'=>function($product){
                            $product->with('lang');
                        }])
                        ->with(['addon'=>function($q){
                            $q->with(['resturant_addons'=>function($rest){
                                $rest->with('lang:id,addon_id,name,language');
                            }]);
                        }]);
                    }])
             ->where('id',$id)
             ->first();
             $details->order_status_id = $details->order_status;
             $ord_status = Config::get('constants.userapp_order_status');
             $stat =  $ord_status[$details->order_status];
             $details->order_status =trans("userapp-messages.order_status.{$stat}");
             $details->restaurant->logo = $details->restaurant->logo ? asset('/uploads/'. $details->restaurant->logo) : null ;
             $details->order_date = Carbon::parse($details->created_at)->format('d M Y , g:i A');
             $details->order_item->each(function($item){
                $item->menu_items->cover_photo = $item->menu_items->cover_photo ? asset('uploads/'.$item->menu_items->cover_photo) : null ;
                $item->addon->each(function($item_ingre){
                    if($item_ingre->resturant_addons->type == 'By Restaurant')
                    {
                        $item_ingre->resturant_addons->image_path  =  $item_ingre->resturant_addons->image_path ? asset('/uploads/restaurant/addons/'. $item_ingre->resturant_addons->image_path) : null;
    
                    }else{
                        $item_ingre->resturant_addons->image_path  =  $item_ingre->resturant_addons->image_path? asset('/uploads/autosuggest/'. $item_ingre->resturant_addons->image_path) : null;
                    }
                });
             });

        $details->rating = $this->getOrderReview($id);
        return new SuccessWithData(
            $details);
    }

    protected  function getOrderReview($order)
    {
        $ratings = Reviews::with(['reviewsegment'=>function($q){
            $q->with(['segment'=>function($seg){
                $seg->with('lang:id,autosuggest_id,name,language')
                ->select('id');
            }]);
            
        }])
        ->with('customerdata:id,name')
        ->where('order_id', $order)
        ->where('approval_status', 'approve')
        ->first();
            // if($ratings)
        // $ratings->rating = $ratings->avg('rating');

        return $ratings ?? null ;
    }
}
