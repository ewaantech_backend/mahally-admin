<?php

namespace App\Http\Controllers\Api\Userapp;

use App\Models\Customers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Userapp\UpdateUserProfile;

class EditProfileController extends Controller
{
    protected $customer ;

    public function __construct(Customers $customer)
    {
        $this->customer = $customer ;
    }
    public function editProfile()
    {
        $customer = $this->customer 
                    ->where('id',auth()->user()->id)
                    ->select('name','phone_number','email','id','photo','phno_in_order_review','phno_in_order')
                    ->firstOrFail();
        $customer->photo = $customer->photo ? asset('uploads/'.$customer->photo) : null ;

        return new SuccessWithData($customer);
    }
    public function updateProfile(UpdateUserProfile $req)
    {
        $user = Customers::where('id',auth()->user()->id)
                 ->first();
        $path =$user->photo ;
        if($req->file('photo'))
        {
            $file = $req->file('photo');
            $path = $file->store("user_profile", ['disk' => 'public_uploads']);

        }
        
                $this->customer->where('id',auth()->user()->id)
                ->update([
                    "name"=>$req->name,
                    "email"=>$req->email,
                    "photo"=>$path,
                    "phno_in_order_review"=>$req->ph_in_review,
                    "phno_in_order"=>$req->ph_in_order
                ]);

                return new SuccessResponseMessage(trans("userapp-messages.profile.update_succ"));
        
    }
}
