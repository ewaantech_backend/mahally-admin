<?php

namespace App\Http\Controllers\Api\Userapp;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Userapp\LanguageRequest;

class LanguageController extends Controller
{
    public function update(LanguageRequest $request)
    {
        /** @var \App\Models\Customer*/
        $user = auth()->user();
        $user->update(['language' => $request->lang]);

        return new SuccessResponseMessage('Language updated successfully');
    }
}
