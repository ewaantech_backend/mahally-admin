<?php

namespace App\Http\Controllers\Api\Userapp;

use App\Models\Order;
use App\Models\OrderLog;
use App\Models\MenuItems;
use App\Models\OrderItems;
use App\Models\Promocodes;
use App\Services\SendEmail;
use Illuminate\Http\Request;
use App\Models\MenuItemAddons;
use App\Models\OfferManagement;
use App\Models\OrderItemsAddon;
use App\Models\RestaurantAddons;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Userapp\CheckoutRequest;
use App\Models\CommissionCategory;
use App\Models\Customers;
use App\Models\OfferLoyality;
use App\Models\ProductLoyality;
use App\Models\Restaurant;
use Exception;

class CheckoutController extends Controller
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order ;

    }

    public function checkOut(CheckoutRequest $req)
    {
        if($req->promo_code)
        {
            $promo = Promocodes::where(['promocode'=> $req->promo_code,'status'=>'active'])->first();
            if(!$promo)
            {
                return new ErrorResponse('Promocode not valid');
            }
            $cust_usage =  Order::where('promo_code',$req->promo_code)
                ->where('customer_id',$req->customer_id)
                ->count();
                $promo_total_usage = Order::where('promo_code',$req->promo_code)
                        ->count();
                if($promo_total_usage >= $promo->max_usage)
                {
                    return new ErrorResponse('Promocode Maximum usage exceeded');
                }else
                if($promo->max_usage_per_user <= $cust_usage)
                {
                    return new ErrorResponse('promocode Maximum usage exceeded for you ');
                }

        }
            $rest_data= Restaurant::where('id',$req->restaurant_id)->first();
            $commision = CommissionCategory::where('id',$rest_data->commission_category_id)->first();
            $order = Order::create([
                'customer_id'=>$req->customer_id,
                'restaurant_id'=>$req->restaurant_id,
                'order_type'=>$req->order_type,
                'item_count'=>$req->item_count,
                'order_status'=>1,
                'order_id'=>$this->generateOrderId(),
                'payment_mode'=>$req->payment_type,
                'table_id'=>$req->table_id  ?? 0 ,
                'vat' =>$rest_data->vat_includes == "yes" ? $rest_data->vat_percentage : 0 ,
                'commision_percentage'=>$commision->value
                
            ]);
            $items = [];
            $addOns = [];
            $total_sum = 0;
            $grand_sum = 0 ;
            foreach ($req->order_items as $key => $item) {
                if($item['item_type'] == 'loyality')
                {
                    $item_price = 0 ;
                }else if($item['item_type'] == 'offer')
                {
                    $item_price_all = MenuItems::where('id',$item['item_id'])->first();
                    $single_val = $item_price_all->discount_price ?  $item_price_all->discount_price : $item_price_all->price ;
                    $offer  = OfferManagement::where('id',$item['offer_id'])->first();
                    if($offer->discount_type == 'Value')
                    {
                        $item_price = $single_val - $offer->discount_value ;
                    }else{
                        $item_price = $single_val -( $offer->discount_value /100) ;

                    }
                }else{
                    $item_price_all = MenuItems::where('id',$item['item_id'])->first();
                    $item_price = $item_price_all->discount_price ?? $item_price_all->price ;

                }
                $total = $item_price * $item['item_count'] ;
                $total_sum = $total_sum+$total;
                $items = [
                    "order_id"=>$order->id,
                    "item_id"=>$item['item_id'],
                    'item_count'=>$item['item_count'],
                    'item_price' =>$item_price,
                    'sub_total'=>$total,
                    'grant_total'=>$total
                ];
                $item_id = OrderItems::create($items);
                $pro_loyality =  ProductLoyality::where([
                    'customer_id'=>$req->customer_id,
                    'restaurant_id'=>$req->restaurant_id,
                    'product_id'=> $item['item_id']
                ])
                ->first();
                if($item['item_type'] == 'normal')
                {
                    if($pro_loyality)
                    {
                        ProductLoyality::where([
                            'customer_id'=>$req->customer_id,
                            'restaurant_id'=>$req->restaurant_id,
                            'product_id'=>$item['item_id'],
                            
                        ])
                        ->update([
                            'product_count'=>$pro_loyality->product_count + $item['item_count']
                        ]);

                    }else{
                        ProductLoyality::create([
                            'customer_id'=>$req->customer_id,
                            'restaurant_id'=>$req->restaurant_id,
                            'product_id'=> $item['item_id'],
                            'product_count'=>$item['item_count']
                        ]);
                    }
                }
                if($item['item_type'] == 'loyality')
                {
                    $lay = OfferLoyality::where([
                        'product_id'=>$item['item_id'],
                        'offer_id'=>$item['offer_id'],
                        'customer_id'=>$req->customer_id
                    ])
                    ->first();
                    if($lay)
                    {
                        OfferLoyality::where([
                            'product_id'=>$item['item_id'],
                            'offer_id'=>$item['offer_id'],
                            'customer_id'=>$req->customer_id
                        ])
                        ->update([
                            'loyality_taken'=>$item['item_count']+$lay->loyality_taken,
                        ]);
                    }else{
                        
                        OfferLoyality:: create([
                            'product_id'=>$item['item_id'],
                            'offer_id'=>$item['offer_id'],
                            'customer_id'=>$req->customer_id,
                            'loyality_taken'=>$item['item_count']
                        ]);

                    }
                   
                }
               if($item['addon'] != null)
                {
                    foreach($item['addon'] as $addon)
                    {
                        // $addOn_id = MenuItemAddons::where('id',$addon['addon_id'])->first();
                        $add_price = RestaurantAddons::where('id', $addon['addon_id'])->first();
                        $grand = $add_price->price * $addon['item_count'] ;
                        $grand_sum =  $grand_sum + $grand ;
                        $addOns = [
                            "order_id"=>$order->id,
                            "order_item_id"=>$item_id->id,
                            "addon_id"=>$addon['addon_id'],
                            'item_count'=>$addon['item_count'],
                            'item_price'=>$add_price->price,
                            'grant_total'=>$grand,
                            'sub_total' => $grand
                        ];
                        OrderItemsAddon::create($addOns);
                    }
                }
                $order_grand =  $grand_sum + $total_sum ;
                $value = 0 ;
                if($req->promo_code)
                {
                    $promo = Promocodes::where(['promocode'=> $req->promo_code,'status'=>'active'])->first();
                    if($promo)
                    {
                        switch($promo->type)
                        {
                            case 'Amount':
                                $value = $promo->amount;
                            break;
                            case 'Percentage':
                                $value = $order_grand * $promo->amount /100 ;
                            break ;
                        }
                    }else{
                        return new ErrorResponse('Promocode not correct/expeire');
                    }
                }
                $value_grand_tot = $order_grand - $value ;
                if($rest_data->vat_includes == "yes")
                {
                    $value_grand_tot =$value_grand_tot +($value_grand_tot * $rest_data->vat_percentage/100) ;
                } 
                Order::where('id',$order->id)
                        ->update([
                            'sub_total' =>$order_grand,
                            'grand_total'=> $value_grand_tot < 0 ? 0 :$value_grand_tot,
                            'promo_code'=>$req->promo_code,
                            'promo_type'=>$promo->type ?? null,
                            'promo_value'=>$promo->amount ?? null
                            
                        ]);

            }
        $my_order = Order::where('id',$order->id)->first();
        OrderLog::create([
            'order_id' => $my_order->id,
            'log' => auth()->user()->name . " (Customer) create a new order ",
            'user_id' => auth()->user()->id,
            'done_by' => 'customer',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        // $this->sendEmailToCustomer($order->id);
        return new SuccessWithData([
            'order_id'=>$my_order->order_id,
            "amount"=>$my_order->grand_total,
            "id"=>$my_order->id
        ]);
    }

    protected function generateOrderId()
    {
        $lastQtn = Order::select('order_id')
        ->orderBy('id', 'desc')
        ->first();

    $lastId = 0;

    if ($lastQtn) {
        $lastId = (int) substr($lastQtn->order_id, 8);
    }

    $lastId++;

    return 'MAH-ORD-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);

    }

    protected function sendEmailToCustomer($order_id)
    {
        $order_data = Order::with(['restaurant'=>function($q){
            $q->with('lang:name,id,restuarants_id');
        }])
        ->with(['order_item'=>function($item){
            $item->with(['menu_items'=>function($product){
                $product->with('lang');
            }])
            ->with(['addon'=>function($q){
                $q->with(['resturant_addons'=>function($rest){
                    $rest->with('lang:id,addon_id,name,language');
                }]);
            }]);
        }])
    ->where('id',$order_id)
    ->first();
        
    $customer_data = Customers::where('id',$order_data->customer_id)->first();
        $subject = 'Thank you for your order!';
        // dd($customer_data );
        if ($customer_data->email)
        {
            SendEmail::generateOrderEmail($customer_data->email, $customer_data->name, $subject, $order_data, 1);
        }
       
    }

    public function vatDetails(Request $req)
    {
        $vat_data = Restaurant::where('id',$req->restaurant_id)
        ->select('id','vat_includes','vat_percentage','vat_reg_number')
        ->first();
        return new SuccessWithData($vat_data);
    }
}
