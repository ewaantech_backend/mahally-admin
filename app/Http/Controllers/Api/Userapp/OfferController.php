<?php

namespace App\Http\Controllers\Api\Userapp;

use Exception;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Reviews;
use App\Models\Promocodes;
use App\Models\AutoSuggest;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Traits\FormatLanguage;
use App\Models\OfferManagement;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Userapp\ReviewRequest;
use App\Models\Restaurant;

class OfferController extends Controller
{
    use FormatLanguage;

    protected $offer ;

    public function __construct(OfferManagement $offer)
    {
        $this->offer = $offer ;
    }
    public function listOffers(Request $req)
    {
        $lat = $req->latitude ?? 0 ;
        $long = $req->longitude ?? 0;
        $rest_id = '';
        // if($req->latitude && $req->longitude)
        // {
        //     $rest_id = Restaurant::where(['latitude'=> $req->latitude,'longitude'=>$req->longitude])
        //                     ->pluck('id')
        //                     ->toArray();
        // }
        
        
        $offerList = $this->offer->with('lang')
        ->with(['restaurent'=>function($q) use($lat,$long){
            $q->with('lang:id,restuarants_id,name,language')
            ->select('id','location','latitude','longitude','opening_time','closing_time')
            ->selectRaw('ROUND( 6371 * acos( cos( radians(?) ) *
                cos( radians(latitude ) )
                * cos( radians(longitude ) - radians(?)
                ) + sin( radians(?) ) *
                sin( radians(latitude ) ) )
                ,2) AS distance', 
                [$lat, $long, $lat]
                 );
        }])
        ->with(['items'=>function($q) {
            $q->with('lang:id,menu_item_id,language');
        }])
        ->where('status','active')
        ->whereDate('expire_in', '>=', Carbon::now())
    //     ->when($lat, function ($query) use ($rest_id) {
    //         return $query->whereIn('restuarants_id', $rest_id);
    //   })
        ->get()
        ->sortBy('distance');
        $this->keyByNested($offerList, 'lang', 'language');
        $offerList->each(function($offer){
            
            $subscription_end = new Carbon($offer->expire_in);
            $days = $subscription_end->diffInDays(Carbon::now());
            $offer->remaining_days = $days + 1;
            $offer->lang['en']->image_path = $offer->lang['en']->image_path ? asset('uploads/restaurant/offer_management/en/'.$offer->lang['en']->image_path) : null ;
            $offer->lang['ar']->image_path = $offer->lang['ar']->image_path ? asset('uploads/restaurant/offer_management/ar/'.$offer->lang['ar']->image_path) :null ;
            $offer->time_status = $this->restaurantStatus($offer->restuarants_id);
        });

        return new SuccessWithData($offerList);
    }

    public function ratingOrder(ReviewRequest $request)
    {
     
        $order = $this->getBooking($request->order_id);

        if ($order->order_status != 5) {
            return new ErrorResponse(trans('userapp-messages.review.canot_review'));
        }

        $ratings = Arr::pluck($request->review, 'rating');
        $totalRating = round(array_sum($ratings) / count($ratings), 1);

        $data = $request->validated();
        // dd($data['restaurant']);
        DB::transaction(function () use ($order, $totalRating, $data) {

            $review = Reviews:: updateOrCreate([
                'order_id' => $order->id,
                'customer_id' => auth()->id(),
            ],
            [
                'restaurant_id'=>$data['restaurant'],
                'rating' => $totalRating,
                'approval_status' => 'Approve',
                'comment'=>$data['message']
            ]);
            $review->reviewsegment()->forceDelete();
            $review->reviewsegment()->createMany($data['review']);
        });

        return new SuccessResponseMessage(trans('userapp-messages.review.add_succes'));
    }
    protected function getBooking($bookingId)
    {
        $booking = Order::where('id', $bookingId)
            ->where('customer_id', auth()->id())
            ->select('id', 'order_id', 'order_status')
            ->first();

        if (is_null($booking)) {
            throw new Exception('Invalid order');
        }

        return $booking;
    }

    public function listRating()
    {
        $list = AutoSuggest::with('lang:id,language,autosuggest_id,name')
                    ->where('type','rating_segments')
                    ->select('id','type')
                    ->get();
        $this->keyByNested($list,'lang','language');
        return new SuccessWithData($list);
    }

    public function getReviews($reataurant)
    {
        $ratings = Reviews::with(['reviewsegment'=>function($q){
            $q->with(['segment'=>function($seg){
                $seg->with('lang:id,autosuggest_id,name,language')
                ->select('id');
            }]);
            
        }])
        ->with('customerdata:id,name,photo')
        ->where('restaurant_id', $reataurant)
        ->where('approval_status', 'approve')
            
            // ->selectRaw(' ROUND(AVG(rating), 1) as rating')
            ->paginate(15)
            ->groupBy('customer_id');
            $ratings->map(function ($rate){
                $rate->rating = Round($rate->avg('rating'));
                $rate->each(function($q){
                    $q->customerdata->photos =  $q->customerdata->photo ?  asset('/uploads/'.$q->customerdata->photo) : null ;
                });
            });
           
        return new SuccessWithData($ratings);
    }

    public function getOrderReview($order)
    {
        $ratings = Reviews::with(['reviewsegment'=>function($q){
            $q->with(['segment'=>function($seg){
                $seg->with('lang:id,autosuggest_id,name,language')
                ->select('id');
            }]);
            
        }])
        ->with('customerdata:id,name,photo')
        ->where('order_id', $order)
        ->where('approval_status', 'approve')
            
            // ->selectRaw(' ROUND(AVG(rating), 1) as rating')
            ->paginate(15)
            ->groupBy('customer_id');
            $ratings->map(function ($rate){
                $rate->rating = Round($rate->avg('rating'));
                $rate->each(function($q){
                    $q->customerdata->photos =  $q->customerdata->photo ?  asset('/uploads/'.$q->customerdata->photo) : null ;
                });
            });

        return new SuccessWithData($ratings);
    }

    public function promocodeCheck(Request $req)
    {
        $_code = $req->code ;
             
        $promo =  Promocodes::where(DB::raw('BINARY `promocode`'), $_code)
        ->where('status','active')
        ->whereDate('expire_in','>=',now())
        ->first();
            if(!$promo)
            {
                return new ErrorResponse(trans('userapp-messages.promocode.in_valid'));
            }else{
               $cust_usage =  Order::where('promo_code',$req->code)
                ->where('customer_id',$req->cust_id)
                ->count();
                $promo_total_usage = Order::where('promo_code',$req->code)
                        ->count();
                if($promo_total_usage >= $promo->max_usage)
                {
                    return new ErrorResponse(trans('userapp-messages.promocode.max_usage'));
                }else
                if($promo->max_usage_per_user <= $cust_usage)
                {
                    return new ErrorResponse(trans('userapp-messages.promocode.user_exceed'));
                }else{
               
                return response()->json([
                    "success"=>true,
                    'msg' => trans('userapp-messages.promocode.apply'), 
                    'data' => $promo
                    ]);
                }

            }
    }

    protected function restaurantStatus($rest_id)
    {
        $details = Restaurant::where('id',$rest_id)->first();
        if((isset($details->opening_time)) && (isset($details->closing_time)))
        {
        // $time_open = (Carbon::parse($details->opening_time)->format('H:i:s')) ;
        // $current_time = Carbon::now()->format('H:i:s');
        // $time_close =(Carbon::parse($details->closing_time)->format('H:i:s')) ;
        //         $details->closing_time = $time_close;
        //         $details->opening_time =$time_open ;
        $time_open =optional($details->opening_time)->format('H:i:s');
        
                $current_time = Carbon::now()->timezone('Asia/Riyadh')->format('H:i:s');
                        $time_close =optional($details->closing_time)->format('H:i:s');
                
                        $details->closing_time = $time_close;
                        $details->opening_time =$time_open ;
                if($current_time >= $time_open && $current_time < $time_close)
                {
                    $diff_min = Carbon::parse($details->closing_time)->diffInMinutes(Carbon::now()) ;
                    if( $diff_min <= 30 ){

                        $time_stat = 'Closing in '.$diff_min .' minutes';
                    }else{
                        $time_stat = 'OPEN';
                    }
                   
                }else{
                   $time_stat = 'CLOSED';
                }

                return $time_stat ;
        }
    }
}
