<?php

namespace App\Http\Controllers\Api\Userapp;

use Carbon\Carbon;
use App\Models\Customers;
use App\Services\OtpService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Responses\ErrorResponse;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Http\Requests\Api\Userapp\LoginRequest;
use App\Http\Requests\Api\Userapp\OtpVerifyRequest;

class LoginController extends Controller
{
    protected $customer;

    public function __construct(Customers $customer)
    {
        $this->customer = $customer;
    }
    public function UserLogin(LoginRequest $req, OtpService $otpService)
    {
        if ($this->is_arabic($req->phone)) {
            $converted_phone = $this->convert($req->phone);
            $phone_trimmed = ltrim($converted_phone, '0');
            $phone = '0' . $phone_trimmed;
        } else {
            $phone_trimmed = ltrim($req->phone, '0');
            $phone = '0' . $phone_trimmed;
        }
        $customer = $this->customer
            ->whereIn('phone_number', array($phone_trimmed, $phone))
            // ->where('role_id', static::DRIVER)
            // ->where('status','active')
            ->first();

        $noUserFound = is_null($customer);

        if ($noUserFound) {

           $customer = Customers::create([
               "phone_number"=> $req->phone
           ]);
           $otp = $this->generateNewOtp($customer);
            return new SuccessResponseMessage(trans('userapp-messages.login.send_succes'));
        }else{
            $cust = $this->customer
            ->whereIn('phone_number', array($phone_trimmed, $phone))
            // ->where('role_id', static::DRIVER)
            ->where('status','active')
            ->first();

        $noUser= is_null($cust);
            if($noUser){
                return new ErrorResponse(trans('userapp-messages.login.activate'));
            }else{
            $otp = $this->generateNewOtp($customer);
            return new SuccessResponseMessage(trans('userapp-messages.login.send_succes'));
            }
        }

       
    }
    /**
     * Generate new otp and save to db
     *
     * @param User $user
     * @return string
     */
    protected function generateNewOtp(Customers $customer)
    {
        $otpExpiry = Carbon::parse($customer->otp_generated_at)
            ->addMinutes(0);

        $now = now();

        // $otp = $otpExpiry->gt($now)
        //     ? $user->otp
        //     : OtpGenerator::generateOtp($user->phone);
        $otp = 1234;
        // dd($driver);
        $customer->update([
            'otp' => $otp,
            'otp_generated_at' => $now,
        ]);

        return $otp;
    }
    protected function is_arabic($str)
    {
        if (mb_detect_encoding($str) !== 'UTF-8') {
            $str = mb_convert_encoding($str, mb_detect_encoding($str), 'UTF-8');
        }

        /*
        $str = str_split($str); <- this function is not mb safe, it splits by bytes, not characters. we cannot use it
        $str = preg_split('//u',$str); <- this function woulrd probably work fine but there was a bug reported in some php version so it pslits by bytes and not chars as well
        */
        preg_match_all('/.|\n/u', $str, $matches);
        $chars = $matches[0];
        $arabic_count = 0;
        $latin_count = 0;
        $total_count = 0;
        foreach ($chars as $char) {
            //$pos = ord($char); we cant use that, its not binary safe 
            $pos = $this->uniord($char);
            // echo $char ." --> ".$pos.PHP_EOL;

            if ($pos >= 1536 && $pos <= 1791) {
                $arabic_count++;
            } else if ($pos > 123 && $pos < 123) {
                $latin_count++;
            }
            $total_count++;
        }
        if (($arabic_count / $total_count) > 0.6) {
            // 60% arabic chars, its probably arabic
            return true;
        }
        return false;
    }

    protected  function convert($string)
    {
        return strtr($string, array('۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9'));
    }

    protected function uniord($u)
    {
        // i just copied this function fron the php.net comments, but it should work fine!
        $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
        $k1 = ord(substr($k, 0, 1));
        $k2 = ord(substr($k, 1, 1));
        return $k2 * 256 + $k1;
    }

    public function otpVerify(OtpVerifyRequest $request)
    {
        if ($this->is_arabic($request->phone)) {
            $converted_phone = $this->convert($request->phone);
            $phone_trimmed = ltrim($converted_phone, '0');
            $phone = '0'.$phone_trimmed;
        } else {
            $phone_trimmed = ltrim($request->phone, '0');
            $phone = '0'.$phone_trimmed;
        }
        $user = $this->customer
            ->whereIn('phone_number', array($phone_trimmed,$phone))
            ->where('status', 'active')
            ->first();

        if (!$user) {
            return new ErrorResponse(trans('userapp-messages.login.user_nt_exist'));
        }

        if ($this->isValidOtp($user, $request->otp)) {

            $user->update([
               
                'fcm_token' => $request->fcm_token,
            ]);
            
            $token = $user->createToken('Api Token', ['customer'])->accessToken;
            return new SuccessWithData([
                'user' => $user,
                'access_token' => $token,
            ]);
        }

        return new ErrorResponse(trans('userapp-messages.login.otp_wrong'));
    }
    
     /**
     * Check if the otp is valid
     *
     * @param User $user
     * @param string $otp
     * @return boolean
     */
    protected function isValidOtp(Customers $customer, $otp)
    {
        $otpExpiry = Carbon::parse($customer->otp_generated_at)
            ->addMinutes(60);

        return $customer->otp == $otp
            && $otpExpiry->gt(now());
    }

    /**
     * Undocumented function
     *
     * @param LoginRequest $request
     * @param OtpService $otpService
     * @return void
     */
    
   public function resend(LoginRequest $request,OtpService $otpService)
   {
    if ($this->is_arabic($request->phone)) {
        $converted_phone = $this->convert($request->phone);
        $phone_trimmed = ltrim($converted_phone, '0');
        $phone = '0'.$phone_trimmed;
    } else {
        $phone_trimmed = ltrim($request->phone, '0');
        $phone = '0'.$phone_trimmed;
    }

    $user = $this->customer
        ->whereIn('phone_number', array($phone_trimmed,$phone))
        ->where('status', 'active')
        ->first();
    
    $otp = $this->generateNewOtp($user);
    $phone_trimmed = ltrim($request->phone, '0');

    // $message = trans('sms.otp.salon', ['otp' => $otp]);
    // $otpService->send($message, $phone_trimmed, $user->country_code);

    return new SuccessResponseMessage(trans('userapp-messages.login.resend'));
   }

   public function logout()
   {
        $user = auth()->user();
        $user->token()->revoke();
        return new SuccessResponseMessage(trans('userapp-messages.login.logout'));

   }

}
