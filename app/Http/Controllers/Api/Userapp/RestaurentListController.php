<?php

namespace App\Http\Controllers\Api\Userapp;

use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Userapp\RestaurantListRequest;
use App\Http\Responses\SuccessWithData;
use App\Models\AutoSuggest;
use App\Traits\FormatLanguage;
use Carbon\Carbon;

class RestaurentListController extends Controller
{
    use FormatLanguage;

    protected $restaurent ;

    public function __construct(Restaurant $restaurent)
    {
        $this->restaurent= $restaurent;

    }

    public function get(RestaurantListRequest $req)
    {
        $latitude = $req->latitude ?? 24.7136;
        $longitude = $req->longitude ?? 46.6753;
        $category = $req->category ;
        $nearBy = $req->nearBy ;
        $list = $this->restaurent
                ->select('restuarants.id','restuarants.logo','restuarants.cover_photo','restuarants.location','restuarants.latitude','restuarants.longitude','restuarants.closing_time','restuarants.opening_time')
                ->with('lang:id,name,language,restuarants_id')
                // ->with(['type'=>function($q){
                //     $q->where('type', 'restaurant_type')
                //     ->with('lang');
                // }])
                ->with(['type'=>function($q){
                    $q->where('type', 'restaurant_type')
                    ->withPivot(["restuarants_id",
                    'restuarant_type_id','deleted_at'])
                   ->where('restuarants_restuarant_type.deleted_at', null)
                    ->with('lang:id,autosuggest_id,language,name');
                }])
                ->with('rating')
                ->when($category, function ($query) use ($category) {
                    $query->whereHas('type', function ($query) use ($category) {
                        $query->whereIn('restuarants_restuarant_type.restuarant_type_id',$category)
                        ->where('restuarants_restuarant_type.deleted_at',null);
                    });
                })
                ->where('status','active')
                ->selectRaw('ROUND( 6371 * acos( cos( radians(?) ) *
                cos( radians( latitude ) )
                * cos( radians( longitude ) - radians(?)
                ) + sin( radians(?) ) *
                sin( radians( latitude ) ) )
                ,2) AS distance', 
                [$latitude, $longitude, $latitude]
                 )
                 ->when($req->has('sortby' ), function ($query) use($req) {
                  if($req->sortby == "nearby")
                  {
                    $query->having('distance', '<=', 1000);
                  }else if($req->sortby == "popularity")
                  {
                    $query->whereHas('rating', function ($query){
                        $query->orderBy('reviews.rating','desc');
                    });
                  }
                })
                ->orderBy('distance')
                ->paginate(5);
            $list->map(function($data){
                $data->logo = $data->logo ?  asset('uploads/'.$data->logo) : null ;
                $data->cover_photo = $data->cover_photo ?  asset('uploads/'.$data->cover_photo) : null ;
                // $time_close =Carbon::parse($data->closing_time)->format('H:i:s') ;
                // $time_open =Carbon::parse($data->opening_time)->format('H:i:s') ;
                // $current_time = Carbon::now()->format('H:i:s');
                // $data->closing_time = $time_close;
                // $data->opening_time =$time_open ;
                $time_open =optional($data->opening_time)->format('H:i:s');
        
                $current_time = Carbon::now()->timezone('Asia/Riyadh')->format('H:i:s');
                        $time_close =optional($data->closing_time)->format('H:i:s');
                
                        $data->closing_time = $time_close;
                        $data->opening_time =$time_open ;
                if($current_time >= $time_open && $current_time < $time_close)
                {
                    $diff_min = Carbon::parse($data->closing_time)->diffInMinutes(Carbon::now()) ;
                    if( $diff_min <= 30 ){

                        $data->time_status = 'Closing in '.$diff_min .' minutes';
                    }else{
                        $data->time_status = 'OPEN';
                    }
                   
                }else{
                    $data->time_status = 'CLOSED';
                }
            });
             $this->keyByNested($list, 'lang', 'language');
        return new SuccessWithData($list);
    }
    public function nearBy(RestaurantListRequest $req)
    {
        $latitude = $req->latitude ?? 24.7136;
        $longitude = $req->longitude ?? 46.6753;
        $category = $req->category ;
        $list = $this->restaurent
                ->select('restuarants.id','restuarants.logo','restuarants.cover_photo','restuarants.location','restuarants.latitude','restuarants.longitude','restuarants.closing_time','restuarants.opening_time')
                ->with('lang:id,name,language,restuarants_id')
                ->with(['type'=>function($q){
                    $q->where('type', 'restaurant_type')
                    ->withPivot(["restuarants_id",
                    'restuarant_type_id','deleted_at'])
                   ->where('restuarants_restuarant_type.deleted_at', null)
                    ->with('lang:id,autosuggest_id,language,name');
                }])
                ->with('rating')
                ->when($category, function ($query) use ($category) {
                    $query->whereHas('type', function ($query) use ($category) {
                        $query->whereIn('restuarants_restuarant_type.restuarant_type_id',$category)
                                    ->where('restuarants_restuarant_type.deleted_at',null);
                    });
                })
                ->where('status','active')
                ->selectRaw('ROUND( 6371 * acos( cos( radians(?) ) *
                cos( radians( latitude ) )
                * cos( radians( longitude ) - radians(?)
                ) + sin( radians(?) ) *
                sin( radians( latitude ) ) )
                ,2) AS distance', 
                [$latitude, $longitude, $latitude]
                 )
                ->orderBy('distance')
                ->take(20)
                ->get();
            $list->each(function($data){
                $this->keyBy($data, 'lang', 'language');
                $data->logo = $data->logo ?  asset('uploads/'.$data->logo) : null ;
                $data->cover_photo = $data->cover_photo ?  asset('uploads/'.$data->cover_photo) : null ;
                // $time_close =Carbon::parse($data->closing_time)->format('H:i:s') ;
                // $time_open =Carbon::parse($data->opening_time)->format('H:i:s') ;
                // $current_time = Carbon::now()->format('H:i:s');
                // $data->closing_time = $time_close;
                // $data->opening_time =$time_open ;
                $time_open =optional($data->opening_time)->format('H:i:s');
        
                $current_time = Carbon::now()->timezone('Asia/Riyadh')->format('H:i:s');
                        $time_close =optional($data->closing_time)->format('H:i:s');
                
                        $data->closing_time = $time_close;
                        $data->opening_time =$time_open ;
                if($current_time >= $time_open && $current_time < $time_close)
                {
                    $diff_min = Carbon::parse($data->closing_time)->diffInMinutes(Carbon::now()) ;
                    if( $diff_min <= 30 ){

                        $data->time_status = 'Closing in '.$diff_min .' minutes';
                    }else{
                        $data->time_status = 'OPEN';
                    }
                   
                }else{
                    $data->time_status = 'CLOSED';
                }
            });
            
        return new SuccessWithData($list);
    }

    public function restaurantType()
    {
        $type = AutoSuggest::where('type','restaurant_type')
        ->with('lang:id,autosuggest_id,name,language')
        ->select('id')
        ->get();
        $this->keyByNested($type, 'lang', 'language');
        
        return new SuccessWithData($type);
    }
    
}
