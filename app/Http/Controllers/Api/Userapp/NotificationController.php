<?php

namespace App\Http\Controllers\Api\Userapp;

use App\User;
use Illuminate\Http\Request;
use App\Models\AppNotification;
use App\Http\Controllers\Controller;
use App\Http\Responses\SuccessWithData;
use App\Http\Responses\SuccessResponseMessage;
use App\Models\Customers;

class NotificationController extends Controller
{
    protected $notification;

    public function __construct(AppNotification $notification)
    {
        $this->notification = $notification;
    }

    public function get()
    {
        $notifications = $this->notification
            ->where('to_type', 'userapp')
            ->where('to_id', auth()->id())
            ->orderBy('created_at', 'desc')
            ->select('id', 'content', 'is_read', 'details', 'created_at')
            ->paginate(15);
        
        return new SuccessWithData($notifications);
    }

    public function markAsRead($notificationId)
    {
        $notification = $this->notification
            ->where('id', $notificationId)
            ->where('to_id', auth()->id())
            ->where('to_type', 'userapp')
            ->firstOrFail();

        $notification->update(['is_read' => 1]);

        return new SuccessResponseMessage('Notification changed to read status');
    }
    public function toggle(Request $request)
    {
        $request->validate([
            'enable' => 'required|boolean'
        ]);
        
        /** @var \App\Models\Customer */
        $user = auth()->user();
        $status = $request->enable ;

        $user->update(['need_notification' => $status]);
        
        
        $message = $status == 1 ? trans('userapp-messages.notification.enabled'):trans('userapp-messages.notification.disabled');

        return new SuccessResponseMessage($message);
    }

    public function notificationCheck()
    {
        $need_notification = auth()->user()->only('id','name','need_notification');
        return new SuccessWithData( $need_notification);
    }
    
    public function unreadCount()
    {
        $noti_unread = $this->notification
            ->where('to_id', auth()->id())
            ->where('to_type', 'userapp')
            ->where('is_read', '0')
            ->count();
        $badge = Customers::where('id',auth()->user()->id)->first();
        return new SuccessWithData([
            "count"=>$noti_unread,
            "badge"=>$badge->badge_seen
            ]);
    }
    public function badgeUpdate()
    {
        $badge = Customers::where('id',auth()->user()->id)->update([
            'badge_seen' =>'false'
        ]);
        return new SuccessResponseMessage('badge updated');
    
    }
}
