<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\UserRoles;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Mail\SetPasswordEmail;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserManagementController extends Controller
{
    protected const ZERO = 0;
    public function get(Request $req)
    {

        $role = UserRoles::get();
        $search = $req->search ?? '';
        $role_id = $req->select_role ?? '';
        if ($search || $role_id) {
            $query = User::query()->with('roles');
            if ($search) {
                $query->where(function ($sub) use ($search) {
                    $sub->where('name', 'like', "%" . $search . "%");
                    $sub->orWhere('email', 'like', "%" . $search . "%");
                    $sub->orWhere('phone', 'like', "%" . $search . "%");
                    $sub->orWhere('user_id', 'like', "%" . $search . "%");
                });
            }

            if ($role_id) {

                $query->where('role_id', $role_id);
            }
            $user = $query->where('id', '!=', Auth::user()->id)->paginate(20);
            return view('admin.user.index', compact('user', 'role', 'search', 'role_id'));
        } else {
            $user = User::with('roles')->where('id', '!=', Auth::user()->id)->paginate(20);
            return view('admin.user.index', compact('user', 'role', 'search', 'role_id'));
        }
    }
    public function store(Request $req)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'role' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'User with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'User with same phone number already exists',
            'role.required' => 'Role is required.'
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

            $user = $this->generateUserId();
            $userData = User::create([
                'name' => $req->name,
                'email' => $req->email,
                'country_code' => "+966",
                'phone' => (int)$req->phone,
                'role_id' => $req->role,
                'user_id' => $user,
                'status' => 'active'
            ]);
            $msg = "User added successfully";
            if ($userData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $users = User::where('id', $id)->first();
        return [
            'user' => $users
        ];
    }
    public function generateUserId()
    {
        $lastUser = User::select('user_id')
            ->orderBy('id', 'desc')
            ->first();

        $lastId = 0;

        if ($lastUser) {
            $lastId = (int) substr($lastUser->user_id, 8);
        }
        $lastId++;
        $userId = 'MAHUSER-' . str_pad($lastId, 5, '0', STR_PAD_LEFT);
        return $userId;
    }

    public function update(Request $req)
    {

        $rules = [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $req->user_unique,
            'phone' => 'required|unique:users,phone,' . $req->user_unique,
            'role' => 'required',
        ];
        $messages = [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required.',
            'email.unique' => 'User with same email already exists',
            'phone.required' => 'Phone number is required.',
            'phone.unique' => 'User with same phone number already exists',
            'role.required' => 'Role is required.'
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $userData = User::where('id', $req->user_unique)
                ->update([
                    'name' => $req->name,
                    'email' => $req->email,
                    'country_code' => "+966",
                    'phone' => (int)$req->phone,
                    'role_id' => $req->role,
                ]);
            $msg = "User details updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }

    public function statusUpdate(Request $req)
    {
        if ($req->status == 'Deactivate') {
            $status = 'deactive';
        } else {
            $status = 'active';
        }

        $statusUpdate = User::where('id', $req->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $req)
    {
        if ($req->id) {
            $user = User::find($req->id);
            if (!empty($user)) {
                $user->delete();
                return response()->json(['status' => 1, 'message' => 'User deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }

    public function passwordSend(Request $req)
    {
        $user = User::where('id', $req->id)->first();

        // $password = mt_rand(100000, 999999);
        $password = '123456';
        $data = [
            'password' => $password,
            'user' => $user
        ];
        DB::transaction(function () use ($data, $password, $user, $req) {
            User::where('id', $req->id)->update([
                'password' => Hash::make($password)
            ]);
        });

        $email = Mail::to($user->email)->send(new SetPasswordEmail($data));
        // return $email;
        // if ($email) {
        //     return response()->json(['status' => 1, 'message' => 'Welcome mail send successfully']);
        // } else {
        //     return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        // }
        // return response()->json(['status' => 1, 'message' => 'One time password send successfully']);
        return response()->json(['status' => 1, 'message' => 'Welcome mail send successfully']);
    }
    public function link_resturant(Request $req)
    {
        $user = User::select('id', 'name', 'restaurants')->where('id', $req->id)->first();
        $sel_rest = [];

        if ($user->restaurants) {
            foreach (json_decode($user->restaurants) as $item) {
                $sel_rest[] = $item;
            }
        }
        // dd($sel_rest);
        $restaurants = Restaurant::with('lang:id,restuarants_id,name')->select('id')->where('status', 'active')->where('deleted_at', NULL)->get();

        return view('admin.user.add_restaurant', compact('user', 'restaurants', 'sel_rest'));
    }
    public function add_resturants(Request $req)
    {
        $user_id = $req->user_id;
        $restaurants = $req->restaurants;
        $user = User::where('id', $user_id)->update([
            'restaurants' => json_encode($restaurants)
        ]);

        if ($user) {
            return response()->json(['status' => 1, 'message' => 'Restaurants added  successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
