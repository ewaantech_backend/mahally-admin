<?php

namespace App\Http\Controllers\Admin;

use App\Models\Pages;
use App\Models\PagesLang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function get()
    {
        $pages = Pages::with('lang')->orderBy('created_at', 'desc')->paginate(20);
        $app_type = [
            'user',
            'sales',
            'restaurant',
        ];
        return view('admin.cms.pages.index', compact('pages', 'app_type'));
    }
    public function edit($id)
    {
        $page_data = Pages::with('lang')
            ->where('id', $id)->first();
        return [
            'page' => $page_data
        ];
    }
    public function update(Request $request)
    {
        $sement = DB::transaction(function () use ($request) {
            Pages::where('id', $request->id)
                ->update([
                    'available_for' => 'user'
                ]);
            PagesLang::where('language', 'en')
                ->where('page_id', $request->id)
                ->update([
                    'title' => $request->title_en,
                    'content' => $request->content_en
                ]);
            PagesLang::where('language', 'ar')
                ->where('page_id', $request->id)
                ->update([
                    'title' => $request->title_ar,
                    'content' => $request->content_ar
                ]);
        });
        return [
            "msg" => "success"

        ];
    }
}
