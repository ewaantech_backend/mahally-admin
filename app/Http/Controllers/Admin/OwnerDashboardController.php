<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Models\Order;
use App\Models\Customers;
use App\Models\OrderItems;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class OwnerDashboardController extends Controller
{
    public function get()
    {
        $user = User::select('id', 'restaurants')->where('id', Auth::user()->id)->first();
        $sel_rest = [];

        if ($user->restaurants) {
            foreach (json_decode($user->restaurants) as $item) {
                $sel_rest[] = $item;
            }
        }

        $total_sale =  Order::whereIn('orders.order_status', [5])->whereIn('restaurant_id', $sel_rest)->sum('orders.grand_total');
        $total_customer = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->whereIn('orders.restaurant_id', $sel_rest)->where('customer.status', 'active')->groupBy('orders.customer_id')->get();
        $total_customers = count($total_customer);

        $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->whereIn('restaurant_id', $sel_rest)->where('deleted_at', NULL)->count();
        $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");


        $total_restaurants = Restaurant::whereIn('id', $sel_rest)->where('status', 'active')->where('deleted_at', NULL)->count();
        $order_status = Config::get('constants.frontdisk_order_status');
        $status = array("2", "3", "4");
        $recent_order = Order::with('customer')->with('restaurant')->whereIn('restaurant_id', $sel_rest)->whereIn('order_status', $status)->orderBy('created_at', 'desc')->limit(5)->get();
        // dd($recent_order->restaurant);

        $resturants =  Restaurant::with('lang')->whereIn('id', $sel_rest)->where('status', 'active')->where('deleted_at', NULL)->get();


        $top_selling_restaurant = Order::with(['restaurant' => function ($p) use ($sel_rest) {
            $p->with('lang:restuarants_id,language,name');
        }])
            ->groupBy('restaurant_id')
            ->selectRaw('order_id,restaurant_id, sum(grand_total) as grand_total')->whereIn('restaurant_id', $sel_rest)->orderBy('grand_total', 'desc')->limit(5)
            ->get();


        $top_selling_item = OrderItems::with(['menu_items' => function ($q) {
            $q->with('lang:menu_item_id,name');
        }])
            ->with(['order' => function ($q) {
                $q->with(['restaurant' => function ($p) {
                    $p->with('lang:restuarants_id,language,name');
                }]);
            }])
            ->groupBy('item_id')
            ->selectRaw('order_id,item_id, sum(item_count) as item_count')->orderBy('item_count', 'desc')->limit(5)
            ->get();
        $top_customer_using_app = Order::with('customer')
            ->groupBy('customer_id')
            ->selectRaw('order_id,customer_id, sum(grand_total) as grand_total,count(id) as total_order')->whereIn('restaurant_id', $sel_rest)->orderBy('grand_total', 'desc')->limit(5)
            ->get();

        $top_order_cancelling_restaurant = Order::with(['restaurant' => function ($p) {
            $p->with('lang:restuarants_id,language,name');
        }])
            ->groupBy('restaurant_id')
            ->where('order_status', 5)
            ->selectRaw('order_id,restaurant_id, sum(grand_total) as grand_total,count(id) as total_order')->whereIn('restaurant_id', $sel_rest)->orderBy('grand_total', 'desc')->limit(5)
            ->get();

        return view('admin.restaurant_owner.dashboard.index', compact('recent_order', 'order_status', 'total_customers', 'sales_count', 'total_orders', 'total_restaurants', 'resturants', 'top_selling_item', 'top_selling_restaurant', 'top_customer_using_app', 'top_order_cancelling_restaurant'));
    }
}
