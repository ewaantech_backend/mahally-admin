<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Customers;
use App\Models\Restaurant;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class OrderController extends Controller
{
    protected $order;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }
    public function activeOrders(Request $req)
    {
        $cust_id = $req->customer ?? '';
        $search = $req->search ?? '';
        $stat = $req->status ?? '';
        $date = $req->order_date ?? '';
        $restaurant_id = $req->restaurant ?? '';
       
        $restaurant = Restaurant::with('lang')->where('status', 'active')->where('deleted_at', NULL)->get();
      
        $customer = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->select('customer.id', 'customer.name','customer.phone_number')->groupBy('orders.customer_id')->get();

    
        $status = [1,2,3,4];
        $query = $this->order::with('customer')->with(['restaurant' => function ($q) {
            $q->with('lang');
        }])->whereIn('order_status', $status);

        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->order_date) {
            $date_new = date('Y-m-d', strtotime($req->order_date));
         
            $query->whereDate('created_at', $date_new);
        }
        if ($req->customer) {
            $query->where('customer_id', $req->customer);
        }
        if ($req->status) {
            $query->where('order_status', $req->status);
        }
        if ($req->restaurant) {
            $query->where('restaurant_id', $req->restaurant);
        }

        $orders = $query->orderBy('created_at', 'desc')->paginate(20);
        $order_status = Config::get('constants.frontdisk_order_status');
      
        return view('admin.orders.active_orders', compact('orders', 'order_status', 'search', 'customer', 'restaurant','date','cust_id','restaurant_id','stat'));
    }
    public function deliveredOrders(Request $req)
    {

        $cust_id = $req->customer ?? '';
        $search = $req->search;
        $stat = $req->status;
        $selected_region = $req->region;
        $date = $req->date;
        $restaurant_id = $req->restaurant ?? '';
        $dates =$req->order_date ?? '';

        $restaurant = Restaurant::with('lang')->where('status', 'active')->where('deleted_at', NULL)->get();
        $query = $this->order::with('customer')->where('order_status', "5");

        if ($req->search) {
            $query->where('order_id', 'like', "%" . $req->search . "%");
        }
        if ($req->order_date) {
            $date_new = date('Y-m-d', strtotime($req->order_date));
            $query->whereDate('created_at', $date_new);
        }
        if ($req->customer) {
            $query->where('customer_id', $req->customer);
        }
        if ($req->restaurant) {
            $query->where('restaurant_id', $req->restaurant);
        }
        $customer = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->select('customer.id', 'customer.name','customer.phone_number')->groupBy('orders.customer_id')->get();

        $orders = $query->orderBy('created_at', 'desc')->paginate(20);
        $order_status = Config::get('constants.frontdisk_order_status');
        return view('admin.orders.delivered_orders', compact('orders', 'order_status', 'restaurant','customer','restaurant_id','cust_id','dates'));
    }
}
