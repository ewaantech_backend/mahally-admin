<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Models\MenuCategory;
use App\Models\MenuItems;
use App\Models\Order;
use App\Models\Restaurant;
use App\Models\Customers;
use DatePeriod;
use DateTime;
use DateInterval;
use Validator;
use Config;
use DB;

class ReportsController extends Controller {

    //list customer
    public function index(Request $req) {

        $menu_category = MenuCategory::with('lang')->where(['status' => 'active'])->get();
        $menu_items = MenuItems::with('lang')->where(['status' => 'active'])->get();
        $restaurant = Restaurant::with('lang')->where(['status' => 'active'])->get();
        return view('admin.reports.index', compact('menu_category', 'menu_items', 'restaurant'));
    }

    public function salestab(Request $req) {
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        $filter_items = $req->filter_items;
        $filter_category = $req->filter_category;
        $fiter_restaurant = $req->fiter_restaurant;


        $query = DB::table('orders')
                ->select('menu_category.id as id', DB::raw('sum(distinct order_items.grant_total) as value'), 'menu_category_i18n.name as label', DB::raw('count(distinct orders.id) as sale_count'))
                ->join('order_items', 'orders.id', '=', 'order_items.order_id')
                ->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_category_i18n', 'menu_category_i18n.menu_cat_id', '=', 'menu_category.id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id')
                ->whereIn('orders.order_status', [5]);
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $categories = $query->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)
                        ->orderBy('sale_count', 'desc')
                        ->groupBy('menu_category.id')
                        ->limit(5)->get();
        $topcategories = $categories;

        // Applying categories filter
        $cats = $req->filter_category;
        $all_cat = $topcategories;
        if (!empty($topcategories) && !empty($cats)) {
            $all_cat = array();
            foreach ($topcategories as $key => $value) {
                if (in_array($value->id, $cats)) {
                    $all_cat[] = $value;
                }
            }
        }
        $all_filt_cat_final = $all_cat;


        $query = DB::table('orders')
                ->select('menu_items.id as id', DB::raw('sum(distinct order_items.grant_total) as value'), 'menu_items_i18n.name as label', DB::raw('count(distinct orders.id) as sale_count'))
                ->join('order_items', 'orders.id', '=', 'order_items.order_id')
                ->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id')
                ->join('menu_items_i18n', 'menu_items.id', '=', 'menu_items_i18n.menu_item_id')
                ->whereIn('orders.order_status', [5]);
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $top_items = $query->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)
                        ->orderBy('sale_count', 'desc')
                        ->limit(5)->get();
        
        $total_customers = Customers::where('status', 'active')->whereBetween('created_at', [$start, $end])->count();
        $query = DB::table('orders')->select(DB::raw('sum(orders.grand_total) as sum'))
                ->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id');
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $total_sale = $query->whereIn('orders.order_status', [5])->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)->first();
        $total_sale = $total_sale->sum ? $total_sale->sum : 0.00;

        $query = DB::table('orders')->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id');
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $total_orders = $query->whereIn('orders.order_status', [5])->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)->count();
        
        echo json_encode(['message' => 'Sales tab data fetched', 'top_categories' => $topcategories, 'all_categories' => $all_filt_cat_final, 'total_sale' => $total_sale,
            'total_orders' => $total_orders, 'total_customers' => $total_customers, 'top_items' => $top_items, 'status' => TRUE]);
        return;
    }

    public function linechart(Request $req) {

        $menu_category = MenuCategory::with('lang')->where(['status' => 'active'])->get();
        $menu_items = MenuItems::with('lang')->where(['status' => 'active'])->get();
        $restaurant = Restaurant::with('lang')->where(['status' => 'active'])->get();

        return view('admin.reports.linechart', compact('menu_category', 'menu_items', 'restaurant'));
    }

    public function linecharttab(Request $req) {
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        $filter_brand = $req->filter_brand;
        $filter_category = $req->filter_category;

        $date1 = date_create($start);
        $date2 = date_create($end);
        $diff = date_diff($date1, $date2);
        $datecount = $diff->format("%a");
        
        
        $total_customers = Customers::where('status', 'active')->whereBetween('created_at', [$start, $end])->count();
        $query = DB::table('orders')->select(DB::raw('sum(orders.grand_total) as sum'))
                ->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id');
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $total_sale = $query->whereIn('orders.order_status', [5])->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)->first();
        $total_sale = $total_sale->sum ? $total_sale->sum : 0.00;

        $query = DB::table('orders')->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id');
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $total_orders = $query->whereIn('orders.order_status', [5])->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)->count();
        
        
        $sql = DB::table('orders');
        if ($datecount > 31) {
            $sql->select('menu_category.id as id', DB::raw('sum(distinct order_items.grant_total) as value'), 'menu_category_i18n.name as label', DB::raw('DATE_FORMAT(orders.created_at,"%Y-%M") as date'), DB::raw('DATE_FORMAT(orders.created_at,"%Y%m") as sortcol'));
        } else {
            $sql->select('menu_category.id as id', DB::raw('sum(distinct order_items.grant_total) as value'), 'menu_category_i18n.name as label', DB::raw('DATE_FORMAT(orders.created_at,"%Y-%m-%d") as date'), DB::raw('DATE_FORMAT(orders.created_at,"%Y%m%d") as sortcol'));
        }
        $sql->join('order_items', 'orders.id', '=', 'order_items.order_id')
                ->join('restuarants', 'orders.restaurant_id', '=', 'restuarants.id')
                ->join('menu_category', 'restuarants.id', '=', 'menu_category.restuarants_id')
                ->join('menu_category_i18n', 'menu_category_i18n.menu_cat_id', '=', 'menu_category.id')
                ->join('menu_items', 'menu_category.id', '=', 'menu_items.menu_cate_id')
                ->whereIn('orders.order_status', [5]);
        if (!empty($filter_category)) {
            $query->whereIn('menu_category.id', $filter_category);
        }
        if (!empty($filter_items)) {
            $query->whereIn('menu_items.id', $filter_items);
        }
        if (!empty($fiter_restaurant)) {
            $query->whereIn('orders.restaurant_id', $fiter_restaurant);
        }
        $categories = $sql->whereDate('orders.created_at', '>=', $start)
                ->whereDate('orders.created_at', '<=', $end)
                ->groupBy('menu_category.id', 'orders.created_at')
                ->get();
        $topcategories = $categories;

        // Applying categories filter
        $cats = $req->filter_category;
        $all_cat = $topcategories;
        if (!empty($topcategories) && !empty($cats)) {
            $all_cat = array();
            foreach ($topcategories as $key => $value) {
                if (in_array($value->id, $cats)) {
                    $all_cat[] = $value;
                }
            }
        }
        $all_filt_cat_final = $all_cat;

        


        // Graph

        $linegraphdata = array();
        $linegraphdata_final = array();
        $yaxis = array();
//        $total_sale = 0;
        if (!empty($all_filt_cat_final)) {
            foreach ($all_filt_cat_final as $key => $value) {
                $linegraphdata[$value->date]['day'] = $value->date;
                $linegraphdata[$value->date]['sortcol'] = $value->sortcol;
                if (!isset($linegraphdata[$value->date][$value->label])) {
                    $linegraphdata[$value->date][$value->label] = 0;
                }
                if (!isset($linegraphdata[$value->date]['value'])) {
                    $linegraphdata[$value->date]['value'] = 0;
                }
                $linegraphdata[$value->date][$value->label] += $value->value;
                $linegraphdata[$value->date]['value'] += $value->value;
                array_push($yaxis, $value->label);
            }
            foreach ($linegraphdata as $key => $value) {
//                $total_sale += $value['value'];
                unset($value['value']);
                $linegraphdata_final[] = $value;
            }
            $yaxis = array_unique($yaxis);
        }

        $y_axis = array();
        if (!empty($yaxis)) {
            foreach ($yaxis as $key => $value) {
                array_push($y_axis, $value);
            }
        }

        $linegraph_data = array();
        if (!empty($linegraphdata_final)) {
            foreach ($linegraphdata_final as $key => $value) {
                foreach ($yaxis as $ykey => $yvalue) {
                    if (!isset($value[$yvalue])) {
                        $value[$yvalue] = 0;
                    } else {
                        $temp = $value[$yvalue];
                        unset($value[$yvalue]);
                        $value[$yvalue] = $temp;
                    }
                }
                // array_push($linegraph_data,$value);
                $linegraph_data[$value['day']] = $value;
            }
        }

        $lgraph_data = array();
        if (!empty($linegraph_data)) {
            if ($datecount > 31) {
                $temp_date_from = date("Y-F", strtotime($start));
                $temp_date_to = date("Y-F", strtotime($end));
                $month_array = array($temp_date_from);
                while ($temp_date_from != $temp_date_to) {
                    $temp_date_key = date("Ym", strtotime($temp_date_from . ' +1 months'));
                    $temp_date_from = date("Y-F", strtotime($temp_date_from . ' +1 months'));
                    $month_array[$temp_date_key] = $temp_date_from;
                }
                foreach ($month_array as $key => $value) {
                    if (isset($linegraph_data[$value])) {
                        array_push($lgraph_data, $linegraph_data[$value]);
                    } else {
                        $temp_array = array();
                        $temp_array['day'] = $value;
                        $sortcol = $key;
                        $temp_array['sortcol'] = $sortcol;
                        foreach ($y_axis as $ykey => $yvalue) {
                            $temp_array[$yvalue] = 0;
                        }
                        array_push($lgraph_data, $temp_array);
                    }
                }

                usort($lgraph_data, function ($a, $b) {
                    return $a['sortcol'] - $b['sortcol'];
                });
                foreach ($lgraph_data as $key => $value) {
                    unset($lgraph_data[$key]['sortcol']);
                }
            } else {
                if (!empty($linegraph_data)) {
                    $temp_date_to = date("Y-m-d", strtotime($end . ' +1 days'));
                    $period = new DatePeriod(
                            new DateTime($start), new DateInterval('P1D'), new DateTime($temp_date_to)
                    );
                    foreach ($period as $key => $value) {
                        $nowdate = $value->format('Y-m-d');
                        $sortcol = $value->format('Ymd');
                        if (isset($linegraph_data[$nowdate])) {
                            array_push($lgraph_data, $linegraph_data[$nowdate]);
                        } else {
                            $temp_array = array();
                            $temp_array['day'] = $nowdate;
                            $temp_array['sortcol'] = $sortcol;
                            foreach ($y_axis as $ykey => $yvalue) {
                                $temp_array[$yvalue] = 0;
                            }
                            array_push($lgraph_data, $temp_array);
                        }
                    }

                    usort($lgraph_data, function ($a, $b) {
                        return $a['sortcol'] - $b['sortcol'];
                    });
                    foreach ($lgraph_data as $key => $value) {
                        unset($lgraph_data[$key]['sortcol']);
                    }
                }
            }
        }

        echo json_encode(['message' => 'Sales line chart tab data fetched', 'total_sale' => $total_sale, 'total_orders' => $total_orders,
            'linegraph_ykeys' => $y_axis, 'linegraph_data' => $lgraph_data, 'total_customers' => $total_customers, 'status' => TRUE]);
        return;
    }

    public function hourlychart(Request $req) {

        return view('admin.reports.hourly');
    }

    public function hourlytab(Request $req) {
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');
        
        $total_sale = DB::table('orders')->whereIn('orders.order_status', [5])->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)->sum('orders.grand_total');


        $total_orders = DB::table('orders')->whereIn('orders.order_status', [5])->whereDate('orders.created_at', '>=', $start)
                        ->whereDate('orders.created_at', '<=', $end)->count('orders.id');
        
        $items = DB::table('orders')
                ->select(DB::raw('sum(order_items.grant_total) as sales'), DB::raw('DATE_FORMAT(orders.created_at,"%l %p") as time'), DB::raw('DATE_FORMAT(orders.created_at,"%H") as date'))
                ->join('order_items', 'orders.id', '=', 'order_items.order_id')
                ->whereIn('orders.order_status', [5])
                ->whereDate('orders.created_at', '>=', $start)
                ->whereDate('orders.created_at', '<=', $end)
                ->groupBy('date', 'time')
                ->get();

        $all_items = $items;
        $hourly_data = array();
//        $total_sale = 0;
        if (!empty($all_items)) {
            foreach ($all_items as $key => $value) {
                $times = explode(' ', $value->time);
                $right_time = $times[0] + 1;
                if ($right_time == 13) {
                    if ($times[1] == 'AM') {
                        $right_time = '1 AM';
                    } else {
                        $right_time = '1 PM';
                    }
                } elseif ($right_time == 12) {
                    if ($times[1] == 'AM') {
                        $right_time = '12 PM';
                    } else {
                        $right_time = '12 AM';
                    }
                } else {
                    $right_time = $right_time . '' . $times[1];
                }
                $newtime = str_replace(' ', '', $value->time) . ' - ' . str_replace(' ', '', $right_time);
                $value->time = $newtime;
                unset($value->date);
                //array_push($hourly_data,$value);
                $hourly_data[$newtime] = $value;
//                $total_sale += $value->sales;
            }
        }

        $timespan_array = array('12AM - 1AM',
            '1AM - 2AM',
            '2AM - 3AM',
            '3AM - 4AM',
            '4AM - 5AM',
            '5AM - 6AM',
            '6AM - 7AM',
            '7AM - 8AM',
            '8AM - 9AM',
            '9AM - 10AM',
            '10AM - 11AM',
            '11AM - 12PM',
            '12PM - 1PM',
            '1PM - 2PM',
            '2PM - 3PM',
            '3PM - 4PM',
            '4PM - 5PM',
            '5PM - 6PM',
            '6PM - 7PM',
            '7PM - 8PM',
            '8PM - 9PM',
            '9PM - 10PM',
            '10PM - 11PM',
            '11PM - 12AM');
        $hourly_data_final = array();
        foreach ($timespan_array as $key => $value) {
            if (isset($hourly_data[$value])) {
                array_push($hourly_data_final, $hourly_data[$value]);
            } else {
                array_push($hourly_data_final, array('sales' => 0, 'time' => $value));
            }
        }

        $total_customers = Customers::where('status', 'active')->whereBetween('created_at', [$start, $end])->count();

        echo json_encode(['message' => 'Hourly tab data fetched', 'total_sale' => $total_sale, 'total_orders' => $total_orders,
            'hourly_data' => $hourly_data_final, 'total_customers' => $total_customers, 'status' => TRUE]);
        return;
    }

}
