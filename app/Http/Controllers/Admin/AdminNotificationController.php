<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\AdminNotification;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class AdminNotificationController extends Controller
{
    public function get(Request $req)
    {
        $search = $req->search_note ?? '' ;
        $query =AdminNotification::with('lang');
        if ($search) {
            $query->whereHas('lang', function ($query) use ($search) {
                $query->where('title', 'like', "%" . $search . "%");
                $query->orWhere('content', 'like', "%" . $search . "%");
            });
        }
        $notification =  $query->paginate(20);
        return view('admin.notifications.admin_notification',compact('notification','search'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'title_en'=>'required',
            'notification_en'=>'required',
            'title_ar'=>'required',
            'notification_ar'=>'required'
        ],[
            'title_en.required'=>'Title(EN) required',
            'title_ar.required'=>'Title (AR) required',
            'notification_en.required'=>'Notification (EN) required',
            'notification_ar.required'=>'Notification (AR) required'

        ]);
        $note= DB::transaction(function () use ($request) {
            
            
             $note = new AdminNotification();
             $note->save();

            $note->lang()->createMany([
                [
                    'title' => $request->title_en,
                    'content'=>$request->notification_en,
                    'language' => 'en',
                ],
                [
                    'title' => $request->title_ar,
                    'content'=>$request->notification_ar,
                    'language' => 'ar',
                ],
            ]);

            return $note;
        });
       if($note)
       {
            return redirect()->route('admin-notification.get')->with('success','Notification added successfully');
       }else{
        return redirect()->route('admin-notification.get')->with('error','Erroe !!');
       }
    }

    public function delete(Request $req)
    {
        $del=AdminNotification::find($req->id);
        $del->lang()->delete();
        $del->delete();
        return response()->json(['status' => 1, 'message' => 'Notification deleted successfully']);
    }
}
