<?php

namespace App\Http\Controllers\Admin;

use App\Models\Membership;
use Illuminate\Http\Request;
use App\Models\MembershipLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class MembershipController extends Controller
{
    public function get(Request $request)
    {
        $membership = Membership::with('lang')->paginate(20);
        return view('admin.settings.membership.index', compact('membership'));
    }

    public function store(Request $request)
    {
        $member = DB::transaction(function () use ($request) {
            $member = Membership::create([
                'status' => 'active',
                'price_monthly' => $request->price_monthly,
                'price_yearly' => $request->price_yearly,
            ]);
            $member->lang()->createMany([
                [
                    'membership_name' => $request->title_en,
                    'language' => 'en',
                ],
                [
                    'membership_name' => $request->title_ar,
                    'language' => 'ar',
                ],
            ]);
            return $member;
        });
        if ($member) {
            return [
                "msg" => "success"
            ];
        } else {
            return [
                "msg" => "error"
            ];
        }
    }
    // public function statusUpdate(Request $request)
    // {

    //     $status = $request->status === 'Deactivate' ? 'deactive' : 'active';
    //     Faqs::where('id', $request->id)
    //         ->update([
    //             'status' => $status
    //         ]);
    //     return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    // }
    public function edit($id)
    {
        $member = Membership::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'member' => $member,
        ];
    }
    public function update(Request $request)
    {
        $faq = DB::transaction(function () use ($request) {
            Membership::where('id', $request->id)
                ->update([
                    'price_monthly' => $request->price_monthly,
                    'price_yearly' => $request->price_yearly,
                ]);
            MembershipLang::where('membership_id', $request->id)
                ->where('language', 'en')
                ->update(
                    [
                        'membership_name' => $request->title_en,
                    ]
                );
            MembershipLang::where('membership_id', $request->id)
                ->where('language', 'ar')
                ->update(
                    [
                        'membership_name' => $request->title_ar,
                    ]
                );
        });

        return [
            'msg' => "success"
        ];
    }
    public function statusUpdate(Request $request)
    {

        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';
        Membership::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        $faq = Membership::find($request->id);
        $faq->lang()->delete();
        $faq->delete();
        return response()->json(['status' => 1, 'message' => 'Membership deleted successfully']);
    }
}
