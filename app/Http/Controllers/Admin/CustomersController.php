<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Models\Customers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;

class CustomersController extends Controller
{
    public function get(Request $request)
    {
        $search = $request->customer_select;
        $search_field = $request->search_field ?? '';


        if ($search_field) {
            $data = Customers::select('id')->where('name', 'like', "%" . $search_field . "%")
                ->orWhere('phone_number', 'like', "%" . $request->search_field . "%")
                ->orWhere('email', 'like', "%" . $request->search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['id'];
            }
            $customers = Customers::whereIn('id', $da)->paginate(20);
        } else {
            $customers = Customers::paginate(20);
        }
        return view('admin.customers.index', compact('customers', 'search_field'));
    }


    public function statusUpdate(Request $request)
    {

        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';
        Customers::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function details(Request $req)
    {
        $customer_id = $req->id;
        $customer = Customers::where('id', $customer_id)->first();
        return view('admin.customers.customer_view', compact('customer'));
    }

    public function destroy(Request $request)
    {
        $faq = Customers::find($request->id);
        $faq->delete();
        return response()->json(['status' => 1, 'message' => 'Customer deleted successfully']);
    }
    public function search(Request $req)
    {
        $lang = Customers::where('name', 'like', "%" . $req->search . "%")
            ->orWhere('phone_number', 'like', "%" . $req->search . "%")
            ->orWhere('email', 'like', "%" . $req->search . "%")
            ->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->id, "label" => $lan->name);
        }

        return response()->json($response);
    }
    public function get_viewtabs(Request $req)
    {
        $customer_id = $req->id;

        if ($req->activeTab == 'BASIC INFO') {
            $customer_id = $req->id;
            $customer = Customers::where('id', $customer_id)->first();
            return view('admin.customers.customer_info_view', compact('customer'))->render();
        } elseif ($req->activeTab == 'ORDERS') {

            $order_status = Config::get('constants.frontdisk_order_status');
            $orders = Order::with('restaurant')->where('id', $customer_id)->paginate(20);

            return view('admin.customers.orders', compact('orders','order_status'))->render();
        }
    }
}
