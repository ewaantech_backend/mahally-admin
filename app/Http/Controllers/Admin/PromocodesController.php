<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Models\Promocodes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PromocodesController extends Controller
{
    public function get(Request $request)
    {
        $search = $request->promo_select;
        $search_field = $request->search_field ?? '';
        $discount_type = [
            'Amount',
            'Percentage',
        ];
        if ($search_field) {
            $data = Promocodes::select('id')->where('promocode', 'like', "%" . $search_field . "%")
                ->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['id'];
            }
            $promo = Promocodes::whereIn('id', $da)->paginate(20);
        } else {
            $promo = Promocodes::paginate(20);
        }
        return view('admin.promocodes.index', compact('promo', 'search_field', 'discount_type'));
    }

    public function store(Request $request)
    {
        $rules = [
            'promocode' => 'required|unique:promocodes,promocode',
        ];
        $messages = [
            'promocode.required' => 'Promocode is required.',
            'promocode.unique' => 'Promocode already exists'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {

        $promocode = DB::transaction(function () use ($request) {
            $promocode = Promocodes::create([
                'promocode' => $request->promocode,
                'type' => $request->type,
                'amount' => $request->amount,
                'expire_in' => date('Y-m-d H:i:s', strtotime($request->expire_in)),
                'max_usage' => $request->max_usage,
                'max_usage_per_user' => $request->max_usage_per_user,
                'status' => 'active',
            ]);

            return $promocode;
        });
        if ($promocode) {
            return response()->json(['status' => 1, 'message' => 'Promocode added successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
        }
    }
    public function statusUpdate(Request $request)
    {

        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';
        Promocodes::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function edit($id)
    {
        $promocode = Promocodes::where('id', $id)->first();
        return [
            'page' => $promocode,
        ];
    }
    public function update(Request $request)
    {
         $rules = [
            'promocode' => 'required|unique:promocodes,promocode,' . $request->id,
        ];
        $messages = [
            'promocode.required' => 'Promocode is required.',
            'promocode.unique' => 'Promocode already exists'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
        $promocode = DB::transaction(function () use ($request) {

            $promocode = Promocodes::where('id', $request->id)
                ->update([
                    'promocode' => $request->promocode,
                    'type' => $request->type,
                    'amount' => $request->amount,
                    'expire_in' =>  date('Y-m-d H:i:s', strtotime($request->expire_in)),
                    'max_usage' => $request->max_usage,
                    'max_usage_per_user' => $request->max_usage_per_user,
                ]);
            return $promocode;
        });

        if ($promocode) {
            return response()->json(['status' => 1, 'message' => 'Promocode updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
        }
    }

    public function destroy(Request $request)
    {
        $promocode = Promocodes::find($request->id);
        $promocode->delete();
        return response()->json(['status' => 1, 'message' => 'Promocode deleted successfully']);
    }
    public function search(Request $req)
    {
        $lang = Promocodes::where('promocode', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->id, "label" => $lan->promocode);
        }

        return response()->json($response);
    }
}
