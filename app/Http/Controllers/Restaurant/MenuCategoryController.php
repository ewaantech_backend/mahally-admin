<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Timing;
use App\Models\MenuCategory;
use Illuminate\Http\Request;
use App\Models\MenuCategoryLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\MenuCategoryAvailableTime;
use Illuminate\Support\Facades\Validator;

class MenuCategoryController extends Controller
{
    public function get(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $menus = MenuCategory::with('lang')->with('available_time')->where('restuarants_id', $rest_id)->orderBy('order', 'asc')->paginate(20);
        $timing = Timing::where('restuarants_id', $rest_id)->get();
        return view('restaurant.menu_category.index', compact('menus', 'rest_id', 'timing'));
    }
    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'calories' => 'required',
            // 'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'Menu category (EN) is required.',
            'title_ar.required' => 'Menu category (AR) is required.',
            'calories.required' => 'Calory is required.',
            // 'upload_image.max' => 'The image must be less than 1MB in size',
            // 'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/menu_category/resturant_' . $rest_id . '/', $image_name);
            } else {
                $image_name =  NULL;
            }
            $menu = DB::transaction(function () use ($request, $image_name, $rest_id) {
                $menu = MenuCategory::create([
                    // 'calories' => $request->calories,
                    'calories' => "calories",
                    'image_path' => $image_name,
                    'restuarants_id' => $rest_id,
                ]);
                $menu->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->title_ar,
                        'language' => 'ar',
                    ],
                ]);

                return $menu;
            });
            if ($menu) {
                foreach ($request->available_time as $time) {
                    $menuTime = MenuCategoryAvailableTime::create([
                        'time_id' => $time,
                        'menu_cat_id' => $menu->id
                    ]);
                }
            }

            $msg = "Menu category added successfully";
            if ($menu) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $menu = MenuCategory::with('lang')->with('available_time')->where('id', $id)->first();
        return [
            'menu' => $menu
        ];
    }
    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'calories' => 'required',
            // 'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'Menu category (EN) is required.',
            'title_ar.required' => 'Menu category (AR) is required.',
            'calories.required' => 'Calory is required.',
            // 'upload_image.max' => 'The image must be less than 1MB in size',
            // 'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        $rest_id = Auth::user()->restuarants_id;

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/menu_category/resturant_' . $rest_id . '/', $image_name);
            } else {
                $image_name =  NULL;
            }
            $menus = MenuCategory::where('id', $request->unique_id)
                ->update([
                    // 'calories' => $request->calories,
                    'calories' => "calories",
                    'image_path' => $image_name,
                ]);

            $menus = MenuCategoryLang::where('menu_cat_id', $request->unique_id)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                    ]
                );
            $menus = MenuCategoryLang::where('menu_cat_id', $request->unique_id)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                    ]
                );
            if ($menus) {
                MenuCategoryAvailableTime::where(['menu_cat_id' => $request->unique_id])->delete();
                foreach ($request->available_time as $time) {
                    $menuTime = MenuCategoryAvailableTime::create([
                        'time_id' => $time,
                        'menu_cat_id' => $request->unique_id
                    ]);
                }
            }
            $msg = "Menu Category updated successfully";
            if ($menus) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        MenuCategory::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }
    public function destroy(Request $request)
    {
        if ($request->id) {
            $menus = MenuCategory::find($request->id);
            if (!empty($menus)) {
                $menus->lang()->delete();
                $menus->available_time()->delete();
                $menus->delete();
                return response()->json(['status' => 1, 'message' => 'Menu category deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function sortable(Request $request)
    {
        $posts = MenuCategory::all();

        foreach ($posts as $post) {
            foreach ($request->order as $order) {
                if ($order['id'] == $post->id) {
                    $post->update(['order' => $order['position']]);
                }
            }
        }

        return response()->json(['status' => 1, 'message' => 'Menu category sorted successfully']);
    }
}
