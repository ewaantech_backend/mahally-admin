<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use App\Models\Customers;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RestaurantHomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $rest_id = Auth::user()->restuarants_id;
        // $total_sale =  Order::whereIn('orders.order_status', [5])->where('restaurant_id', $rest_id)
        //     ->sum('orders.grand_total');
        // $total_customers = Customers::where('status', 'active')->count();

        // $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->where('restaurant_id', $rest_id)->count();

        // $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");
        return view('restaurant.dashboard.index');
    }
    // public function getCount(Request $req)
    // {
    //     $rest_id = Auth::user()->restuarants_id;
    //     $start = Carbon::parse($req->start_date)->format('Y-m-d');
    //     $end = Carbon::parse($req->end_date)->format('Y-m-d');

    //     $total_sale =  Order::whereIn('orders.order_status', [5])->where('restaurant_id', $rest_id)->whereBetween('created_at', [$start, $end])
    //         ->sum('orders.grand_total');
    //     $total_customers = Customers::where('status', 'active')->whereBetween('created_at', [$start, $end])->count();

    //     $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->where('restaurant_id', $rest_id)->whereBetween('created_at', [$start, $end])->count();

    //     $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");
    //     return [
    //         'sales_count' => $sales_count,
    //         'total_customers' => $total_customers,
    //         'total_orders' => $total_orders,
    //     ];
    // }
}
