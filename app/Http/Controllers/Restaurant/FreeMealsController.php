<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\FreeMeals;
use App\Models\MenuItems;
use Illuminate\Http\Request;
use App\Models\FreeMealsLang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class FreeMealsController extends Controller
{
    public function index(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $free_meals = FreeMeals::with('lang')->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        $menuItemList = MenuItems::with('lang')->where('restuarants_id', $rest_id)->get();
        return view('restaurant.free_meals.index', compact('free_meals', 'menuItemList', 'rest_id'));
    }
    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'order' => 'required|numeric',
            'menu_item_id' => 'required',
            'expire_in' => 'required',
        ];
        $messages = [
            'title_en.required' => 'Title (EN) is required.',
            'title_ar.required' => 'Title (AR) is required.',
            'order.required' => 'Discount value is required.',
            'menu_item_id.required' => 'Item is required.',
            'expire_in.required' => 'Expire In is required.',
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $free_meal = DB::transaction(function () use ($request, $rest_id) {
                $free_meal = FreeMeals::create([
                    'restuarants_id' => $rest_id,
                    'status' => 'deactive',
                    'menu_item_id' => $request->menu_item_id,
                    'expire_in' => date('Y-m-d H:i:s', strtotime($request->expire_in)),
                    'order' => $request->order,
                ]);
                $free_meal->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'description' => $request->description_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->title_ar,
                        'description' => $request->description_ar,
                        'language' => 'ar',
                    ],
                ]);
                return $free_meal;
            });
            $msg = "Free meal loyalty added successfully";
            if ($free_meal) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $fee_meal = FreeMeals::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'fee_meal' => $fee_meal,

        ];
    }
    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'order' => 'required|numeric',
            'menu_item_id' => 'required',
            'expire_in' => 'required',
        ];
        $messages = [
            'title_en.required' => 'Title (EN) is required.',
            'title_ar.required' => 'Title (AR) is required.',
            'order.required' => 'Discount value is required.',
            'menu_item_id.required' => 'Item is required.',
            'expire_in.required' => 'Expire In is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {


            $free_meal = FreeMeals::where('id', $request->id_pg)
                ->update([
                    'menu_item_id' => $request->menu_item_id,
                    'expire_in' => date('Y-m-d H:i:s', strtotime($request->expire_in)),
                    'order' => $request->order,
                ]);

            $free_meal = FreeMealsLang::where('free_meal_id', $request->id_pg)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                        'description' => $request->description_en,
                    ]
                );
            $free_meal = FreeMealsLang::where('free_meal_id', $request->id_pg)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                        'description' => $request->description_ar,
                    ]
                );

            $msg = "Free meal loyalty updated successfully";
            if ($free_meal) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        FreeMeals::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        if ($request->id) {
            $offer = FreeMeals::find($request->id);
            if (!empty($offer)) {
                $offer->lang()->delete();
                $offer->delete();
                return response()->json(['status' => 1, 'message' => 'Free meal loyalty deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
