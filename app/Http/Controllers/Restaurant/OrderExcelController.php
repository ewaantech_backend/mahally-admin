<?php

namespace App\Http\Controllers\Restaurant;


use Illuminate\Http\Request;
use App\Exports\ActiveOrderExport;
use App\Exports\DelveredOrderExport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

class OrderExcelController extends Controller
{
    public function downloadExcelActive()
    {
        return Excel::download(new ActiveOrderExport,'Orders.xlsx');
    }
    public function downloadExcelDelivered()
    {
        return Excel::download(new DelveredOrderExport,'Orders.xlsx');
    }
}
