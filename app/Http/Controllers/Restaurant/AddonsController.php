<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\AutoSuggest;
use Illuminate\Http\Request;
use App\Models\RestaurantAddons;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\RestaurantAddonsLang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AddonsController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->addons_select;
        $search_field = $request->search_field ?? '';
        $rest_id = Auth::user()->restuarants_id;
        if ($search_field) {
            $data = RestaurantAddonsLang::select('addon_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['addon_id'];
            }
            $addons = RestaurantAddons::with('lang')->whereIn('id', $da)->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $addons = RestaurantAddons::with('lang')->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        }
        // $lang = 'en';
        // $addonsMainList = AutoSuggest::with(['lang' => function ($q) use ($lang) {
        //     $q->where('language', $lang)->orderBy('name', 'asc');
        // }])->where('type', 'addons')->where('status', 'active')->where('deleted_at', NULL)->get();
        $addonsMainList = AutoSuggest::leftJoin('autosuggest_i18n', 'autosuggest.id', 'autosuggest_i18n.autosuggest_id')->select('autosuggest.id', 'autosuggest_i18n.name')->where('autosuggest.type', 'addons')->where('autosuggest.status', 'active')->where('autosuggest_i18n.language', 'en')->where('autosuggest.deleted_at', NULL)->orderBy('autosuggest_i18n.name', 'asc')->get();
        $addonsExist = RestaurantAddons::select('autosuggest_id')->where('restuarants_id', $rest_id)->where('type', 'From Main List')->get()->toArray();
        $addons_data = array();
        foreach ($addonsExist as  $addon_data) {
            $addons_data[] = $addon_data['autosuggest_id'];
        }
        return view('restaurant.addons.index', compact('addons', 'search_field', 'addonsMainList', 'addons_data', 'addonsExist', 'rest_id'));
    }

    public function store(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'price' => 'required|numeric',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'title_ar.required' => 'This filed is required.',
            'price.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 1MB in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/addons/', $image_name);
            } else {
                $image_name =  NULL;
            }

            $addons = DB::transaction(function () use ($request, $image_name, $rest_id) {
                $addons = RestaurantAddons::create([
                    'status' => 'deactive',
                    'type' => 'By Restaurant',
                    'image_path' => $image_name,
                    'restuarants_id' => $rest_id,
                    'price' => $request->price,
                ]);
                $addons->lang()->createMany([
                    [
                        'name' => $request->title_en,
                        'language' => 'en',
                    ],
                    [
                        'name' => $request->title_ar,
                        'language' => 'ar',
                    ],
                ]);
                return $addons;
            });
            $msg = "Addons added successfully";
            if ($addons) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function storeMainList(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $price = array_values(array_filter($request->price));
        // $price =  array_filter($request->price);
        $addonsExist = RestaurantAddons::where('restuarants_id', $rest_id)->where('type', 'From Main List')->delete();
        if ($request->addons) {
            foreach ($request->addons as $key => $item) {
                $addonsMainList = AutoSuggest::with('lang')->where('id', $item)->first();
                $addonsNew = DB::transaction(function () use ($request, $addonsMainList, $key, $item, $rest_id, $price) {
                    $addonsNew = RestaurantAddons::create([
                        'status' => 'active',
                        'type' => 'From Main List',
                        'restuarants_id' => $rest_id,
                        'autosuggest_id' => $item,
                        'price' => $price[$key],
                        'image_path' => $addonsMainList->image_path,
                    ]);
                    $addonsNew->lang()->createMany([
                        [
                            'name' => $addonsMainList->lang[0]->name,
                            'language' => 'en',
                        ],
                        [
                            'name' => $addonsMainList->lang[1]->name,
                            'language' => 'ar',
                        ],
                    ]);
                    return $addonsNew;
                });
            }
            $msg = "Addons added successfully";
            if ($addonsNew) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        } else {
            return response()->json(['status' => 1, 'message' => 'Addons updated successfully']);
        }
    }
    public function edit($id)
    {
        $addons = RestaurantAddons::with('lang')
            ->where('id', $id)
            ->first();
        return [
            'page' => $addons,

        ];
    }

    public function update(Request $request)
    {
        $rules = [
            'title_en' => 'required',
            'title_ar' => 'required',
            'price' => 'required|numeric',
            'upload_image' => 'image|mimes:png,jpg,jpeg|max:2048',
        ];
        $messages = [
            'title_en.required' => 'This filed is required.',
            'title_ar.required' => 'This filed is required.',
            'price.required' => 'This filed is required.',
            'upload_image.max' => 'The image must be less than 1MB in size',
            'upload_image.mimes' => "The image must be of the format jpeg or png",
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            if (!empty($request->file('upload_image'))) {

                $file = $request->file('upload_image');
                $image_name = $file->getClientOriginalName() . '.' . $file->getClientOriginalExtension();
                $path_en = $file->move(public_path() . '/uploads/restaurant/addons/', $image_name);
            } else {
                $image_name =  NULL;
            }
            $addons = RestaurantAddons::where('id', $request->id_pg)
                ->update([
                    'image_path' => $image_name,
                    'price' => $request->price,
                ]);

            $addons = RestaurantAddonsLang::where('addon_id', $request->id_pg)
                ->where('language', 'en')
                ->update(
                    [
                        'name' => $request->title_en,
                    ]
                );
            $addons = RestaurantAddonsLang::where('addon_id', $request->id_pg)
                ->where('language', 'ar')
                ->update(
                    [
                        'name' => $request->title_ar,
                    ]
                );

            $msg = "Addon updated successfully";
            if ($addons) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function statusUpdate(Request $request)
    {
        $status = $request->status === 'Deactivate' ? 'deactive' : 'active';

        RestaurantAddons::where('id', $request->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $request)
    {
        if ($request->id) {
            $addons = RestaurantAddons::find($request->id);
            if (!empty($addons)) {
                $addons->lang()->delete();
                $addons->delete();
                return response()->json(['status' => 1, 'message' => 'Addon deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function search(Request $req)
    {
        $lang = RestaurantAddonsLang::where('name', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->addons_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
