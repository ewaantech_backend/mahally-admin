<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Reviews;
use App\Models\Customers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ReviewsController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->customer;
        $search_field = $request->search_field ?? '';
        $customer = Customers::RightJoin('reviews', 'reviews.customer_id', 'customer.id')->select('customer.id', 'customer.name')->groupBy('reviews.customer_id')->get();

        $rest_id = Auth::user()->restuarants_id;
        if ($search) {
            $reviews = Reviews::with('customer')->where('restaurant_id', $rest_id)->where('customer_id', $request->customer)->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $reviews = Reviews::with('customer')->where('restaurant_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        }
        return view('restaurant.reviews.index', compact('reviews', 'search_field', 'rest_id', 'customer'));
    }
}
