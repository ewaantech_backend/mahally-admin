<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Timing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TimingController extends Controller
{
    public function get(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;
        $time = Timing::where('restuarants_id', '=', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        return view('restaurant.timing.index', compact('time'));
    }
    public function store(Request $req)
    {
        $rules = [
            'start_time' => 'required',
            'end_time' => 'required',
        ];
        $messages = [
            'start_time.required' => 'Start time is required.',
            'end_time.required' => 'End time is required.',
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $tableData = Timing::create([
                'start_time' => $req->start_time,
                'end_time' => $req->end_time,
                'restuarants_id' => $rest_id,
            ]);
            $msg = "Timing added successfully";
            if ($tableData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $time = Timing::where('id', $id)->first();
        return [
            'time' => $time
        ];
    }
    public function update(Request $req)
    {
        $rules = [
            'start_time' => 'required',
            'end_time' => 'required',
        ];
        $messages = [
            'start_time.required' => 'Start time is required.',
            'end_time.required' => 'End time is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $userData = Timing::where('id', $req->time_unique_id)
                ->update([
                    'start_time' => $req->start_time,
                    'end_time' => $req->end_time,
                ]);
            $msg = "Timing  updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }
    public function statusUpdate(Request $req)
    {
        if ($req->status == 'Deactivate') {
            $status = 'deactive';
        } else {
            $status = 'active';
        }

        $statusUpdate = Timing::where('id', $req->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $req)
    {
        if ($req->id) {
            $timing = Timing::find($req->id);
            if (!empty($timing)) {
                $timing->delete();
                return response()->json(['status' => 1, 'message' => 'Timing deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
}
