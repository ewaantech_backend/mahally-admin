<?php

namespace App\Http\Controllers\Restaurant;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\RestaurantsContactInfo;
use Illuminate\Support\Facades\Validator;

class ChangePassword extends Controller
{
    public function changePassword()
    {
        return view('restaurant.password.index');
    }
    public function updatePassword(Request $request)
    {
        $rules = [
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password' => 'min:6'
        ];
        $messages = [
            'password.required' => 'Password required',
            'confirm_password.required' => 'Confirm password required'
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            RestaurantsContactInfo::where('restuarants_id', Auth::user()->restuarants_id)
                ->update([
                    'password' => Hash::make($request->password)
                ]);
            return response()->json(['status' => 1, 'message' => 'Password updated successfully']);
        }
    }
}
