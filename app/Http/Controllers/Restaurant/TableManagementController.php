<?php

namespace App\Http\Controllers\Restaurant;

use PDF;
use Illuminate\Http\Request;
use App\Models\TableManagement;
use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class TableManagementController extends Controller
{
    public function get(Request $req)
    {
        $search = $req->search ?? '';
        $rest_id = Auth::user()->restuarants_id;
        if ($search) {
            $query = TableManagement::query();
            if ($search) {
                $query->where(function ($sub) use ($search) {
                    $sub->where('table_number', 'like', "%" . $search . "%");
                });
            }
            $table = $query->where('restuarants_id', '=', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
            return view('restaurant.table_management.index', compact('table', 'search'));
        } else {
            $table = TableManagement::where('restuarants_id', '=', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
            return view('restaurant.table_management.index', compact('table', 'search'));
        }
    }
    public function store(Request $req)
    {
        $rules = [
            'table_number' => 'required',
            'table_color' => 'required',
        ];
        $messages = [
            'table_number.required' => 'Table number is required.',
            'table_color.required' => 'Table color is required.',
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $tableData = TableManagement::create([
                'table_number' => $req->table_number,
                'table_color' => $req->table_color,
                'restuarants_id' => $rest_id,
            ]);
            $msg = "Table added successfully";
            if ($tableData) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }
    public function edit($id)
    {
        $table = TableManagement::where('id', $id)->first();
        return [
            'table' => $table
        ];
    }
    public function update(Request $req)
    {
        $rules = [
            'table_number' => 'required',
            'table_color' => 'required',
        ];
        $messages = [
            'table_number.required' => 'Table number is required.',
            'table_color.required' => 'Table color is required.',
        ];
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {

            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $userData = TableManagement::where('id', $req->table_unique_id)
                ->update([
                    'table_number' => $req->table_number,
                    'table_color' => $req->table_color,
                ]);
            $msg = "Table  updated successfully";
            return response()->json(['status' => 1, 'message' => $msg]);
        }
    }
    public function statusUpdate(Request $req)
    {
        if ($req->status == 'Deactivate') {
            $status = 'deactive';
        } else {
            $status = 'active';
        }

        $statusUpdate = TableManagement::where('id', $req->id)
            ->update([
                'status' => $status
            ]);
        return response()->json(['status' => 1, 'message' => "Status updated successfully"]);
    }

    public function destroy(Request $req)
    {
        if ($req->id) {
            $table = TableManagement::find($req->id);
            if (!empty($table)) {
                $table->delete();
                return response()->json(['status' => 1, 'message' => 'table deleted successfully']);
            }
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }
    public function search(Request $req)
    {
        $table = TableManagement::where('table_number', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($table as $lan) {
            $response[] = array("value" => $lan->id, "label" => $lan->table_number);
        }

        return response()->json($response);
    }
    public function TableCodeGeneration($table)
    {
        $rest_id = Auth::user()->restuarants_id;
        $data = TableManagement::where('id', $table)->first();

        $target_dir_path = "public/upload/qrcodes/table";
        $target_orgin_dir_path = "/upload/qrcodes/table";
        if (!File::isDirectory($target_dir_path)) {
            File::makeDirectory($target_dir_path, 0777, true, true);
        }

        $color = $data['table_color'];
        $tablecode = "Restaurant iD :" . $rest_id . "  Table No. :" . $table  . " Color : " . $color;
        // $invEncode = "Restaurant-" . $rest_id . ",Table-" . $table . ",Color- <input type='button' class='tbl-btn' style='background-color: $color;'>";
        $path = '/uploads/table/qrcode';

        $createfilepath = $target_dir_path . '/' . $tablecode;

        $filepath = $target_orgin_dir_path . '/qr_' . $tablecode;

        // QrCode::backgroundColor(255, 255, 0)
        //     ->format('png')
        //     ->size(300)
        //     ->generate($tablecode, $createfilepath);
        // ini_set('max_execution_time', 300);
        // $pdf = PDF::loadView('restaurant.table_management.qr-code', compact('createfilepath'));
        // return $pdf->stream('table-qrcode.pdf');
        $rest_data = Restaurant::with('lang')->with('contact')->where('id', $rest_id)->first();
        $pdf = PDF::loadView('restaurant.table_management.qr-code', compact('tablecode', 'rest_data', 'data'));
        return $pdf->stream();
    }
}
