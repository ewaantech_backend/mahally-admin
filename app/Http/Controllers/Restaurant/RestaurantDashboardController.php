<?php

namespace App\Http\Controllers\Restaurant;

use App\Models\Order;
use App\Models\Customers;
use App\Models\OrderItems;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class RestaurantDashboardController extends Controller
{
    public function get()
    {
        $rest_id = Auth::user()->restuarants_id;
        $total_sale =  Order::whereIn('orders.order_status', [5])->where('restaurant_id', $rest_id)->sum('orders.grand_total');
        $total_customer = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->where('customer.status', 'active')->where('orders.restaurant_id', $rest_id)->groupBy('orders.customer_id')->get();
        $total_customers = count($total_customer);

        $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->where('restaurant_id', $rest_id)->where('deleted_at', NULL)->count();
        $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");

        $order_status = Config::get('constants.frontdisk_order_status');
        $status = [1,2,3,4];
        $recent_order = Order::with('customer')->where('restaurant_id', $rest_id)->whereIn('order_status', $status)->orderBy('created_at', 'desc')->limit(5)->get();

        $order_item = OrderItems::whereHas('menu_items', function ($q) use($rest_id){ 
            $q->with('lang:menu_item_id,name');
            $q->where('restuarants_id', $rest_id);
        })->with(['menu_items' => function ($q) {
            $q->with('lang:menu_item_id,name');
        }])
            ->with(['order' => function ($q) {
                $q->with(['restaurant' => function ($p) {
                    $p->with('lang:restuarants_id,language,name');
                }]);
            }])
            ->groupBy('item_id')
            ->selectRaw('order_id,item_id, sum(item_count) as item_count')->orderBy('item_count', 'desc')->limit(5)
            ->get();

        return view('restaurant.dashboard.index', compact('total_customers', 'total_orders', 'sales_count', 'recent_order', 'order_status', 'order_item'));
    }
    public function getCount(Request $req)
    {
        $rest_id = Auth::user()->restuarants_id;
        $start = Carbon::parse($req->start_date)->format('Y-m-d');
        $end = Carbon::parse($req->end_date)->format('Y-m-d');

        $total_sale =  Order::whereIn('orders.order_status', [5])->where('restaurant_id', $rest_id)->whereBetween('created_at', [$start, $end])
            ->sum('orders.grand_total');
        $total_customers = Customers::RightJoin('orders', 'orders.customer_id', 'customer.id')->where('customer.status', 'active')->whereBetween('orders.created_at', [$start, $end])->groupBy('customer.id')->count();

        $total_orders = Order::whereIn('order_status', [1, 2, 3, 4, 5])->where('restaurant_id', $rest_id)->whereBetween('created_at', [$start, $end])->count();

        $sales_count = 'SAR ' . number_format($total_sale, 2, ".", "");
        return [
            'sales_count' => $sales_count,
            'total_customers' => $total_customers,
            'total_orders' => $total_orders,
        ];
    }
}
