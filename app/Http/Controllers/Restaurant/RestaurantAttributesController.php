<?php

namespace App\Http\Controllers\Restaurant;

use Attribute;
use App\Models\Attributes;
use Illuminate\Http\Request;
use App\Models\AttributesValue;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\RestaurantAttributes;
use Illuminate\Support\Facades\Auth;
use App\Models\RestaurantAttributesLang;
use Illuminate\Support\Facades\Validator;
use App\Models\RestaurantAttributesValues;
use App\Models\RestaurantAttributesValuesLang;

class RestaurantAttributesController extends Controller
{
    public function get(Request $request)
    {
        $search = $request->faq_select;
        $search_field = $request->search_field ?? '';
        $rest_id = Auth::user()->restuarants_id;

        $attributeLang = RestaurantAttributesLang::where('language', 'en')->get();

        if ($search_field) {
            $data = RestaurantAttributesLang::select('attribute_id')->where('name', 'like', "%" . $search_field . "%")->get()->toArray();
            $da = [];
            foreach ($data as  $d) {
                $da[] = $d['attribute_id'];
            }
            $attribute = RestaurantAttributes::with('lang')->with('attr_value')->whereIn('id', $da)->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        } else {
            $attribute = RestaurantAttributes::with('lang')->with('attr_value')->where('restuarants_id', $rest_id)->orderBy('created_at', 'desc')->paginate(20);
        }
        $attributesMainList = Attributes::leftJoin('attributes_i18n', 'attributes.id', 'attributes_i18n.attribute_id')->select('attributes.id', 'attributes_i18n.name')->where('attributes.status', 'active')->where('attributes_i18n.language', 'en')->where('attributes.deleted_at', NULL)->orderBy('attributes_i18n.name', 'asc')->get();
        $attributesExist = RestaurantAttributes::select('attribute_id')->where('restuarants_id', $rest_id)->where('type', 'From Main List')->get()->toArray();
        $attr_data = array();
        foreach ($attributesExist as  $attri_data) {
            $attr_data[] = $attri_data['attribute_id'];
        }


        return view('restaurant.attributes.index', compact('attribute', 'attributeLang', 'search_field', 'attributesMainList', 'attr_data'));
    }
    public function create(Request $req)
    {
        $_attribute_id = $req->id;
        $row_data = $lang_array = array();
        if ($_attribute_id != '') {
            $row_data = RestaurantAttributes::with('lang')->where('id', $req->id)->first();
            $variant = RestaurantAttributesValues::leftJoin('restaurant_attribute_values_i18n', 'restaurant_attribute_values_i18n.rest_attribute_value_id', '=', 'restaurant_attribute_values.id')
                ->select('restaurant_attribute_values_i18n.id', 'restaurant_attribute_values_i18n.name', 'restaurant_attribute_values_i18n.rest_attribute_value_id as attr_id')
                ->where('restaurant_attribute_values.rest_attribute_id', '=', $row_data['id'])
                ->where('restaurant_attribute_values_i18n.language', '=', 'en')
                ->where('restaurant_attribute_values_i18n.deleted_at', NULL)
                ->where('restaurant_attribute_values.deleted_at', NULL)
                ->get(['id', 'attr_id', 'name']);
            foreach ($variant as $val) {
                $var_ar = RestaurantAttributesValuesLang::where(['en_matching_id' => $val['id'], 'rest_attribute_value_id' => $val['attr_id'], 'language' => 'ar', 'deleted_at' => NULL])->first();
                $variant_array['en_name'] = $val['name'];
                $variant_array['en_variant_id'] = $val['id'];
                $variant_array['ar_name'] = $var_ar['name'];
                $variant_array['ar_variant_id'] = $var_ar['id'];
                $lang_array[] = $variant_array;
            }
        }

        $data = [
            'row_data' => $row_data,
            'variant_data' => $lang_array,
        ];

        return view('restaurant.attributes.create', $data);
    }

    public function save(Request $req)
    {
        $rules = [
            'name_en' => 'required',
            'name_ar' => 'required',
        ];
        $messages = [
            'name_en.required' => 'Name(EN) is required.',
            'name_ar.required' => 'Name(AR) is required.',
        ];
        $rest_id = Auth::user()->restuarants_id;
        $validator = Validator::make($req->all(), $rules, $messages);
        if (!$validator->passes()) {
            return response()->json(['status' => 0, 'message' => $validator->errors()->first()]);
        } else {
            $variant_array = ([
                [
                    'name' => $req->att_value_en,
                    'language' => 'en',
                ],
                [
                    'name' => $req->att_value_ar,
                    'language' => 'ar',
                ],
            ]);
            if (empty($req->_attribute_id)) {
                $attribute = DB::transaction(function () use ($req, $rest_id) {
                    $attribute = RestaurantAttributes::create([
                        'status' => 'active',
                        'is_mandatory' => $req->is_mandatory,
                        'type' => 'By Restaurant',
                        'restuarants_id' => $rest_id,
                    ]);
                    $attribute->lang()->createMany([
                        [
                            'name' => $req->name_en,
                            'language' => 'en',
                        ],
                        [
                            'name' => $req->name_ar,
                            'language' => 'ar',
                        ],
                    ]);

                    return $attribute;
                });


                $variant = DB::transaction(function () use ($req, $attribute) {
                    foreach ($req->att_value_en as $key => $var_value) {
                        $variant = RestaurantAttributesValues::create([
                            'rest_attribute_id' => $attribute->id,
                        ]);

                        $variant_lang = RestaurantAttributesValuesLang::create([
                            'rest_attribute_value_id' => $variant->id,
                            'name' => $var_value,
                            'language' => 'en',
                        ]);
                        RestaurantAttributesValuesLang::create([
                            'rest_attribute_value_id' => $variant->id,
                            'en_matching_id' => $variant_lang->id,
                            'name' => $req->att_value_ar[$key],
                            'language' => 'ar',
                        ]);
                    }
                    return $variant;
                });
                $msg = "Attribute added successfully";
            } else {
                $get_attribute = RestaurantAttributes::where('id', $req->_attribute_id)->first();

                $attribute = DB::transaction(function () use ($req) {

                    $attribute = RestaurantAttributes::where('id', $req->_attribute_id)
                        ->update([
                            'is_mandatory' => $req->is_mandatory,
                        ]);

                    RestaurantAttributesLang::where('language', 'en')
                        ->where('rest_attribute_id', $req->_attribute_id)
                        ->update([
                            'name' => $req->name_en,
                        ]);
                    RestaurantAttributesLang::where('language', 'ar')
                        ->where('rest_attribute_id', $req->_attribute_id)
                        ->update([
                            'name' => $req->name_ar,
                        ]);

                    return $attribute;
                });
                $variant = DB::transaction(function () use ($req) {
                    $attr = RestaurantAttributesValues::where('rest_attribute_id', $req->_attribute_id)->first();
                    if (!empty($attr)) {
                        RestaurantAttributesValuesLang::where('rest_attribute_value_id', $attr->id)->delete();
                        RestaurantAttributesValues::where('id', $attr->id)->delete();
                    }

                    foreach ($req->att_value_en as $key => $var_value) {
                        $variant = RestaurantAttributesValues::create([
                            'rest_attribute_id' => $req->_attribute_id,
                        ]);
                        $variant_lang = RestaurantAttributesValuesLang::create([
                            'rest_attribute_value_id' => $variant->id,
                            'name' => $var_value,
                            'language' => 'en',
                        ]);
                        RestaurantAttributesValuesLang::create([
                            'rest_attribute_value_id' => $variant->id,
                            'en_matching_id' => $variant_lang->id,
                            'name' => $req->att_value_ar[$key],
                            'language' => 'ar',
                        ]);
                    }
                    return $variant;
                });

                $msg = "Attribute updated successfully";
            }
            if ($attribute) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        }
    }

    public function storeMainList(Request $request)
    {
        $rest_id = Auth::user()->restuarants_id;
        $addonsExist = RestaurantAttributes::where('restuarants_id', $rest_id)->where('type', 'From Main List')->delete();
        if ($request->attr) {
            foreach ($request->attr as $key => $item) {
                $attrMainList = Attributes::with('lang')->where('id', $item)->first();
                $attriNew = DB::transaction(function () use ($request, $attrMainList, $key, $item, $rest_id) {
                    $attriNew = RestaurantAttributes::create([
                        'status' => 'active',
                        'type' => 'From Main List',
                        'restuarants_id' => $rest_id,
                        'attribute_id' => $item,
                    ]);
                    $attriNew->lang()->createMany([
                        [
                            'name' => $attrMainList->lang[0]->name,
                            'language' => 'en',
                        ],
                        [
                            'name' => $attrMainList->lang[1]->name,
                            'language' => 'ar',
                        ],
                    ]);
                    return $attriNew;
                });

                $attrMainValueList = AttributesValue::with('lang')->where('attribute_id', $item)->get();
                foreach ($attrMainValueList as $key => $var_value) {
                    $variant = DB::transaction(function () use ($attrMainValueList, $attriNew, $var_value) {
                        $variant = RestaurantAttributesValues::create([
                            'rest_attribute_id' => $attriNew->id,
                        ]);

                        $variant_lang = RestaurantAttributesValuesLang::create([
                            'rest_attribute_value_id' => $variant->id,
                            'name' => $var_value->lang[0]->name,
                            'language' => 'en',
                        ]);
                        RestaurantAttributesValuesLang::create([
                            'rest_attribute_value_id' => $variant->id,
                            'en_matching_id' => $variant_lang->id,
                            'name' => $var_value->lang[1]->name,
                            'language' => 'ar',
                        ]);

                        return $variant;
                    });
                }
            }

            $msg = "Attributes added successfully";
            if ($attriNew) {
                return response()->json(['status' => 1, 'message' => $msg]);
            } else {
                return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
            }
        } else {
            return response()->json(['status' => 1, 'message' => 'Attributes updated successfully']);
        }
    }

    public function activate(Request $req)
    {
        $att = RestaurantAttributes::where('id', $req->id)->first();
        if ($att) {
            if ($att->status == 'deactive') {
                RestaurantAttributes::where('id', $req->id)
                    ->update([
                        'status' => 'active'
                    ]);
            } else {
                RestaurantAttributes::where('id', $req->id)
                    ->update([
                        'status' => 'deactive'
                    ]);
            }
            return response()->json(['status' => 1, 'message' => 'Status updated successfully']);
        } else {
            return response()->json(['status' => 0, 'message' => 'Sorry something went wrong.']);
        }
    }



    public function destroy(Request $request)
    {
        $attr = RestaurantAttributes::find($request->id);
        $attr->lang()->delete();
        $attr->delete();
        return response()->json(['status' => 1, 'message' => 'Attribute deleted successfully']);
    }
    public function search(Request $req)
    {
        $lang = RestaurantAttributesLang::where('name', 'like', "%" . $req->search . "%")->where('deleted_at', NULL)->get();
        $response = array();
        foreach ($lang as $lan) {
            $response[] = array("value" => $lan->faq_id, "label" => $lan->name);
        }

        return response()->json($response);
    }
}
