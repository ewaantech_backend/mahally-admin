<?php

namespace App\Http\Requests\Api\Userapp;

use Illuminate\Foundation\Http\FormRequest;

class RestaurentDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "latitude" =>'nullable|numeric',
            "longitude" =>"nullable|numeric",
            "cust_id"=>"nullable"
        ];
    }
}
