<?php

namespace App\Http\Requests\Api\Userapp;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"=>'required',
            "email"=>['required',
                'email',
                Rule::unique('customer', 'email')->ignore(auth()->id()),
            ],
            "photo"=>'nullable|image|mimes:png,jpg,jpeg',
            "ph_in_order"=>'required',
            "ph_in_review"=>'required'
            
        ];
    }
}
