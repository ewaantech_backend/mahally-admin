<?php

namespace App\Http\Requests\Api\Userapp;

use Illuminate\Foundation\Http\FormRequest;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id'=>'required',
            'restaurant_id'=>'required',
            'order_type'=>'required',
            'item_count'=>'required',
            'payment_type'=>'required',
            'order_items'=>'nullable',
            'promo_code'=>"nullable",
            'table_id'=>'nullable'
        ];
    }
}
