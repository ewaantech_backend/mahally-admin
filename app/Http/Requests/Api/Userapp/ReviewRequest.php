<?php

namespace App\Http\Requests\Api\Userapp;

use Illuminate\Foundation\Http\FormRequest;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'review' => 'required|array',
            'review.*.segment_id' => 'required|exists:autosuggest,id',
            'review.*.rating' => 'required|integer|max:5|min:1',
            'order_id'=>'nullable',
            'restaurant'=>'nullable',
            'message'=>'nullable'
            
        ];
    }
}
