<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantAttributes extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "restaurant_attributes";

    public function lang()
    {
        return $this->hasMany(RestaurantAttributesLang::class, 'rest_attribute_id');
    }
    public function attr_value()
    {
        return $this->hasOne(RestaurantAttributesValues::class, 'rest_attribute_id');
    }
}
