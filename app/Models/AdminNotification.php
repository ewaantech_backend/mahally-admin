<?php

namespace App\Models;

use App\Models\AdminNotificationLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminNotification extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */
    
    protected $table="admin_notifications";
    
    public function lang()
    {
        return $this->hasMany(AdminNotificationLang::class, 'notification_id');
    }
}
