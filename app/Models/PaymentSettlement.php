<?php

namespace App\Models;

use App\Models\Order;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentSettlement extends Model
{
//    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "payment_settlement";
    
    public function order()
    {
        return $this->hasMany(Order::class, 'restaurant_id', 'restaurant_id');
    }
    public function restaurant()
    {
        return $this->hasMany(Restaurant::class, 'restaurant_id');
    }
    public function history()
    {
        return $this->hasMany(SettlementHistory::class, 'settlement_id');
    }
}
