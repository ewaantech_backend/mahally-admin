<?php

namespace App\Models;

use App\Models\CancellationReasonLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CancellationReason extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "cancellation_reason";

    public function lang()
    {
        return $this->hasMany(CancellationReasonLang::class, 'cancellation_id');
    }
}
