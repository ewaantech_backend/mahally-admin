<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RestaurantAttributesLang extends Model
{
    use SoftDeletes;
    /*
    * guarded variable
    *
    * @var array
    */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected  $table = "restaurant_attributes_i18n";
}
