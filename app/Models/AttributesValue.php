<?php

namespace App\Models;

use App\Models\AttributesValueLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributesValue extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "attributes_value";

    protected $dates = ['deleted_at'];

    public function lang()
    {
        return $this->hasMany(AttributesValueLang::class, 'attribute_value_id');
    }
}
