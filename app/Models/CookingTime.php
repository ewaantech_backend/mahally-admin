<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CookingTime extends Model
{
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "cooking_time";
}
