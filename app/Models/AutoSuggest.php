<?php

namespace App\Models;

use App\Models\AutoSuggestLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AutoSuggest extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "autosuggest";

    public function lang()
    {
        return $this->hasMany(AutoSuggestLang::class, 'autosuggest_id');
    }
}
