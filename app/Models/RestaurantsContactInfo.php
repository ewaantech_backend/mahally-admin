<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class RestaurantsContactInfo extends Authenticatable
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    // protected $guarded = [];
    protected $guard = 'restaurant';
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "restuarants_contactinfo";

    protected $fillable = [
        'restuarants_id', 'contact_person', 'designation', 'phone', 'email'
    ];
}
