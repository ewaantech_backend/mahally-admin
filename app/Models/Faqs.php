<?php

namespace App\Models;

use App\Models\FaqsLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faqs extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "faq";

    public function lang()
    {
        return $this->hasMany(FaqsLang::class, 'faq_id');
    }
}
