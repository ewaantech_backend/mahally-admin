<?php

namespace App\Models;

use App\Models\MembershipLang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Membership extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "membership";

    public function lang()
    {
        return $this->hasMany(MembershipLang::class, 'membership_id');
    }
}
