<?php

namespace App\Models;

use App\Models\ReviewSegments;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reviews extends Model
{
    use  SoftDeletes;
    /**
     * guarded variable
     *
     * @var array
     */
    protected $guarded = [];
    /**
     * $table variable
     *
     * @var string
     */

    protected $table = "reviews";

    public function customer()
    {
        return $this->hasMany(Customers::class, 'id', 'customer_id');
    }
    public function restaurant()
    {
        return $this->hasMany(Restaurant::class, 'id', 'restaurant_id');
    }
    public function reviewsegment()
    {
        return $this->hasMany(ReviewSegments::class, 'review_id');
    }
    public function customerdata()
    {
        return $this->belongsTo(Customers::class, 'customer_id');
    }
}
