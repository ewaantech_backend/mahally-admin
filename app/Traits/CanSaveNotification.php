<?php

namespace App\Traits;

use App\Models\AppNotification;


trait CanSaveNotification
{
    public function saveNotification(array $content, string $fromType, string $toType, array $details, string $to)
    {
      
       $note= AppNotification::create([
            'content' => json_encode($content, JSON_UNESCAPED_UNICODE),
            'from_type' => $fromType,
            'to_type' => $toType,
            'details' => json_encode($details),
            'to_id' => $to,
        ]);
        return $note;
    }
}