<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

trait FormatLanguage
{
    /**
     * Key a realtion data by specified key
     *
     * @param Model $model
     * @param string $relation
     * @param string $key
     * @return void
     */
    public function keyBy(Model $model, string $relation, string $key)
    {
        /** @var Collection */
        $related = $model->getRelation($relation);
        $keyedData = $related->keyBy($key);
        $model->unsetRelation($relation);
        $model->setRelation($relation, $keyedData);
    }

    /**
     * Key a collection of data relationship
     *
     * @param Collection|LengthAwarePaginator $collection
     * @param string $relation
     * @param string $key
     * @return void
     */
    public function keyByNested($collection, string $relation, string $key)
    {
        $collection->each(function (Model $model) use ($relation, $key) {
            $this->keyBy($model, $relation, $key);
        });
    }
}