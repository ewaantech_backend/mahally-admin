<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderAddonsForignkey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_addons', function (Blueprint $table) {
            $table->dropForeign(['addon_id']);
        });
        Schema::table('order_addons', function (Blueprint $table) {
            $table->dropColumn('addon_id');
            
        });
        Schema::table('order_addons', function (Blueprint $table) {
            $table->unsignedBigInteger('addon_id')->after('order_item_id')->nullable();
            
            $table->foreign('addon_id')
                ->references('id')
                ->on('restaurant_addons')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
