<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items_attributes', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->engine = 'InnoDB';
            $table->unsignedSmallInteger('menu_item_id');
            $table->unsignedSmallInteger('attribute_id');
            $table->unsignedSmallInteger('attribute_value_id')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('menu_item_id')
                ->references('id')
                ->on('menu_items')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('attribute_id')
                ->references('id')
                ->on('restaurant_attributes')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('attribute_value_id')
                ->references('id')
                ->on('restaurant_attribute_values')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items_attributes');
    }
}
