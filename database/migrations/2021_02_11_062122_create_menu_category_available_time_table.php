<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuCategoryAvailableTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_category_available_time', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->engine = 'InnoDB';
            $table->unsignedSmallInteger('menu_cat_id');
            $table->unsignedSmallInteger('time_id');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('menu_cat_id')
                ->references('id')
                ->on('menu_category')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('time_id')
                ->references('id')
                ->on('timing')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_category_available_time');
    }
}
