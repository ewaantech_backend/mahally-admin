<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFreeMealLoyaltyI18nTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('free_meal_loyalty_i18n', function (Blueprint $table) {
            $table->id();
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('free_meal_id');
            $table->text('name');
            $table->text('description')->nullable();
            $table->enum('language', ['en', 'ar']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('free_meal_id')
                ->references('id')
                ->on('free_meal_loyalty')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('free_meal_loyalty_i18n');
    }
}
