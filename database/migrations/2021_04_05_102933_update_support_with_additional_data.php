<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSupportWithAdditionalData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::table('support_ticket', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('user_name');
        });
        Schema::table('support_ticket', function (Blueprint $table) {
            // $table->integer('order_id')->nullable();
            $table->string('subject', 1000)->after('id');
            $table->text('message')->nullable()->after('id');
            $table->enum('app_type', ['userapp', 'admin'])->after('id');
            $table->unsignedBigInteger('submitted_by')->after('id');
        });
        Schema::table('support_ticket_i18n', function (Blueprint $table) {
            $table->enum('user_type', ['userapp','admin'])->after('message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
