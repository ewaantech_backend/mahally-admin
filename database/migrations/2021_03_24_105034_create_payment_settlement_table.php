<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSettlementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_settlement', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->integer('restaurant_id')->nullable();
            $table->dateTime('settlement_date')->nullable();
            $table->decimal('settlement_amount',10,2)->nullable();
            $table->integer('type')->comment('1 => Bank Deposit,  2 => Online Transfer')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_settlement');
    }
}
