<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCustomerWithPhoto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer', function (Blueprint $table) {

            $table->text('photo')->nullable()->after('status');
            $table->enum('phno_in_order',['enable','disable'])
                        ->default('enable')
                        ->after('status');
            $table->enum('phno_in_order_review',['enable','disable'])
                        ->default('enable')
                        ->after('status');

        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer', function (Blueprint $table) {

        });
    }
}
