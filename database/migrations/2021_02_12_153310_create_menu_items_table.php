<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_items', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->engine = 'InnoDB';
            $table->unsignedBigInteger('restuarants_id');
            $table->unsignedSmallInteger('menu_cate_id');
            $table->text('calory')->nullable();
            $table->integer('price')->nullable();
            $table->integer('discount_price')->nullable();
            $table->text('cover_photo')->nullable();
            $table->enum('status', ['active', 'deactive'])->default('deactive');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('restuarants_id')
                ->references('id')
                ->on('restuarants')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('menu_cate_id')
                ->references('id')
                ->on('menu_category')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_items');
    }
}
