<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCommissionCategoryIdFieldToRestuarantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('restuarants', function (Blueprint $table) {
           $table->dropForeign('restuarants_commission_category_id_foreign');
           $table->dropIndex('restuarants_commission_category_id_foreign');
        });
        Schema::table('restuarants', function (Blueprint $table) {
          $table->integer('commission_category_id')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('restuarants', function (Blueprint $table) {
            //
        });
    }
}
