<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->text('order_id')->nullable();
            $table->bigInteger('customer_id')->unsigned()->nullable();
            $table->bigInteger('restaurant_id')->unsigned()->nullable();
            $table->enum('order_type', ['dine', 'takeaway']);
            $table->integer('item_count')->nullable();
            $table->integer('order_status')->nullable();
            $table->text('grand_total')->nullable();
            $table->text('sub_total')->nullable();
            $table->text('tax_total')->nullable();
            $table->text('vat')->nullable();
            $table->text('payment_mode')->nullable();
            $table->text('cod_fee')->nullable();
            $table->timestamps();


            $table->foreign('customer_id')
                ->references('id')
                ->on('customer')
                ->onUpdate('cascade')
                ->onDelete('cascade');
            $table->foreign('restaurant_id')
                ->references('id')
                ->on('restuarants')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
