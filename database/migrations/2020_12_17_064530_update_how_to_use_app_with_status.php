<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHowToUseAppWithStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('how_to_use', function (Blueprint $table) {
            $table->enum('status', ['active', 'deactive'])
                ->default('active')
                ->after('available_for');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('use_app_with_status', function (Blueprint $table) {
            //
        });
    }
}
