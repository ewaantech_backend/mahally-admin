<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestuarantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restuarants', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->unsignedBigInteger('city_id');
            $table->unsignedBigInteger('commission_category_id');
            $table->unsignedSmallInteger('membership_id');
            $table->text('logo')->nullable();
            $table->text('cover_photo')->nullable();
            $table->timestamp('opening_time')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('closing_time')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->enum('vat_includes', ['yes', 'no'])->default('no');
            $table->integer('vat_percentage')->nullable();
            $table->text('vat_reg_number', 100)->nullable();
            $table->text('coordinates', 100)->nullable();
            $table->enum('status', ['active', 'deactive'])->default('active');

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('city_id')
                ->references('id')
                ->on('autosuggest')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('commission_category_id')
                ->references('id')
                ->on('commission_category')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('membership_id')
                ->references('id')
                ->on('membership')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restuarants');
    }
}
