<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CookingTimeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cooking_time')->insert([
            [
                'time' => '05 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => '10 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => '15 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => '20 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => '25 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => '30 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => '45 mins ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'time' => 'No time ',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
