<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StaffRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('restaurant_staff_roles')->insert([
            [
                'name' => 'Kitchen Staff',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Office Staff',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
