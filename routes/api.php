<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Kitchen\LoginController;
use App\Http\Controllers\Api\Frontoffice\OrderController;
use App\Http\Controllers\Api\Kitchen\KitchenOrderController;
use App\Http\Controllers\Api\Frontoffice\FrontDiskLoginController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('kitchen-staff-login', [LoginController::class, 'login']);
Route::post('forgot-password', [LoginController::class, 'forgotPassword']);
Route::post('verify', [LoginController::class, 'verifyCode']);
Route::post('reset-password', [LoginController::class, 'resetPassword']);


Route::group(['middleware' => ['auth:kitchen', 'scope:kitchen']], function () {
    Route::post('kitchen-logout', [LoginController::class, 'logout']);
    Route::get('cooking-time-list', [KitchenOrderController::class, 'cookingTime']);
    Route::get('active-order-list', [KitchenOrderController::class, 'ActiveOrders']);
    Route::post('kitchen/change-status', [KitchenOrderController::class, 'ChangeOrderStatus']);
    Route::get('ready-for-pickup-order-list', [KitchenOrderController::class, 'ReadyForPickupOrders']);
    Route::get('delivered-order-list', [KitchenOrderController::class, 'Delivered']);
    Route::get('kitchen-order-status', [KitchenOrderController::class, 'OrderStatus']);
    Route::post('active-order-id-list', [KitchenOrderController::class, 'ActiveOrderIdList']);
    Route::post('pickup-order-id-list', [KitchenOrderController::class, 'ReadyForPickupOrderIdList']);
    Route::post('delivered-order-id-list', [KitchenOrderController::class, 'DeliveredOrderIdList']);
    Route::post('kitchen/search-order-list', [KitchenOrderController::class, 'SearchOrdersList']);
    Route::post('update_cooking_time', [KitchenOrderController::class, 'UpdateCookingTIme']);
    Route::post('kitchen/order-view', [KitchenOrderController::class, 'orderView']);
});


Route::post('front-desk-staff-login', [FrontDiskLoginController::class, 'login']);
Route::post('front-desk-forgot-password', [FrontDiskLoginController::class, 'forgotPassword']);
Route::post('front-desk-verify', [FrontDiskLoginController::class, 'verifyCode']);
Route::post('front-desk-reset-password', [FrontDiskLoginController::class, 'resetPassword']);

Route::group(['middleware' => ['auth:frontdesk', 'scope:frontdesk']], function () {
    Route::post('frontdesk-logout', [FrontDiskLoginController::class, 'logout']);
    Route::post('frontdesk/orders-list', [OrderController::class, 'Orders']);
    Route::post('frontdesk/change-status', [OrderController::class, 'ChangeOrderStatus']);
    Route::get('frontdesk/order-status', [OrderController::class, 'OrderStatus']);
    Route::post('frontdesk/order-id-list', [OrderController::class, 'OrderIdList']);
    Route::post('frontdesk/search-order-list', [OrderController::class, 'SearchOrdersList']);
    Route::post('frontdesk/order-view', [OrderController::class, 'orderView']);
});
