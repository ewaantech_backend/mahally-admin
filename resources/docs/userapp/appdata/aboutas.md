# About as

---

About as  details

### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/aboutas` | No            |



### Response

```json
{
    "success": true,
    "data": {
        "id": 1,
        "lang": {
            "en": {
                "page_id": 1,
                "title": "About as",
                "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n                            Lorem Ipsum has been the industry standard dummy text ever since the 1500s, \n                            when an unknown printer took a galley of type and scrambled it to make a type \n                            specimen book. It has survived not only five centuries, but also the leap into electronic \n                            typesetting, remaining essentially unchanged",
                "language": "en"
            },
            "ar": {
                "page_id": 1,
                "title": "النوع وتدافعت عليه لصنع",
                "content": " يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع\n                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية",
                "language": "ar"
            }
        }
    }
}
```
