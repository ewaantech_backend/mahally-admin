# Terms and Conditions

---

Terms and conditions details

### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| GET    | `api/user-app/terms-and-conditions` | No            |



### Response

```json
{
    "success": true,
    "data": {
        "id": 3,
        "lang": {
            "en": {
                "page_id": 3,
                "title": "Terms and condition",
                "content": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. \n                Lorem Ipsum has been the industry standard dummy text ever since the 1500s, \n                when an unknown printer took",
                "language": "en"
            },
            "ar": {
                "page_id": 3,
                "title": "النوع وتدافعت عليه لصنع",
                "content": " يستخدم في صناعة الطباعة والتنضيد. عندما أخذت طابعة غير معروفة لوحًا من النوع وتدافعت عليه لصنع نوع\n                كتاب العينة. لقد نجت ليس فقط خمسة قرون ، ولكن أيضًا القفزة الإلكترونية",
                "language": "ar"
            }
        }
    }
}
```
