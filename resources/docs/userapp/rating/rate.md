
# Rating 


### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| POST    | `api/user-app/rate/order` | Yes|

### Request
```json
{
   "review":[
       {
           "segment_id": 1,
            "rating": 4
       }
   ],
   "restaurant":"17",
   "order_id":"2",
   "message":"bad"
}
```

### Response
```json
{
    "success": true,
    "msg": "Review added successfully"
}
```
