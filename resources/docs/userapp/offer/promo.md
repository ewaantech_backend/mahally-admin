# Promocode check 
---
### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| POST    | `api/user-app/promocode` | No            |
### Request
```json
{
    "code":"NEW",
    "cust_id:5
}
```
### Response
```json
{
    "success": false,
    "error": {
        "msg": "Invalid promocode"
    }
}
// Succes
{
    "success": true,
    "msg": "Valid promocode"
}
```
