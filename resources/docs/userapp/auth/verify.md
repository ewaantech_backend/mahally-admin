# Verify OTP

---

Verify OTP

### Details

| Method | Uri                         | Authorization |
| :----- | :-------------------------- | :------------ |
| POST   | `api/user-app/verify`         | No            |

### Request Params

```json
{
    "phone": "9995256535",
    "otp": "1234",
    "fcm_token": "fdgrghajdhaskfldhgkagfagwifhwe"
}
```

### Response

```json
{
    "success": true,
    "data": {
        "user": {
            "id": 4,
            "name": null,
            "email": null,
            "phone_number": "09995256535",
            "city": null,
            "verification_code": null,
            "code_sent_at": null,
            "verified_at": null,
            "status": "active",
            "fcm_token": "jdzhjhjcvhjjj",
            "otp": "1234",
            "otp_generated_at": "2021-03-01 16:49:55",
            "need_notification": 1,
            "language": "en",
            "created_at": "2021-03-01T14:03:51.000000Z",
            "updated_at": "2021-03-01T16:49:55.000000Z",
            "deleted_at": null
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiODA2ZDZlNGYzYTcwOTExYjE5ZjAyMWE4YmYyZTg1NGNmMjgwMGM2NWU5M2ZhZWM4Mjg1YzAzNWM2NWJkYzdlM2FhMWJkMjk0NmM5MzRkNmMiLCJpYXQiOjE2MTQ2MTc1MTUsIm5iZiI6MTYxNDYxNzUxNSwiZXhwIjoxNjQ2MTUzNTE1LCJzdWIiOiI0Iiwic2NvcGVzIjpbImN1c3RvbWVyIl19.pWpgEfrUjumz9Fv86-KTHfXJrT7jnyCQAO2sYLnLsdjeT1NmaEixOWn_UzgVpwh1ZGCTQc0KnNii-ilvXrRzJa8YB_inxbpSDf557FG55h_GuVPoOBK13aeLeba8CGf0-SOSqWBoRyz57sFPOEmWWyx6HuNS1jkIwlW1bE5O-Kw6FvjVDmiDV1vqIThheWMifs3nZ2yVonZpz-7u7cSmGQS8T20gKwtzzRFCvYPbXeeP0qCYALPT6Q7yg7Z-nHENQCHmFqCw1REZuOtva0w1x0-XtVgUnBhqj6AmYMzM8cMJXq9DA9peCQb_6yCh_4c6msZIovnpTebDf7ZtShy3T-yl8ORALKW0Q9QaG8ukyn7hXpNEXSA7yqHuod5Co2zXfu7sAZjMGcHg_yVG1kIMCtYiVGBqqEKMMIi0Cd6cRzBPp74kWU5tlL0FXeNgwLhj2cyLGbHSTChocya-fm8RoLNsgzQTgFpoG3zK7JPbYYaWfGXrwhj7Vl8N5n0jZZv_-GZZcOOFWkklFBrBl1bkM-ZMaTcaNXmIFKz3PjkKW5gEVEQWYXHmIKNDwlPLVAnRk1ooVvmXcr_1licT7rlfDfpcjjq69Lyfnthu-bSSK_xU5BcU-VH22PqqGerXfgp3M-w_YmVliUg3XyHrFs91WjCLeIY_c93rtT8xHKKqdOg"
    }
}
```
