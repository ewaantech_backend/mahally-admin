# Free Loyality

---

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/loyality-meal` | NO         |


### Request Params
```json
{
    "cust_id":"5",
    "latitude":"8.5685708",
    "longitude":"76.87313329999999"
}
```


### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 1,
            "restuarants_id": 17,
            "menu_item_id": 20,
               "retaurant": {
                "id": 17,
                "lang": [
                    {
                        "id": 33,
                        "restuarants_id": 17,
                        "name": "Cafe Arabesque",
                        "description": "SDSFDFDSFDDFDSFSFDSFDSDFSFnnnnnnnnnnnnnnnnnnnn",
                        "language": "en"
                    },
                    {
                        "id": 34,
                        "restuarants_id": 17,
                        "name": "Cafe Arabesque",
                        "description": "SDSFDFDSFDDFDSFSFDSFDSDFSFnnnnnnnnnnnnnnnnnnnn",
                        "language": "ar"
                    }
                ]
            },
            "lang": {
                "en": {
                    "id": 1,
                    "free_meal_id": 1,
                    "name": "fried chicken",
                    "description": "free meal",
                    "language": "en",
                    
                },
                "ar": {
                    "id": 2,
                    "free_meal_id": 1,
                    "name": "النوع وتدافعت عليه لصنع",

                }
            }
        },
    ]
}
```
