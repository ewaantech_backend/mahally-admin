# Notification list
---
### Details
| Method | Uri   | Authorization |
| : |   :-   |  :  |
| GET | `api/user-app/notification/list` | yes |

### Response
```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "content": "{\"en\":\"Opertaion team replied on your query support\",\"ar\":\"Opertaion team replied on your query support\"}",
                "is_read": 0,
                "details": "{\"type\":\"support\",\"request_id\":\"1\"}",
                "created_at": "2021-04-07T09:28:08.000000Z"
            }
        ],
        "first_page_url": "http://mahally.test/api/user-app/notification/list?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://mahally.test/api/user-app/notification/list?page=1",
        "next_page_url": null,
        "path": "http://mahally.test/api/user-app/notification/list",
        "per_page": 15,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
```
