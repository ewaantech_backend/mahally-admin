# Restaurant Type
---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| GET   | `api/user-app/restaurant-type` | No            |



### Response

```json
{
    "success": true,
    "data": [
        {
            "id": 32,
            "lang": {
                "en": {
                    "id": 63,
                    "autosuggest_id": 32,
                    "name": "Chinese",
                    "language": "en"
                },
                "ar": {
                    "id": 64,
                    "autosuggest_id": 32,
                    "name": "صينى",
                    "language": "ar"
                }
            }
        },
        {
            "id": 33,
            "lang": {
                "en": {
                    "id": 65,
                    "autosuggest_id": 33,
                    "name": "Lebanese",
                    "language": "en"
                },
                "ar": {
                    "id": 66,
                    "autosuggest_id": 33,
                    "name": "لبناني",
                    "language": "ar"
                }
            }
        }
    ]
}
```