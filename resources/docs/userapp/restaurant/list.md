# Restaurant list
---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/restaurent` | No            |

### Request Params

```json
{
    "latitude":22.3452,
    "longitude":44.3256,
    "category":[],
    "nearBy":,
    "sortby": "popularity  or  nearby"
}
```

### Response

```json
{
    "success": true,
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 14,
                "logo": null,
                "cover_photo": "http://mahally.test/uploads/restaurant/cover/2GGgF6WeNS1MXFdCQk7yYu0GH8A48dC7YX1gTCVS.png",
                "location": "Unnamed Road 19861,  Saudi Arabia",
                "latitude": "24.563808942585514",
                "longitude": "46.252326367187486",
                "distance": 315.4,
                "type": [
                    {
                        "id": 33,
                        "created_by": 1,
                        "status": "deactive",
                        "pivot": {
                            "restuarants_id": 14,
                            "restuarant_type_id": 33
                        },
                ]
                ....
        "first_page_url": "http://mahally.test/api/user-app/restaurent?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "http://mahally.test/api/user-app/restaurent?page=1",
        "next_page_url": null,
        "path": "http://mahally.test/api/user-app/restaurent",
        "per_page": 5,
        "prev_page_url": null,
        "to": 5,
        "total": 5
    }
}
```