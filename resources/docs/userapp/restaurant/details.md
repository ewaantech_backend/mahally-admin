# Restaurant Details
---
### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/user-app/restaurent/{id}` | No            |

### Request Params

```json
{
    "latitude":22.3452,
    "longitude":44.3256,
    "cust_id":"15"
    }
```

### Response

```json
{
    "success": true,
    "data": {
        "id": 4,
        "logo": "http://mahally.test/uploads/restaurant/logo/X9ThFpdAqnFecfMiTXdyNlSXUnwUGHnXgYfY2auq.png",
        "cover_photo": "http://mahally.test/uploads/restaurant/cover/RHUs69SQKQnI62Qsbk6LsxAFBjQNHqVIUqGOPcnb.png",
        "location": "Riyadh Saudi Arabia",
        "latitude": "24.7136",
        "longitude": "46.6753",
        "distance": 355.99,
        "lang": [
            {
                "id": 7,
                "name": "Cafe Arabesque",
                "language": "en",
                "restuarants_id": 4
            },
            {
                "id": 8,
                "name": "Cafe Arabesque",
                "language": "ar",
                "restuarants_id": 4
            }
        ],
        "restaurant_images": [
            {
                "id": 9,
                "restuarants_id": 4,
                "image_path": "http://mahally.test/uploads/restaurant/restaurant_images/U5GlazjobNcZDEXTYQRiI9t2y2ewdbDHhXpUjncr.png"
            },
            {
                "id": 10,
                "restuarants_id": 4,
                "image_path": "http://mahally.test/uploads/restaurant/restaurant_images/5ksVPnwkvroO8l6OMIqT8tAPhgyv8I7mSNZnMSmA.png"
            }
        ],
        .....
    }
}
```