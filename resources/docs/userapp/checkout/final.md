# Checkout  
---
Checkout 

### Details

| Method | Uri                    | Authorization |
| :----- | :--------------------- | :------------ |
| POST   | `api/user-app/checkout` | yes          |

### Request Params

```json
{
    "customer_id":"5",
    "restaurant_id":"17",
    "order_type":"dine",
    "promo_code":"NEWYEAR-2021",
    "item_count":"2",
    "payment_type":"online",
   "order_items" :[
       {
           "item_id":"21",
           "item_count":1,
           "item_type":"offer / loyality / normal ",
           "offer_id" :"1" , // if item_type == offer 
           "addon":[
               {
               "addon_id":107,
               "item_count":1
           },{
              "addon_id":94,
               "item_count":1 
           }
        //    if add is null => "addon" :null 
           ]
       
       }
   ]
}

```
### Response
```json
{
    "success": true,
    "data": {
        "order_id": "MAH-ORD-00004",
        "amount": "50"
    }
}
```
