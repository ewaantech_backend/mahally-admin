# Forgot Password

---

Forgot Password

### Details

| Method | Uri                              | Authorization |
| :----- | :------------------------------- | :------------ |
| POST   | `api/front-desk-forgot-password` | No            |

### Request Params

```json
{
    "email": "admin@mahally.com5"
}
```

### Response

```json
{
    "success": true,
    "msg": "We have sent varification code to your mail entered"
}
```
