# Login

---

Login using Email

### Details

| Method | Uri                          | Authorization |
| :----- | :--------------------------- | :------------ |
| POST   | `api/front-desk-staff-login` | No            |

### Request Params

```json
{
    "email": "admin@mahally.com5",
    "password": "123456"
}
```

### Response

```json
{
    "success": true,
    "data": {
        "user": {
            "id": 12,
            "restuarants_id": 1,
            "name": "cxvdfgd",
            "user_id": "MAH-0000-00009",
            "email": "admin@mahally.com5",
            "phone": "7903588310",
            "status": "active",
            "created_at": "2021-03-08T10:47:03.000000Z",
            "updated_at": "2021-03-08T10:47:03.000000Z",
            "deleted_at": null,
            "role_id": 2,
            "email_verified_at": null,
            "password": "123456",
            "verification_code": null
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiM2ExMTNmNTg0Y2FmZmNmZTRhNmE1YTYyM2VkOTg0NTcxYjU5NWZhMTFiZGE3ZjgwMzliNmNlZDRiMzA3MzUxOWFlZTQwZjhkZmM1YmIzMGIiLCJpYXQiOjE2MTUyNzE0OTYsIm5iZiI6MTYxNTI3MTQ5NiwiZXhwIjoxNjQ2ODA3NDk1LCJzdWIiOiIxMiIsInNjb3BlcyI6WyJraXRjaGVuIl19.kln4Tzj-E3yAE6yxzi7_JEhbF2iS1eQcanD4PSHJorH-jkKTTD921l9ReV7ljvSCpfpf69S-KdFbFh5sRpSv64zmuI-VlAMV2cJ9P7Uo9slnx-yp3SAleuD9KQxWfDsEcPuj6z-1FAbYQ1eQL0fh6cfZugTUmhLMXlz65z1W8jArEw2pO3BrBBtRRfPogNK97T5izQ80ZpDHXOqKfZJdQD6gkKKelqJd5G35dDg1N9IcmkccZgWQbWaN8Hc-MJR2zE8adQD2iqYCAzcAbVo3y0Bm1RsHpJREwIVD_WTVb2XXmx7DlMgXTG9ZqxGMvdIJbUwavyiTR33JUnxmApFfXbNeB62jBv0Bmvr7nL2ZK2EwGFbQQvzbpEhNg0trXCP-TWeVB6n1NjEQqmhXQtC9-TsmjnSvFXCZtF3X-RxwnQF0qVaquS-mHun510hG84L8vWe69ia3MEWv9r4zxxxOPuBALkn7TlW81thzSWBKQUfKWAOm9DNv_71dhiVtM7ZIyIU50n4HrYzGIcgLY6ctWYVdTKSKZajUqoo9Ph-1DMrfjHqaMnrRbb3GixRwdHSpW4YpvgcZV2qQlCyR6ZG6aFhenI7PrggUPXmredaWcQe27XvagNuJohWTVVOend1o6ugVEkqwmNiGp92XsEOPwBRDu7Op5EeaTlT1uCvyQPE"
    }
}
```
