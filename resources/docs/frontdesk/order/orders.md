# Order

---

Orders

### Details

| Method | Uri                         | Authorization |
| :----- | :-------------------------- | :------------ |
| POST   | `api/frontdesk/orders-list` | Yes           |

### Request Params

```json
{
    "order_status": "1 or 2 or 3 or 4 or 5"
}
```

### Status Value

| param | value            |
| :---- | :--------------- |
| 1     | Pending Orders   |
| 2     | Confirmed Orders |
| 3     | In Kitchen       |
| 4     | Ready For Pickup |
| 5     | Delivered        |

### Response

```json
{
    "success": true,
    "msg": "Orders list available"
}
```
