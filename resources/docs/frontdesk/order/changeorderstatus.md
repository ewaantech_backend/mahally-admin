# Order

---

Change order status

### Details

| Method | Uri                           | Authorization |
| :----- | :---------------------------- | :------------ |
| POST   | `api/frontdesk/change-status` | Yes           |

### Request Params

```json
{
    "order_status": "1 or 2 or 3 or 4 or 5",
    "id": "1"
}
```

### Status Value

| param | value            |
| :---- | :--------------- |
| 1     | Pending Orders   |
| 2     | Confirmed Orders |
| 3     | In Kitchen       |
| 4     | Ready For Pickup |
| 5     | Delivered        |
| 6     | Cancel Order     |

### Response

```json
{
    "success": true,
    "msg": "Status changed successfully"
}
```
