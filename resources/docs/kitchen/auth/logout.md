# Logout

---

Logout

### Details

| Method | Uri                  | Authorization |
| :----- | :------------------- | :------------ |
| POST   | `api/kitchen-logout` | Yes           |

### Request Params

No params

### Response

```json
{
    "success": true,
    "msg": "Logout successful"
}
```
