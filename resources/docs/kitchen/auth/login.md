# Login

---

Login using Email

### Details

| Method | Uri                       | Authorization |
| :----- | :------------------------ | :------------ |
| POST   | `api/kitchen-staff-login` | No            |

### Request Params

```json
{
    "email": "test@test3.com",
    "password": "123456"
}
```

### Response

```json
{
    "success": true,
    "data": {
        "user": {
            "id": 10,
            "restuarants_id": 1,
            "name": "test3",
            "user_id": "MAH-0000-00007",
            "email": "test@test3.com",
            "phone": "7896857419",
            "status": "active",
            "created_at": "2021-03-02T08:02:05.000000Z",
            "updated_at": "2021-03-02T08:02:05.000000Z",
            "deleted_at": null,
            "role_id": null,
            "email_verified_at": null,
            "password": "123456",
            "verification_code": null
        },
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMjY0MjYzYjgyYjhkYzFmYjExMTVhMDQwY2VmYThhMTc5NGJlYjg2OGM5NzY0ZGZkOTRjZTdlMDhmNjQxOTNhOWI1NjE2MmNmZGUxNjgyNDAiLCJpYXQiOjE2MTQ2NzIxNzIsIm5iZiI6MTYxNDY3MjE3MiwiZXhwIjoxNjQ2MjA4MTcyLCJzdWIiOiIxMCIsInNjb3BlcyI6WyJraXRjaGVuIl19.e0DJEGPA6AbIHt0Dx_9Z_AAMFGo-g-qNz-gab1artB_PBfF8uyHYQAfVYwS0IzcwhVP1tWpUOlDJfLuur-6JVOwq17kE4J1cpHeP8VT6IP_O-4j_-d-cw51cOmJAAzA4Utnc7gEJ7Zu2Ca-4Sy0gnRcEaSHujSSZisLkRWJUqaUaD-BXHtoxByZqruJBCbGF3ScgJLJQ3ztN3PNJSZfOWGn5AL8pRnb8jCb_jXDgY-99TL01gEM_pfLuONLlCwsRt9_AwWzKFhfjcJ0rDUCSzVsWZ8sWopcB3Zj6lWTXrLwE9rPfg_DBhEQZybuTasgf5Aqyb2snDVFiLhjob3k8yasdDn5VDm6bxvjYAMbZ6qy192HSlVJeMV41zAFa6LO95bCUUjGSpz7OJwXwjAEhsIiSIRMaxYI10s_WJLj93czA3C7X4oSg9j-qrtfj90yAe5GhISmlG5vCWuScMtFvf9e9IEXBVJ8pMlFZc_D-v9eOXSBd0o82oRT4GLiBxZxdhhiwTP8Te5xRK9CB-uizVl7pRRqD4bKPbix978rzYWHwgg7aVSFirxETTDG--aixb-NtLAwzQW7aYcKU5vvHEfq7Ck570hdBOqNmMnNs8CELi4Nxlmyvo-7wgAM9Lrnqbhhva4ZhjFhGz-TxEpUPOMvdg-67lsbE_BQdtc6WACw"
    }
}
```
