# Ready For Pickup Orders

---

Ready For Pickup Order Listing

### Details

| Method | Uri                               | Authorization |
| :----- | :-------------------------------- | :------------ |
| GET    | `api/ready-for-pickup-order-list` | Yes           |

### Request Params

No params

### Response

```json
{
    "success": true,
    "data": {}
}
```
