# Active Order

---

Active Order Listing

### Details

| Method | Uri                     | Authorization |
| :----- | :---------------------- | :------------ |
| GET    | `api/active-order-list` | Yes           |

### Request Params

No params

### Response

```json
{
    "success": true,
    "data": {}
}
```
