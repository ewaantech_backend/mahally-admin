# Cooking Time

---

Cooking Time Listing

### Details

| Method | Uri                     | Authorization |
| :----- | :---------------------- | :------------ |
| GET    | `api/cooking-time-list` | Yes           |

### Request Params

No params

### Response

```json
{
    "success": true,
    "data": {
        "cooking_time": [
            {
                "id": 1,
                "time": "05 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 2,
                "time": "10 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 3,
                "time": "15 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 4,
                "time": "20 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 5,
                "time": "25 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 6,
                "time": "30 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 7,
                "time": "45 mins ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            },
            {
                "id": 8,
                "time": "No time ",
                "created_at": "2021-03-10T06:38:25.000000Z",
                "updated_at": "2021-03-10T06:38:25.000000Z"
            }
        ]
    }
}
```
