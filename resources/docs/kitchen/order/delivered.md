# Delivered Orders

---

Delivered Order Listing

### Details

| Method | Uri                        | Authorization |
| :----- | :------------------------- | :------------ |
| GET    | `api/delivered-order-list` | Yes           |

### Request Params

No params

### Response

```json
{
    "success": true,
    "data": {}
}
```
