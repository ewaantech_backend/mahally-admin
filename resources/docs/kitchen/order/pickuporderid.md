# Search Id of ready for pickup order

---

Listing ready for pickup order id for searching

### Details

| Method | Uri                        | Authorization |
| :----- | :------------------------- | :------------ |
| POST   | `api/pickup-order-id-list` | Yes           |

### Request Params

```json
{
    "key": "MAH03"
}
```

### Response

```json
{
    "success": true,
    "msg": "Orders Id list available"
}
```
