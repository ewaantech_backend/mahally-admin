@extends('layouts.master')
@section('content-title')
{{ __('messages.table_management.table_management') }}
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_table">
    <i class="ti-plus"></i> {{ __('messages.table_management.add_table_management') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('table_management.get') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" id="search_field" name="search" value="{{$search}}" placeholder="{{ __('messages.table_management.search_placeholder') }}">
                             <input type="hidden" name="table_select" id="table_select">
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">{{ __('messages.search') }}</font>
                            </button>
                            <a href="{{ route('table_management.get') }}" class="btn btn-default reset_style">{{ __('messages.reset') }}</a>
                        </div>
                        <div class="col-md-4">
                            <a href="{{route('restaurant-qrcode',Auth::user()->restuarants_id)}}"class="btn btn-sm btn-danger waves-effect" title="Print counter QR code"  target="_blank" style="float: right;" data-toggle="tooltip" ><i class="fa fa-print"></i></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('messages.table_management.table_header.sl_no') }}</th>
                                <th>{{ __('messages.table_management.table_header.table_number') }}</th>
                                <th>{{ __('messages.table_management.table_header.table_color') }}</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($table) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($table as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->table_number }}</td>
                                <td><input type="button" class="tbl-btn" style="background-color:{{ $row_data->table_color }};"></td>
                                <td class="text-left">
                                    <a href="{{route('table-qrcode',$row_data->id)}}"class="btn btn-sm btn-danger waves-effect" title="print order QR code"  target="_blank"><i class="fa fa-print"></i></a>
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Staff" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>

                                    

                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $table->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="table_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        Add Table
                    </strong></h4>
                <button type="button" class="close cancel-btn" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="table_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="table_unique_id" id='table_unique_id'>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.table_management.form.table_number') }} <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="{{ __('messages.table_management.form.table_number_placeholder') }} " type="text" name="table_number" id="table_number" />
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.table_management.form.table_color') }} <span class="text-danger">*</span></label>
                            <input class="form-control"  type="color" name="table_color" id="table_color" value="#ff0000" />
                        </div>
                        
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-table" id="save_data">
                       {{ __('messages.form.add') }}
                    </button>
                    <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal"> {{ __('messages.form.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
    .tbl-btn{
        background-color: #00ff2a;
        width: 60px;
        height: 20px;
        border: 0;
    }
</style>
@endpush
@push('scripts')
<script>
    $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});
    $('#add_new_table').on('click', function() {
        $('#name_change').html(@json(__('messages.table_management.form.add_table') ));
        $('#save_data').text('Add').button("refresh");
        $("#table_form")[0].reset();
        $('#table_popup').modal({
            show: true
        });
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.save-table').on('click', function(e) {
        $("#table_form").validate({
            rules: {
                table_number: {
                    required: true,
                },
                table_color: {
                    required: true,
                },
            },
            messages: {
                table_number: {
                    required: @json(__('messages.table_management.validation.table_number_required') ),
                },
                table_color: {
                    required: @json(__('messages.table_management.validation.table_color_required') ),
                },  
            },
            submitHandler: function(form) {
                table_unique_id = $("#table_unique_id").val();
                $('.loading_box').show();
                $('.loading_box_overlay').show();
                $('button:submit').attr('disabled', true);
                if (table_unique_id) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('table_management.update')}}",
                        data: {
                            table_number: $('#table_number').val(),
                            table_color: $('#table_color').val(),
                            table_unique_id: table_unique_id
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#table_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('table_management.store')}}",
                        data: {
                            table_number: $('#table_number').val(),
                            table_color: $('#table_color').val(),
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#table_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });

                }



            }
        })
    });

    $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        $('#name_change').html(@json(__('messages.table_management.form.edit_table') ));
        $('#save_data').text('Save').button("refresh");
        var url = "table_management/edit/";
        $.get(url + page, function(data) {
            console.log(data);
            $('#table_number').val(data.table.table_number),
            $('#table_color').val(data.table.table_color),    
            $('#table_unique_id').val(data.table.id)
            $('#table_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Table',
            content: 'Are you sure to ' + act_value + ' the table?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('table_management.status.update')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("table_management.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
   
    
    function deleteUser(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this Table? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('table_management.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("table_management.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }

    $('.cancel-btn').on('click',function(){
        $("#table_unique_id").val('');
        window.location.reload();
    });

    $( "#search_field" ).autocomplete({
        source: function( request, response ) {
        $.ajax( {
          url: "{{route('table_management.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#table_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });
</script>
@endpush