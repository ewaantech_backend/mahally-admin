@extends('layouts.master')
@section('content-title')
CHANGE PASSWORD
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm_change_password" action="javascript:;" method="POST">
                    <div class="row">
                        <div class="col-md-6">
                            <label>Password <span class="text-danger">*</span></label>
                            <input type="password" class="form-control" name="password" id="password" autocomplete="off" placeholder="enter your new password">
                        </div>
                        <div class="col-md-6">
                            <label>Confirm password <span class="text-danger">*</span></label>
                            <input type="password" placeholder="Enter the confirm password" class="form-control" id="confirm_password" name="confirm_password">
                        </div>
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-info waves-effect waves-light save-categorys">
                                Save
                            </button>
                            <button type="button" class="btn btn-default waves-effect" onclick="window.location.reload();">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $("#frm_change_password").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            password: {
                required: true,
                minlength: 6, 
            },
            confirm_password: {
                required: true,
                minlength: 6, 
                equalTo: '#password'
            }
        },
        messages: {
            password: {
                required: 'Password is required.',
            },
            confirm_password: {
                required: 'Confirm password is required.',
                equalTo: 'Password and confirm password not matching.'
            }
        },
        submitHandler: function (form) {
            $.ajax({
                type: "POST",
                url: "{{route('resturant.password.change')}}",
                data: $('#frm_change_password').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        Toast.fire({
                            icon: 'success',
                            title: data.message
                        });
                        window.setTimeout(function () {
                            window.location.href = '{{route("resturant.password.new")}}';
                        }, 1000);
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                }
            });
            return false;
        }
    });
</script>
@endpush