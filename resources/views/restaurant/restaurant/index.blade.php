@extends('layouts.master')

@section('content-title')
 {{ __('messages.edit_details.rest_profile') }}
@endsection
@section('content')
<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="col-lg-12">    
                <div class="col-sm-12">
                    <ul class="nav nav-tabs" role="tablist" id="rest_tab">
                        <li class="nav-item w-25 "> <a class="nav-link active" href="#basic_info" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">BASIC INFO</span></a> </li>
                        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#contact_info" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">CONTACT INFO</span></a> </li>
                        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#dmt_photo" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">DOCUMENTS & PHOTOS</span></a> </li>
                        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#fcty_terms" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">FACILITIES & TERMS</span></a> </li>
                    </ul>
                </div>
                <div class="tab-content">
                    @include('restaurant.restaurant.basic_info')
                </div>                   
            </div>
        </div>
    </div>        
</div>

@endsection
@push('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }
    .modal {
    
}

</style>
<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    /* #formModal { overflow-y:auto; } */
 
</style>

@endpush
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 
<script>   
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});       
</script>
    <script>
    $("button[data-dismiss=modal]").click(function () {
        location.reload();
    });
    $('.nav-tabs a').on('click', function (e) {
        var id = $(this).data("id");
        var x = $(e.target).text();
        var location = $(this).attr('href');
        if (id != '') {
            $.ajax({
                type: "GET",
                url: "{{route('rest_details_tabs')}}",
                data: {'activeTab': x, rest_id: id},
                success: function (result) {
                $('#rest_tab a[href="'+location+'"]').tab('show');
                    $('.tab-content').html(result);
                }
            });
        }
    });
    
    $(document).ready(function(){
        $("#vat_includes").on('click', function (e) {
            if($(this).val() == 'no'){
                $("#vat_includes").val('yes');
                $(".vat-check").show(); 
            }
            else{
                $("#vat_includes").val('no');
                $(".vat-check").hide();
                $('#vat_percentage_error').text(" ");
                $('#vat_reg_number_error').text(" ");  
            }
        });
    });

    $(".select2_multiple").select2({
        placeholder: "Select Multiple Restaurant Type",
        allowClear: true
    });
     $(".select2_multiple").on('select2:select',function (e){
        $("#restaurant_type-error").text('');
    });
//    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "360"});
    $(".save-and-continue").on('click', function (e) {
        $("#submit_action").val('continue');

        if($("#vat_includes").val() == 'yes'){
           
            if( $("#vat_percentage").val() == ''){
                $('#vat_percentage_error').text("VAT Percentage required");
                 e.preventDefault();
            }
            if( $("#vat_reg_number").val() == ''){
                $('#vat_reg_number_error').text("VAT Register number required");
                 e.preventDefault();
            } 
        }
        else{
           $('#vat_percentage_error').text(" ");
           $('#vat_reg_number_error').text(" "); 
        }
    });
    $(".save-btn").on('click', function (e) {
        $("#submit_action").val('save');
        if($("#vat_includes").val() == 'yes'){
           
            if( $("#vat_percentage").val() == ''){
                $('#vat_percentage_error').text("VAT Percentage required");
                 e.preventDefault();
            }
            if( $("#vat_reg_number").val() == ''){
                $('#vat_reg_number_error').text("VAT Register number required");
                 e.preventDefault();
            } 
        }
        else{
           $('#vat_percentage_error').text(" ");
           $('#vat_reg_number_error').text(" "); 
        }
    });
    $("#vat_percentage").keyup(function(){
        if($('#vat_percentage').val() != "") {
            var value = $('#vat_percentage').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            var intRegex = /^\d+$/;
            if(!intRegex.test(value)) {
                $('#vat_percentage_error').text("Accept only numeric value");
                success = false;
            }
        } else {
            $('#vat_percentage_error').text("")
            success = false;
        }
    });

    $("#frm_create_restaurant").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            name_en: {
                required: true,
            },
            name_ar: {
                required: true,
            },
            "restaurant_type[]":{
              required: true,
            },
            membership_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            commission_category_id: {
                required: true,
            }, 
            opening_time: {
                required: true,
            }, 
            closing_time: {
                required: true,
            }, 
            description_en: {
                required: true,
            },
            description_ar: {
                required: true,
            },    
        },
        messages: {
            name_en: {
                required: 'Restaurant name(EN)  required.'
            },
            name_ar: {
                required: 'Restaurant name(AR)  required.'
            },
            "restaurant_type[]":{
              required: "Restaurant type  required.",
            },
            membership_id: {
                required: "Membership  required.",
            },
            city_id: {
                required: "City  required",
            },
            commission_category_id: {
                required: "Commission category required",
            },
            opening_time: {
                required: "Opening time required",
            }, 
            closing_time: {
                required: "Closing time required",
            },
            description_en: {
                required: "Description (EN) required",
            },
            description_ar: {
                required: "Description (AR) required",
            }, 
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("id") == "restaurant_type") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $.ajax({
                type: "POST",
                url: "{{route('save_basic_info_details')}}",
                data: $('#frm_create_restaurant').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#rest_tab a[href="#contact_info"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $("#rest_tab").tabs({cache: true});
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
</script>
@endpush