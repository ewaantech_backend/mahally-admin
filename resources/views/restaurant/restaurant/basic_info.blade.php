<form id="frm_create_restaurant" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.edit_details.form.rest_name_en') }} <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter restaurant name (EN)" type="text" name="name_en" value="{{ isset($row_data['id']) ? $row_data->lang[0]->name : ''}}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ __('messages.edit_details.form.rest_name_ar') }} <span class="text-danger">*</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter restaurant name (AR)" type="text" name="name_ar" value="{{ isset($row_data['id']) ? $row_data->lang[1]->name : ''}}"/>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.edit_details.form.rest_type') }} <span class="text-danger">*</span></label><br/>
                    <select class="select2_multiple" name="restaurant_type[]" multiple="multiple" id="restaurant_type">
                        @foreach($restaurant_type as $row_val)
                        <option  value="{{ $row_val->id }}" {{ !empty($rt_tp_ids) && in_array($row_val->id, $rt_tp_ids) ? 'selected' : '' }}>{{ $row_val->lang[0]->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.edit_details.form.membership') }} <span class="text-danger">*</span></label>
                    <select class="form-control" name="membership_id" id="membership_id">
                        <option value=""> {{ __('messages.edit_details.form.sel_membership') }} </option>
                        @foreach($membership_data as $membership_data_val)
                        <option value="{{ $membership_data_val->membership_id  }}" {{ (isset($row_data['membership_id']) &&  $membership_data_val->membership_id  == $row_data['membership_id']) ? 'selected' : '' }}>{{ $membership_data_val->membership_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.edit_details.form.comm_category') }} <span class="text-danger">*</span></label>
                    <select class="form-control" name="commission_category_id" id="commission_category_id">
                        <option value="">{{ __('messages.edit_details.form.sel_comm_category') }} </option>
                        @foreach($commission_data as $commission_data_val)
                        <option value="{{ $commission_data_val->id  }}" {{ (isset($row_data['commission_category_id']) &&  $commission_data_val->id  == $row_data['commission_category_id']) ? 'selected' : '' }}>{{ $commission_data_val->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.edit_details.form.city') }} <span class="text-danger">*</span></label>
                    <select class="form-control" name="city_id" id="city_id">
                        <option value=""> {{ __('messages.edit_details.form.sel_city') }} </option>
                        @foreach($city_data as $city_val)
                        <option value="{{ $city_val->id  }}" {{ (isset($row_data['city_id']) &&  $city_val->id  == $row_data['city_id']) ? 'selected' : '' }}>{{ $city_val->name }}</option>
                        @endforeach
                    </select>
                </div>
               <div class="col-md-3">
                    <label class="control-label">{{ __('messages.edit_details.form.open_time') }} <span class="text-danger">*</span></label>
                    <input class="form-control" type="time" name="opening_time" value="{{ isset($row_data['id']) ?  optional($row_data->opening_time)->format('H:i:s') : '00:00'}}" />
                    <label class="opening_time_error"></label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">{{ __('messages.edit_details.form.close_time') }} <span class="text-danger">*</span></label>
                     <input class="form-control" type="time" name="closing_time" value="{{ isset($row_data['id']) ?  optional($row_data->closing_time)->format('H:i:s') : '00:00'}}" />
                    <label class="closing_time_error"></label>
                </div>
                 <div class="col-md-6"></div>
                <div class="col-md-4">
                    <br>
                    <br>
                    @if(isset($row_data['vat_includes']) &&  $row_data['vat_includes'] == 'yes')
                        <input type="checkbox" name="vat_includes" id="vat_includes" value="yes" checked>{{ __('messages.edit_details.form.vat_includes') }}
                    @else
                        <input type="checkbox" name="vat_includes" id="vat_includes" value="no" >{{ __('messages.edit_details.form.vat_includes') }} 
                    @endif
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.edit_details.form.vat_per') }} <span class="text-danger vat-check">*</span></label>
                    <input class="form-control form-white" placeholder="Enter VAT Percentage" type="number" name="vat_percentage" id="vat_percentage" value="{{ isset($row_data['id']) ? $row_data['vat_percentage'] : ''}}" min="0" max="100"/>
                    <span class="vat_error" id="vat_percentage_error"></span>
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.edit_details.form.vat_reg_no') }} <span class="text-danger vat-check">*</span></label>
                    <input class="form-control form-white" placeholder="Enter VAT Register Number" type="text" id="vat_reg_number" name="vat_reg_number" value="{{ isset($row_data['id']) ? $row_data['vat_reg_number'] : ''}}"/>
                    <span class="vat_error" id="vat_reg_number_error"></span>
                </div>
                
                <div class="col-md-12">
                    <label>{{ __('messages.edit_details.form.location') }}</label>
                    <input 
                        id="location" 
                        class="form-control" 
                        name="location" 
                        placeholder="Enter your location" 
                        value="{{ isset($row_data['id']) ? $row_data['location'] : old('locations','Riyadh Saudi Arabia')}}"
                    />
                    @error('location')
                    <span class="error">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-12" style="">
                    <div id="map"></div>
                </div>
                <input id="latitude" type="hidden" value="{{ isset($row_data['id']) ? $row_data['latitude'] : old('latitude', 24.7136)}}" name="latitude">
                <input id="longitude" type="hidden" value="{{ isset($row_data['id']) ? $row_data['longitude'] : old('longitude', 46.6753)}}"  name="longitude">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>{{ __('messages.edit_details.form.description_en') }} <span class="text-danger">*</span></label>
                    <textarea class="form-control area" id="description_en" name="description_en"placeholder="Enter description">{{ isset($row_data['id']) ? $row_data['lang'][0]['description'] : ''}}</textarea>
                    <label class="description_en_error"></label>
                </div> 
                <div class="col-md-6">
                    <label>{{ __('messages.edit_details.form.description_ar') }}  <span class="text-danger">*</span></label>
                    <textarea class="form-control area" id="description_ar" name="description_ar" placeholder="أدخل سؤالك" style="text-align:right !important">{{ isset($row_data['id']) ? $row_data['lang'][1]['description'] : ''}}</textarea>
                    <label class="description_ar_error"></label>
                </div> 
            </div>
            
        </div>
        <div class="modal-footer">
            <input type="hidden" id="rest_id" name="rest_id" value="{{ isset($row_data['id']) ? $row_data['id'] : '' }}">
            @if(isset($row_data['id']))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.form.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
               {{ __('messages.form.save_and_continue') }}
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="{{ route('restaurants.get') }}"> {{ __('messages.form.back_to_listing') }}</a>
        </div>
    </div>
</form>
<style>
   
      /* Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;

      }

      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #map {
        widows: 100%;
        height: 200px;
    } 
      
      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }

      #target {
        width: 345px;
      }
      .tabpanel{
    overflow-y: initial !important
}
#tab-body{
    height: 80vh;
    overflow-y: auto;
}
.area{
        height: 100px;
    }
.vat-check{
    display: none;
}
.vat_error{
    font-size: 12px;
    color: red;
}
.pac-container{
    z-index:1051!important;
}
</style>

<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
 <script>
    let map;

    function initMap() {
        // The location of Uluru
        var lat =   $('#latitude').val();
        var long =   $('#longitude').val();
    
        if(lat.length === 0){
            var uluru = {lat: {{old('latitude', 24.7136) }} , lng: {{old('longitude', 46.6753)}} };
        }else{       
                var uluru = {lat: {{ old('latitude', 'JSON.parse(lat)') }}, lng: {{ old('longitude', 'JSON.parse(long)') }} };    
        }
        // The map, centered at Uluru
         map = new google.maps.Map(
            document.getElementById('map'), 
            {zoom: 8, center: uluru,
            gestureHandling: 'greedy'}
        );
        // The marker, positioned at Uluru
        marker = new google.maps.Marker({
            position: uluru, 
            map: map,
            draggable: true,
        });

        marker.addListener('dragend', setLatLng);

        const input = document.getElementById('location');
        let autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

       
        autocomplete.addListener('place_changed', function() {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

            let address = place.formatted_address.split(',');
            let formattedAddress = address.length > 1
                ? address[0] + ', ' +address[address.length - 1]
                : address[0];

            $('#location').val(formattedAddress);
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        });
    }

    function setLatLng(event) {
        const lat = event.latLng.lat();
        const lng = event.latLng.lng();
        $('#latitude').val(lat);
        $('#longitude').val(lng);

        setAddress(lat, lng)
    }

    function setAddress(lat, lng) {
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({'location': {lat, lng}}, function(results, status) {
            if (status === 'OK' && results[0]) {
                let address = results[0].formatted_address.split(',');
                let formattedAddress = address.length > 1
                    ? address[0] + ', ' +address[address.length - 1]
                    : address[0];

                $('#location').val(formattedAddress);
            } 
        });
    }
    
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyBG_t-XkmJAtPKIpyhOoiXz6QuXphkaBQI&callback=initMap">
</script>  

<script>
$(document).ready(function(){
        $("#vat_includes").on('click', function (e) {
            if($(this).val() == 'no'){
                $("#vat_includes").val('yes');
                $(".vat-check").show(); 
            }
            else{
                $("#vat_includes").val('no');
                $(".vat-check").hide();
                $('#vat_percentage_error').text(" ");
                $('#vat_reg_number_error').text(" ");  
            }
        });
    });

    $(".select2_multiple").select2({
        placeholder: "Select Multiple Restaurant Type",
        allowClear: true
    });
     $(".select2_multiple").on('select2:select',function (e){
        $("#restaurant_type-error").text('');
    });
//    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "360"});
    $(".save-and-continue").on('click', function (e) {
        $("#submit_action").val('continue');

        if($("#vat_includes").val() == 'yes'){
           
            if( $("#vat_percentage").val() == ''){
                $('#vat_percentage_error').text("VAT Percentage required");
                 e.preventDefault();
            }
            if( $("#vat_reg_number").val() == ''){
                $('#vat_reg_number_error').text("VAT Register number required");
                 e.preventDefault();
            } 
        }
        else{
           $('#vat_percentage_error').text(" ");
           $('#vat_reg_number_error').text(" "); 
        }
    });
</script>
 
