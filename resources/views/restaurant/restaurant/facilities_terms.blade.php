<form id="frm_create_facility_terms" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">    
            <div class="row pl-2" ><b>{{ __('messages.edit_details.form.facilities') }}</b></div>
            <hr style="height:1px;border-width:0;color:#868e96;background-color:#868e96">
            <div class="row"> 
                @if($facilities)
                    @foreach ($facilities as $item)
                        <div class="col-md-3">
                            @if(in_array($item->id,$exist_facilities))
                            <input type="checkbox" name="facilities[]" id="facilities" value="{{$item->id}}" checked> {{ $item->lang[0]->name}}
                        @else 
                            <input type="checkbox" name="facilities[]" id="facilities" value="{{$item->id}}"> {{ $item->lang[0]->name}}
                        @endif  
                    </div>
                    @endforeach  
                @endif   
            </div>
            <div class="row pl-2"><b>{{ __('messages.edit_details.form.terms') }}</b></div>
            <hr style="height:1px;border-width:0;color:#868e96;background-color:#868e96"> 
            <div class="row">      
                    @if($terms)
                    @foreach ($terms as $item)
                        <div class="col-md-3">
                        @if(in_array($item->id,$exist_tearms))
                            <input type="checkbox" name="terms[]" id="terms" value="{{$item->id}}" checked> {{ $item->lang[0]->name}}
                        @else 
                            <input type="checkbox" name="terms[]" id="terms" value="{{$item->id}}"> {{ $item->lang[0]->name}}
                        @endif    
                    </div>
                    @endforeach  
                @endif
            </div>      
        </div>
        <div class="modal-footer">
           <input type="hidden" id="rest_id" name="rest_id" value="{{ $rest_id }}">
            {{-- <a type="submit" class="btn btn-info waves-effect waves-light save-categorys" href="{{ route('restaurants.get') }}">Done</a> --}}
            {{-- @if(!empty($rest_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            @endif --}}
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
               {{ __('messages.form.done') }}
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $rest_id }}' href="javascript:void(0);"> {{ __('messages.form.back') }}</a>
        </div>
    </div>
</form>
<style type="text/css">
.card {
    position: relative;
    width: 100%;
    display: -ms-flexbox;
    display: flex;
    min-width: 0;
}
</style>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });

    $('.tab_back').on('click', function () {
        var rest_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('rest_details_tabs')}}",
            data: {'activeTab': 'DOCUMENTS & PHOTOS', rest_id: rest_id},
            success: function (result) {
                $('#rest_tab a[href="#fcty_terms"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_facility_terms").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            "facilities[]": {
                required: true,
            },
            "terms[]": {
                required: true,
            },
        },
        messages: {
            "facilities[]": {
                required: 'Facilities is required',
            },
            "terms[]": {
                required: 'Terms is required'
            },
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $.ajax({
                type: "POST",
                url: "{{route('save_facility_terms_details')}}",
                data: $('#frm_create_facility_terms').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                            window.setTimeout(function() {
                                window.location.href = '{{route("create_restaurant_details")}}';
                            }, 1000);
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
 $("button[data-dismiss-modal=modal2]").click(function () {
    $('#image-modal').modal('hide');
    });
</script>