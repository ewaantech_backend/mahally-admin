@extends('layouts.master')

@section('content-title')
{{ __('messages.attributes.attributes') }}
@endsection
@section('add-btn')
<button  class="btn btn-info" id="main_attr_add_btn">
    <i class="ti-plus"></i> {{ __('messages.attributes.add_attributes_from_main_list') }}
</button>
<button  class="btn btn-info create_btn" id="attribute_add_btn">
     <i class="ti-plus"></i> {{ __('messages.attributes.add_attribute') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('attribute.get')}}" id="search-faq-form" method="get">
                            <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Attribute">
                            <input type="hidden" name="faq_select" id="faq_select">
                        </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-faq-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('attribute.get')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th>{{ __('messages.attributes.table_header.sl_no') }}</th>
                                <th>{{ __('messages.attributes.table_header.attr_name_en') }}</th>
                                <th>{{ __('messages.attributes.table_header.attr_name_ar') }}</th>
                                <th>{{ __('messages.attributes.table_header.from') }}</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                         <tbody>
                            @if (count($attribute) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($attribute as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td>{{$row_data->type ?? ''}}</td>
                                @if($row_data->type == 'By Restaurant')
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn" title="Edit Attributes" data-id="{{ $row_data->id }}" ><i class="fa fa-edit"></i></a>
                                   <a class="btn btn-sm btn-danger text-white" title="Delete Staff" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                                @else 
                                <td></td>
                                @endif
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $attribute->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_attribute" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->

{{-- Main List Popup --}}
<div class="modal fade" id="attr-main-popup" tabindex="-1" role="dialog" aria-labelledby="mainList"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="mainList"></h4>
                 <a href="{{route('resturant_attributes.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="attr-main-form" method="POST" action="#">
                    <input type="hidden" id="restaurant_id" name="restaurant_id">
                    @csrf
                    <div class="row mb-3">
                        @if($attributesMainList)
                        @foreach($attributesMainList as $item)
                            <div class="col-md-3">
                                @if(in_array($item->id,$attr_data))
                                    <input type="checkbox" name="attr[]" class="attr" value="{{$item->id}}" checked> {{ $item->name}}
                                @else 
                                    <input type="checkbox" name="attr[]" class="attr" value="{{$item->id}}"> {{ $item->name}}
                                @endif 
                            </div> 
                        @endforeach
                        @endif
                         
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save-addon-main">
                                       {{ __('messages.form.save') }}
                                    </button>
                                    
                                <a href="{{route('resturant_attributes.get')}}" class="btn btn-default reset_style">{{ __('messages.form.cancel') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>

<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
      
         $('.create_btn').on('click', function () {
            var cat_id = $(this).data("cat-id");
            $.ajax({
            type: "GET",
                    url: "{{route('create_resturant_attributes')}}",
                    data: {'cat_id': cat_id},
                    success: function (data) {
                    $('.modal-content').html(data);
                    $('#formModal').modal('show');
                    }
            });
        });
        $('.edit_btn').on('click', function () {
            var attribute_id = $(this).data("id");
            $.ajax({
            type: "GET",
                    url: "{{route('create_resturant_attributes')}}",
                    data: {'id': attribute_id},
                    success: function (data) {
                    $('.modal-content').html(data);
                    $('#formModal').modal('show');
                    }
            });
        });

           $('#main_attr_add_btn').on('click',function(e){
            $('#attr-main-form').trigger("reset")
            $('#mainList').html('Add From Main List');
            // $("#attr-main-form")[0].reset();
            $('#attr-main-popup').modal('show');
           
        });
        
         $.validator.messages.required = "Attribute value (EN) required";
        jQuery.validator.addClassRules("att_value_en", {
            required: true,     
        });
        
        jQuery.validator.addClassRules("att_value_ar", {
            required: true       
        });

        $("#frm_create_attribute").validate({
             
    normalizer: function (value) {
    return $.trim(value);
    },
            rules: {
                    name_en: {
                    required: true,
                    },
                    name_ar: {
                    required: true,
                    },
            },
            messages: {
                    name_en: {
                    required: @json(__('messages.attributes.validation.name_en') ),
                    },
                    name_ar: {
                    required:  @json(__('messages.attributes.validation.name_ar') ),
                    },
            },
            submitHandler: function (form) {
                $('.loading_box').show();
                $('.loading_box_overlay').show();   
                $('button:submit').attr('disabled', true);
            $.ajax({
            type: "POST",
                    url: "{{route('save_resturant_attributes')}}",
                    data: $('#frm_create_attribute').serialize(),
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data.status == 1) {
                            Toast.fire({
                            icon: 'success',
                                    title: data.message
                            });
                            window.setTimeout(function () {
                            window.location.href = '{{route("resturant_attributes.get")}}';
                            }, 1000);
                            $("#frm_create_attribute")[0].reset();
                        } else {
                            Toast.fire({
                            icon: 'error',
                                    title: data.message
                            });
                        }
                        $('.loading_box').hide();
                        $('.loading_box_overlay').hide();
                        $('button:submit').attr('disabled', false);
                    },
                    error: function (err) {
                        $('.loading_box').hide();
                        $('.loading_box_overlay').hide();
                        $('button:submit').attr('disabled', false);
                    }
            });
            return false;
            }
    });
       
function deleteUser(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this attribute? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('resturant_attributes.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("resturant_attributes.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Attribute',
            content: 'Are you sure to ' + act_value + ' the attribute?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('activate_resturant_attributes')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("resturant_attributes.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

    $('.save-addon-main').on('click',function(e){  
           $("#attr-main-form").validate({
                ignore: [],
                rules: {
                  
                },
                messages: {               
                    
                },
                errorPlacement: function(error, element) {
                       
                },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    $('button:submit').attr('disabled', true);
                  
                    $.ajax({
                        type: "POST",
                        url: "{{route('resturant_attributes.storeMainList')}}",
                        data: $('#attr-main-form').serialize(),

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#attr-main-form")[0].reset();
                            } else {
                               
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });
                  
                
                }
            });                 
        });
    </script>
@endpush