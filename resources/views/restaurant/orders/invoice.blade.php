<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   </head>
   <body width="100%" >
       <table>
           <tr>
               <td class="logo"><img src="{{ asset('assets/images/sidelogo.png') }}" class="img-fluid img-fluid-01"></td>
           </tr>
            <tr>
                <td>
                     <h4 class="modal-title"><strong>{{ __('messages.orders.order_info.order_details') }}</strong></h4>
                </td>
            </tr>
            <tr>
                <td>
                    <label class="control-label">{{ __('messages.orders.order_info.order_info') }}</label>
                    <br /><br/>
                </td>
            </tr>
            <tr>
                <td style="widows: 30%">
                    <label class="control-label">{{ __('messages.orders.order_info.order_id') }}</label> :
                    {{ $order['order_id'] }}
                </td>
                <td style="widows: 30%">
                   <label class="control-label">{{ __('messages.orders.order_info.order_date') }} </label> :
                    {{ date('h:i A', strtotime($order['created_at'])) }}
                </td>
                 <td style="widows: 30%">
                    <label class="control-label">{{ __('messages.orders.order_info.payment_type') }} </label> :
                   {{ $order['payment_mode'] }}
                </td>
            </tr>
            <tr>
                <td style="widows: 30%">
                    <label class="control-label">{{ __('messages.orders.order_info.customer') }} </label> :
                     {{ $order->customer[0]->name }}<br/><br/>
                </td>
                <td style="widows: 30%">
                    <label class="control-label">{{ __('messages.orders.order_info.status') }}</label> :
                     {{ $order_status[$order->order_status] }}<br/><br/>
                </td>
                <td style="widows: 30%">
                    <label class="control-label">{{ __('messages.orders.order_info.status') }}</label> :
                     {{ $order_status[$order->order_status] }}<br/><br/>
                </td>
            </tr>
            <tr>
                <td>
                     <label class="control-label">{{ __('messages.orders.order_info.item') }} </label><br><br/> 
                </td>
                <td>
                     <label class="control-label">{{ __('messages.orders.order_info.quantity') }}</label><br> <br/>  
                </td>
                <td>
                      <label class="control-label">{{ __('messages.orders.order_info.price') }}</label><br><br/> 
                </td>
            </tr>
            @php 
                $total = 0;
                $discount = 0;
            @endphp

            @foreach ($order->order_item as $item)
                @if($item->menu_items)
                <tr>
                    <td>{{$item->menu_items->lang[0]->name}}</td>
                    <td>  {{$item->item_count}}</td>
                    <td>
                        {{$item->menu_items->price}}
                        @if($item->menu_items->price)
                           @php 
                                 $total =  $total+ $item->menu_items->price;
                           @endphp
                        @endif
                        @if($item->menu_items->discount_price)
                           @php 
                                 $discount =  $discount+ $item->menu_items->discount_price;
                           @endphp
                        @endif
                    </td>
                </tr>
                @endif 
                @if($item->order_addons)
                <tr>
                    <td style="width: 10%"></td>
                    <td>
                        <span>{{$item->order_addons->resturant_addons->lang[0]->name}}</span>
                    </td>
                    <td>
                        {{$item->order_addons->item_count}}
                    </td>
                    <td>
                        {{$item->order_addons->resturant_addons->price}}
                        @if($item->order_addons->resturant_addons->price)
                           @php 
                                 $total =  $total+ $item->order_addons->resturant_addons->price;
                           @endphp
                        @endif
                        @if($item->order_addons->resturant_addons->discount_price)
                           @php 
                                 $discount =  $discount+ $item->order_addons->resturant_addons->discount_price;
                           @endphp
                        @endif
                    </td>
                </tr>
                @endif
            @endforeach
            <tr>
                <td style="width: 30%"></td>
                <td>{{ __('messages.orders.order_info.subtotal') }}</td>
                <td>SAR {{$total }}</td>
            </tr>
            <tr>
                <td style="width: 30%"></td>
                <td>{{ __('messages.orders.order_info.discount') }}</td>
                <td>SAR {{$discount }}</td>
            </tr>
            <tr>
                <td style="width: 30%"></td>
                <td style="width: 50%"><hr></td>
            </tr>
            <tr>
                <td style="width: 30%"></td>
                <td>
                    {{ __('messages.orders.order_info.total_amt') }}
                </td>
                <td>
                   SAR {{$total-$discount }}
                </td>
            </tr>
       </table>       
   </body>
</html>