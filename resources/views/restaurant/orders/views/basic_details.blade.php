<form id="frm_create_product" action="javascript:;" method="POST">
    <div class="tab-pane active" id="order_info" role="tabpanel">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.order_info') }}</label>
                    <br />
                </div>
                <div class="col-md-8">
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.order_id') }}</label><br>
                      {{ $order['order_id'] }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.order_date') }} </label><br>
                    {{-- {{ $product[0]['lang'][1]['name'] }} --}}
                     {{ $order['created_at'] }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.payment_type') }} </label><br>
                   {{ $order['payment_mode'] }}
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.customer') }} </label><br>
                     {{ $order->customer[0]->name }}
                  
                </div>
                <div class="col-md-4">
                    <label class="control-label">{{ __('messages.orders.order_info.status') }}</label><br/>
                     Delivered
                        
                </div>
                <div class="col-md-4"></div>    
            </div>
            <div class="tbl-bd" style="border-style: groove;margin: 50px">
                <div class="row">
                    <div class="col-md-4">
                        <label class="control-label">{{ __('messages.orders.order_info.item') }} </label><br> 
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">{{ __('messages.orders.order_info.quantity') }}</label><br>   
                    </div>
                    <div class="col-md-4">
                        <label class="control-label">{{ __('messages.orders.order_info.price') }}</label><br>   
                    </div>     
                    @php 
                        $total = 0;
                        $discount = 0;
                    @endphp
                    @foreach ($order->order_item as $item)
                        @if($item->menu_items)
                        <div class="col-md-4">
                            {{$item->menu_items->lang[0]->name}}
                        </div>
                        <div class="col-md-4">
                            {{$item->item_count}}
                        </div>
                        <div class="col-md-4">
                            {{$item->menu_items->price}}
                            @if($item->menu_items->price)
                            @php 
                                    $total =  $total+ ($item->item_count * $item->menu_items->price);
                            @endphp
                            @endif
                            @if($item->menu_items->discount_price)
                            @php 
                                    $discount =  $discount + ($item->item_count * $item->menu_items->discount_price) ;
                            @endphp
                            @endif
                        </div>
                        @endif
                        @if($order->order_addons)
                        @foreach ($order->order_addons as $add_ons)
                        <div class="col-md-4">
                            <span style="padding-left: 25px;">{{$add_ons->lang[0]->name ?? ''}}</span>
                        </div>
                        <div class="col-md-4">
                            {{$add_ons->pivot->item_count ?? ''}}
                        </div>
                        <div class="col-md-4">
                            {{$add_ons->price}}
                            @if($add_ons->price)
                            @php 
                                    $total =  $total+ ($add_ons->price * $add_ons->pivot->item_count);
                            @endphp
                            @endif
                        </div>
                        @endforeach
                        @endif
                    @endforeach
                </div><br><br>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">{{ __('messages.orders.order_info.subtotal') }}</div>
                    <div class="col-md-4">SAR {{$total }}</div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">{{ __('messages.orders.order_info.discount') }}</div>
                    <div class="col-md-4">SAR {{$discount }}</div>
                </div> 
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-7">
                        <hr>
                    </div>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"></div>
                    <div class="col-md-4">{{ __('messages.orders.order_info.total_amt') }}</div>
                    <div class="col-md-4">SAR {{$total-$discount }}</div>
                    
                </div> 
            </div>
        </div>
        <div class="modal-footer">
           
        </div>
    </div>
</form>

<style>

label.control-label {
    font-size: 16px;
    color: #373757;
}
    </style>
