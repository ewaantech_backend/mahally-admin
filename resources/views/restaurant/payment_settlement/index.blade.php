@extends('layouts.master')

@section('content-title')
{{ __('messages.payment_settlement.payment_settlement') }}
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm_settlement_filter">
                    <div class="row">
                        <div class="col-md-6 ml-0">
                            <div class='input-group' id='datepicker'>
                                <input type="text" class="form-control" name="dates" placeholder="{{ __('messages.payment_settlement.by_date') }}" id="dates">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 text-left">
                            <button type="button" class="btn btn-info save-btn" id="settle_filter">
                                <font style="vertical-align: inherit;">{{ __('messages.payment_settlement.search') }}</font>
                            </button>
                            <a href="{{ route('payment_settlement') }}" class="btn btn-default">{{ __('messages.payment_settlement.reset') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row" id="settlement_result">
    @include('restaurant.payment_settlement.details')
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_settlement" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}});
$('input[name="dates"]').daterangepicker({
        "autoUpdateInput": false,
        startDate: "-2m",
        endDate: moment().endOf('month'), 
        maxDate: new Date()
});
$('input[name="dates"]').val();
 $('input[name="dates"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
});

$("#settle_filter").click(function(){
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    var start_date = $('#dates').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = $('#dates').data('daterangepicker').endDate.format('YYYY-MM-DD');
    $.ajax({
        type: "GET",
        url: "{{route('payment_settlement.filter')}}",
        data: {
            start_date: start_date,
            end_date: end_date,
        },
        success: function (data) {
           $('#settlement_result').html(data);
           $('.loading_box').hide();
           $('.loading_box_overlay').hide();
        }
    });
});
</script>
@endpush