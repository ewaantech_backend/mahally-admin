@extends('layouts.master')
@section('content')
@section('content-title')
SUMMARY 
@endsection
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4" >
        </div> 
        <div class="col-md-4 " >
        </div>
        <div class="col-md-4" >
            <label>Select date</label>
            <input type="text" class="form-control" name="dates">
            <i class="fa fa-calendar icon-style"></i>
        </div>
    </div>
</div>
@include('restaurant.dashboard.card_section')
@include('restaurant.dashboard.recent_order')
@include('restaurant.dashboard.top_selling')

<!-- Modal pooup -->
<div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
    @php
$rtl_ext = app()->getLocale() == 'en' ? '' : '-rtl';
@endphp
    <link href="{{asset('/assets/css/dashboard.css')}}" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .right-pos{
            position: absolute;
            right: 35px;
            top: 87px;
        }
        .icon-style{
            position: absolute;
            right: 28px;
            top: 50px;
        }
        .see-more{
            right: 38px;
            position: absolute;
            bottom: 7px;
        }
        .href-style{
            color:  cornflowerblue;
        }
        .scroll {
            max-height: 300px;
            overflow-y: auto;
        }
        hr.divider
        {
            margin-top: 0em;
            margin-bottom: 0em;
            border-width: 2px;
            width: 100%;
        }
        .align-p{
        text-align: center;
    }
    .card [class*="card-header-"] .card-icon, .card [class*="card-header-"] .card-text {
        padding: 5px;
    }
    .card-title{
        font-size: 14px;
    }
    .restaurent_val{
        height: 42px;
    }
   
    </style>
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
$('#datepicker').click(function(){
$('input[name="dates"]').click();
});
$('input[name="dates"]').daterangepicker({maxDate: new Date()});
$('input[name="dates"]').on('apply.daterangepicker', function(ev, picker) {
let start = picker.startDate.format('YYYY/MM/DD');
let end = picker.endDate.format('YYYY/MM/DD');

$.ajax({
type: "POST",
        url: "{{route('getCount')}}",
        data:{
        start_date:start,
                end_date:end
        },
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
            $('#total_sale').text(data.sales_count);
            $('#total_customers').text(data.total_customers);
            $('#total_orders').text(data.total_orders); 
        }
});
});
$('input[name="dates"]').on('cancel.daterangepicker', function(ev, picker) {
window.location.reload();
});

 
</script>
@endpush