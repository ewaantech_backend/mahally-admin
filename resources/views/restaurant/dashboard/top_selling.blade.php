<div class="col-lg-6 ml-0">
    <div class="row">      
        <div class="col-md-6">
            <h5>{{ __('messages.dashboard.top_selling_items') }}</h5>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div id="msgDiv"></div>
            <div class="table-responsive">
                <table class="table table-bordered color-table">
                    <thead>
                        <tr>
                            <th>{{ __('messages.orders.form.sl_no') }}</th>
                            <th>{{ __('messages.dashboard.item_name') }}</th>
                            <th>{{ __('messages.dashboard.restaurant_name') }}</th>
                            <th class="left-align">{{ __('messages.dashboard.sale_count') }}</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($order_item) > 0 )
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($order_item as $item)
                        <tr> 
                            <td>{{$i++}}</td>
                            <td>{{$item->menu_items->lang[0]->name ?? ''}}</td>
                            <td>{{$item->order->restaurant->lang[0]->name ?? ''}}</td>
                             <td>{{$item->item_count ?? ''}}</td>
                        </tr>
                        @endforeach
                            
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No records found!</td>
                        </tr>

                        @endif
                    </tbody>
                </table>
            </div>

    </div>
</div>