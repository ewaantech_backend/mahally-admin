@extends('layouts.master')
@section('content-title')
{{ __('messages.timing.timing') }}
@endsection
@section('add-btn')
<button class="btn btn-primary" id="add_new_timing">
    <i class="ti-plus"></i> {{ __('messages.timing.add_timing') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('messages.timing.table_header.sl_no') }}</th>
                                <th>{{ __('messages.timing.table_header.timing') }}</th>
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($time) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($time as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ date('h:i A', strtotime($row_data->start_time)) }} to {{ date('h:i A', strtotime($row_data->end_time))}}</td>
                               
                                <td class="text-left">
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_user" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Staff" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $time->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="timing_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        Add Timing
                    </strong></h4>
                <button type="button" class="close cancel-btn" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="timing_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="time_unique_id" id='time_unique_id'>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.timing.form.start_time') }} <span class="text-danger">*</span></label>
                            <input class="form-control" type="time" name="start_time" id="start_time" value="00:00" />
                            <label class="start_time_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.timing.form.end_time') }} <span class="text-danger">*</span></label>
                            <input class="form-control" type="time" name="end_time" id="end_time" value="00:00" />
                            <label class="end_time_error"></label>
                        </div>
                        
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-table" id="save_data">
                        {{ __('messages.form.add') }}
                    </button>
                    <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">{{ __('messages.form.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}
@endsection
@push('css')
{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')
{{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#add_new_timing').on('click', function() {
        $('#name_change').html('Add Timing');
        $('#save_data').text('Add').button("refresh");
        $("#timing_form")[0].reset();
        $('#timing_popup').modal({
            show: true
        });
    })
  
    $('.save-table').on('click', function(e) {
        $("#timing_form").validate({
            rules: {
                start_time: {
                    required: true,
                },
                end_time: {
                    required: true,
                },
            },
            messages: {
                start_time: {
                    required: @json(__('messages.timing.validation.start_time') ),
                },
                end_time: {
                    required: @json(__('messages.timing.validation.end_time') ),
                },  
            },
            submitHandler: function(form) {
                time_unique_id = $("#time_unique_id").val();
                $('.loading_box').show();
                $('.loading_box_overlay').show();
                $('button:submit').attr('disabled', true);
                if (time_unique_id) {
                    $.ajax({
                        type: "POST",
                        url: "{{route('timing.update')}}",
                        data: {
                            start_time: $('#start_time').val(),
                            end_time: $('#end_time').val(),
                            time_unique_id: time_unique_id
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#timing_form")[0].reset();
                            } else {
                                // console.log(data.message);
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });


                } else {
                    $.ajax({
                        type: "POST",
                        url: "{{route('timing.store')}}",
                        data: {
                            start_time: $('#start_time').val(),
                            end_time: $('#end_time').val(),
                        },

                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#timing_form")[0].reset();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                            $('button:submit').attr('disabled', false);
                        }
                    });

                }
            }
        })
    });

    $('.edit_user').on('click', function(e) {
        page = $(this).data('id')
        $('#name_change').html('Edit Timing');
        $('#save_data').text('Save').button("refresh");
        var url = "timing/edit/";
        $.get(url + page, function(data) {
            console.log(data);
            $('#start_time').val(data.time.start_time),
            $('#end_time').val(data.time.end_time),    
            $('#time_unique_id').val(data.time.id)
            $('#timing_popup').modal({
                show: true

            });
        });
    });

    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Timing',
            content: 'Are you sure to ' + act_value + ' the timing?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('timing.status.update')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("timing.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
   
    
    function deleteUser(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this Timing? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('timing.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("timing.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }

    $('.cancel-btn').on('click',function(){
        $("#time_unique_id").val('');
    });
</script>
@endpush