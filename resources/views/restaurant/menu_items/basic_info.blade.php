<form id="frm_create_menu_item" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.menu_item_en') }} <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter item name (EN)" type="text" name="name_en" value="{{ isset($row_data['id']) ? $row_data->lang[0]->name : ''}}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.menu_item_ar') }} <span class="text-danger">*</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter item name (AR)" type="text" name="name_ar" value="{{ isset($row_data['id']) ? $row_data->lang[1]->name : ''}}"/>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.item_category') }} <span class="text-danger">*</span></label><br/>
                    <select class="form-control" name="menu_cate_id" id="menu_cate_id">
                        @foreach($menu_category as $category)
                        <option  value="{{ $category->id }}" {{ (isset($row_data['menu_cate_id']) &&  $category->id  == $row_data['menu_cate_id']) ? 'selected' : '' }}>{{ $category->lang[0]->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.calory') }} <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="Enter calory" type="text" id="calory" name="calory" value="{{ isset($row_data['id']) ? $row_data->calory : ''}}"/>
                </div>
                <div class="col-md-12">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.available_in') }} <span class="text-danger">*</span></label><br/>
                    <select class="select2_multiple" name="available_time[]" multiple="multiple" id="available_time">
                        @foreach($available_in as $row_val)
                        <option  value="{{ $row_val->id }}" {{ !empty($menu_abl_in) && in_array($row_val->id, $menu_abl_in) ? 'selected' : '' }}>{{ $row_val->start_time }} to {{ $row_val->end_time }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="available_time_error"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.price') }} <span class="text-danger">*</span></label>
                    <input class="form-control" placeholder="Price" type="text" name="price" value="{{ isset($row_data['id']) ? $row_data->price : ''}}"/>
                </div>
                <div class="col-md-6">
                    <label class="control-label">@if(isset($row_data['id'])){{ __('messages.menu_items.table_header.dis_price')}} @else{{ __('messages.menu_items.form.basic_info.dis_price') }} @endif </label>
                    <input class="form-control" placeholder="Discount Price" type="text" name="discount_price" value="{{ isset($row_data['id']) ? $row_data->discount_price : ''}}"/>
                </div>
                <div class="col-md-12">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.ingredients') }} </label><br/>
                    <select class="select2_multiple" name="ingredients[]" multiple="multiple" id="ingredients">
                        @foreach($ingredients as $row_val)
                        <option  value="{{ $row_val->id }}" {{ !empty($menu_ingredients) && in_array($row_val->id, $menu_ingredients) ? 'selected' : '' }}>{{ $row_val->lang[0]->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="ingredients_error"></label>
                </div>
                <div class="col-md-12">
                    <label class="control-label">{{ __('messages.menu_items.form.basic_info.addons') }} </label><br/>
                    <select class="select2_multiple" name="addons[]" multiple="multiple" id="addons">
                        @foreach($addons as $row_val)
                        <option  value="{{ $row_val->id }}" {{ !empty($menu_addons) && in_array($row_val->id, $menu_addons) ? 'selected' : '' }}>{{ $row_val->lang[0]->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="addons_error"></label>
                </div>
               
               
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>{{ __('messages.menu_items.form.basic_info.description_en') }} <span class="text-danger">*</span></label>
                    <textarea class="form-control area" id="description_en" name="description_en"placeholder="Enter description">{{ isset($row_data['id']) ? $row_data['lang'][0]['description'] : ''}}</textarea>
                    <label class="description_en_error"></label>
                </div> 
                <div class="col-md-6">
                    <label>{{ __('messages.menu_items.form.basic_info.description_ar') }} <span class="text-danger">*</span></label>
                    <textarea class="form-control area" id="description_ar" name="description_ar" placeholder="أدخل سؤالك" style="text-align:right !important">{{ isset($row_data['id']) ? $row_data['lang'][1]['description'] : ''}}</textarea>
                    <label class="description_ar_error"></label>
                </div> 
            </div>
            
        </div>
        <div class="modal-footer">
            <input type="hidden" id="menu_id" name="menu_id" value="{{ isset($row_data['id']) ? $row_data['id'] : '' }}">
            @if(isset($row_data['id']))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.menu_items.form.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                 {{ __('messages.menu_items.form.save_and_continue') }}
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="{{ route('menu_items.get') }}"> {{ __('messages.menu_items.form.back_to_listing') }}</a>
            {{-- <div id="pageloader">
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
</div> --}}
        </div>
    </div>
</form>
<style>
   
      /* Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }
 
      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }

      #target {
        width: 345px;
      }
      .tabpanel{
    overflow-y: initial !important
}
#tab-body{
    height: 80vh;
    overflow-y: auto;
}
.area{
        height: 100px;
    }
    .select2-container{
        width: 100% !important;
    }

</style>
<script>
     
    $(".select2_multiple").select2({
        placeholder: "Select Multiple Options",
        allowClear: true
    });
     $(".select2_multiple").on('select2:select',function (e){
        $(".available_time_error").text('');
    });

     $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "100%"});
    
     $(".save-and-continue").on('click', function (e) {

        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function (e) {
        $("#submit_action").val('save');
    });

    $("#frm_create_menu_item").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            name_en: {
                required: true,
            },
            name_ar: {
                required: true,
            },
            "available_time[]":{
              required: true,
            },
            menu_cate_id: {
                required: true,
            },
            calory: {
                required: true,
            },
            price: {
                required: true,
                number:true,
            }, 
            // discount_price: {
            //     required: true,
            //     number:true,
            // }, 
            // "ingredients[]":{
            //   required: true,
            // },
            //  "addons[]":{
            //   required: true,
            // },
            description_en: {
                required: true,
            },
            description_ar: {
                required: true,
            },    
        },
        messages: {
            name_en: {
                required: 'Item name(EN)  required.'
            },
            name_ar: {
                required: 'Item name(AR)  required.'
            },
            "available_time[]":{
              required: "Available In  required.",
            },
            menu_cate_id: {
                required: "Menu Category  required.",
            },
            calory: {
                required: "Calory  required",
            },
            price: {
                required: "Price required",
                number: "Accept only numeric value"
            },
            // discount_price: {
            //     required: "Discount price required",
            //     number: "Accept only numeric value"
            // }, 
            // "ingredients[]":{
            //   required: "Ingredients required",
            // },
            //  "addons[]":{
            //   required: "Addons required",
            // },
            description_en: {
                required: "Description (EN) required",
            },
            description_ar: {
                required: "Description (AR) required",
            }, 
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("id") == "available_time") {
                error.insertAfter("#available_time_error");
            } else {
                error.insertAfter(element);
            }
            // if (element.attr("id") == "ingredients") {
            //     error.insertAfter("#ingredients_error");
            // } else {
            //     error.insertAfter(element);
            // }
            // if (element.attr("id") == "addons") {
            //     error.insertAfter("#addons_error");
            // } else {
            //     error.insertAfter(element);
            // }
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $('button:submit').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{route('save_menu_basic_info')}}",
                data: $('#frm_create_menu_item').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#item_tabs a[href="#photos"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $("#rest_tab").tabs({cache: true});
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
</script>
 
