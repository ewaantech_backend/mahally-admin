<form id="frm_create_related_items" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">  
          <div class="row attributes">
            <div class="col-md-12">
                <label class="control-label">{{ __('messages.menu_items.form.related_item.related_item') }}</label><br/>

                <select class="select2_multiple" id="select2_menu_items" name="related_items[]" multiple="multiple" id="related_items">
                    @foreach($menu_items as $row_val)
                    <option  value="{{ $row_val->id }}" {{ !empty($exist_items) && in_array($row_val->id, $exist_items) ? 'selected' : '' }} style="width:1000">{{ $row_val->lang[0]->name }}</option>
                    @endforeach
                </select>
                <label class="control-label" id="related_items_error"></label>
            </div> 
          </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="menu_id" name="menu_id" value="{{ $menu_id }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($menu_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.menu_items.form.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                {{ __('messages.menu_items.form.save_and_continue') }}
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $menu_id }}' href="javascript:void(0);">{{ __('messages.menu_items.form.back') }}</a>
        </div>
    </div>
</form>
<style>
    .select2-container{
        width: 100% !important;
    }
</style>
<script>
      $("#select2_menu_items").select2({
        placeholder: "Select Multiple Options",
        allowClear: true
    });
     $("#select2_menu_items").on('select2:select',function (e){
        $("#related_items_error").text('');
    });
   
     $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "100%"});
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });

    $('.tab_back').on('click', function () {
          $("#frm_create_related_items")[0].reset();
        var menu_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('item_tabs')}}",
            data: {'activeTab': 'ATTRIBUTES', menu_id: menu_id},
            success: function (result) {
                $('#item_tabs a[href="#attributes"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    
    $("#frm_create_related_items").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            // "related_items[]": {
            //     required: true,
            // },
        },
        messages: {
            // "related_items[]": {
            //     required: 'Related items required',
            // },
        },
        errorPlacement: function (error, element) {
            // if (element.attr("id") == "related_items") {
            //     error.insertAfter("#related_items_error");
            // } else {
            //     error.insertAfter(element);
            // }
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $('button:submit').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{route('save_related_items')}}",
                data: $('#frm_create_related_items').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                            window.setTimeout(function() {
                                window.location.href = '{{route("menu_items.get")}}';
                            }, 1000);
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });

</script>