<form id="frm_create_photo" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="div-photos">
                <div class="row"><h5 style="padding-left: 20px;">{{ __('messages.menu_items.form.photos.photos') }}</h5></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="mb-2">{{ __('messages.menu_items.form.photos.add_cover_photo') }}</div>
                        <div class="cover-photo" id="pdt_cover_image">
                            <div id="cover-photo-upload" class="add">+</div>
                            {{-- <div id="photo_loader" class="loader" style="display: none;">
                                <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                            </div> --}}
                            <div class="preview-image-container" @if(empty($cover_image)) style="display:none;" @endif id="cover_photo_image_preview">
                                <div class="scrn-link" style="position: relative;top: -20px;">
                                    <button type="button" class="scrn-img-close delete-cover" data-type="cover" data-id="{{ $menu_id ?? ''}}">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img class="scrn-img" style="max-width: 200px" src="{{ !empty($cover_image) ? url('uploads/'.$cover_image) : '' }}" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="control-fileupload" style="display: none;">
                            <label for="cover_photo" data-nocap="1">{{ __('messages.menu_items.form.photos.sel_cover_photo') }}</label>
                            <input type="file" id="cover_photo" name="cover_photo" data-id="{{ $menu_id }}" data-imgw="180" data-imgh="180"/>
                            
                        </div>
                        <div class="mt-2 small">
                            <p>Max file size: 1024KB<br />
                                Supported formats: jpeg,png<br />
                                File dimension: 180 x 180 pixels<br />
                            </p>
                            <span style="display: none;" class="error" id="cover_photo-error">Error</span>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="mb-2">{{ __('messages.menu_items.form.photos.rest_images') }}</div>
                        <div>
                            <div class="uploaded-images">
                                @foreach ($photos as $photo)
                                <div class="scrn-crd scrn-link" id='pdt_image_val_{{ $photo->id }}'>
                                    <button type="button" class="scrn-img-close delete-pdt_image" data-type="pdt_image" data-id="{{ $photo->id }}">
                                        <i class="ti-close"></i>
                                    </button>
                                    <img class="scrn-img" src="{{ !empty($photo->image_path) ? url('uploads/'.$photo->image_path): '' }}" alt="">
                                </div>
                                @endforeach
                                <div class="cover-photo" id="photos-upload">
                                    <div class="add">+</div>
                                </div>
                            </div>
                        </div>

                        <input type="file" id="pdt_image" name="pdt_image" style="display: none;" data-id="{{ $menu_id }}" data-imgw="800" data-imgh="800"/>
                        <div class="mt-2 small">
                            <p>Max file size: 1024KB<br />
                                Supported formats: jpeg,png<br />
                                File dimension: 800 x 800 pixels<br />
                            </p>

                            <span style="display: none;" class="error" id="photos-error"></span>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <div class="modal-footer">
            <input type="hidden" id="menu_id" name="menu_id" value="{{ $menu_id }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($menu_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                {{ __('messages.menu_items.form.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                 {{ __('messages.menu_items.form.save_and_continue') }}
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $menu_id }}' href="javascript:void(0);">{{ __('messages.menu_items.form.back') }}</a>
        </div>
    </div>
</form>
<!-- For cropping -->
<div class="modal none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('messages.menu_items.form.photos.crop_img') }}</h5>
                <button type="button" class="close" data-dismiss-modal="modal2" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-6">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">{{ __('messages.menu_items.form.photos.crop_and_save') }}</button>
                <button type="button" class="btn btn-secondary" data-dismiss-modal="modal2">{{ __('messages.menu_items.form.photos.close') }}</button>
            </div>
        </div>
    </div>
</div>

<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }


/* Important part */
.tabpanel{
    overflow-y: initial !important
}
#tab-body{
    height: 80vh;
    overflow-y: auto;
}
</style>
<script>
    $('input[name="expire_in[]"]').daterangepicker({
    "singleDatePicker": true,
    // "autoUpdateInput": false,
    "autoApply": true,
    'changeMonth': true,
    'changeYear': true,
    locale: {
        format: 'DD-M-YYYY'
       
    }
});
</script>
<script>
  
    $('#cover-photo-upload').click(function (e) {
    $('#cover_photo').click();
    });
    $('#cover_photo').on('change', function () {
        var id = $(this).data("id");
        var imgw = $(this).data('imgw');
        var imgh = $(this).data('imgh');
        const file = $(this)[0].files[0];
        readUrl(file, 'cover_photo', uploadFile, imgw, imgh, id);
    });


    $('#photos-upload').click(function() {
    $('#pdt_image').click();
    });
    $('#pdt_image').on('change', function() {
        var id = $(this).data("id");
        var imgw = $(this).data('imgw');
        var imgh = $(this).data('imgh');
        const file = $(this)[0].files[0];
        img = new Image();
        var imgwidth = 0;
        var imgheight = 0;
        var _URL = window.URL || window.webkitURL;
        img.src = _URL.createObjectURL(file);
        img.onload = function() {
            imgwidth = this.width;
            imgheight = this.height;
             readUrl(file, '', uploadPhotos, imgw, imgh, id);
            // if (imgwidth >= imgw && imgheight >= imgh){
            //     readUrl(file, '', uploadPhotos, imgw, imgh, id);
            // } else{
            //     $('input[type="file"]').val('');
            //     Toast.fire({
            //     icon: 'error',
            //     title: 'Image size must be greater or equal to ' +imgw+' X '+imgh,
            //     });
            // }
        }
    });

    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('menu_id', id);
    formData.append('upload_type', 'single');
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    $.ajax({
    type: "POST",
            url: "{{route('upload_item_images')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#photo_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
                console.log(data);
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#pdt_cover_image').html('<div id="cover-photo-upload" class="add">+</div>' +
                    '<div id="photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="cover_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-cover" data-type="cover" data-id="' + data.menu_id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.image_path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-cover").click(function(e) {' +
                    'delete_image(' + data.menu_id + ' , "cover");' +
                    '});<\/script>');
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`photo_loader`).hide();
            }
            $('.loading_box').hide();
            $('.loading_box_overlay').hide();
            }
    });
    }

   

    function uploadPhotos(photos, type, id) {

    if (!Array.isArray(photos)) {
    photos = [photos]
    }

    var formData = new FormData();
    for (let i = 0; i < photos.length; i++) {
    formData.append('photos[]', photos[i]);
    }

    formData.append('upload_type', 'multiple');
    formData.append('menu_id', id);
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    $.ajax({
    url: "{{route('upload_item_images')}}",
            type : 'POST',
            data : formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            $(`#photos_loader`).show();
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('.uploaded-images').prepend('<div class="scrn-crd scrn-link" id="pdt_image_val_' + data.image_id + '">' +
                    '<button type="button" class="scrn-img-close delete-pdt_image" id="del_image_' + data.image_id + '" data-type="pdt_image" data-id="' + data.image_id + '">' +
                    '<i class="ti-close"></i></button>' +
                    '<img class="scrn-img" src="' + data.image_path + '" alt=""></div>' +
                    '<script>' +
                    '$("#del_image_' + data.image_id + '").click(function(e) {' +
                    'delete_image(' + data.image_id + ', "pdt_image");' +
                    '});<\/script>');
            $('input[type="file"]').val('');
            $('#photos_loader').hide();
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`#photos_loader`).hide();
            }
            $('.loading_box').hide();
            $('.loading_box_overlay').hide();
        }
    });
    }

    $('.delete-pdt_image').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });

    $('.delete-cover').click(function(e) {
    let id = $(this).data('id');
    let type = $(this).data('type');
    delete_image(id, type);
    });

    function delete_image(id, type){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('detete_item_img')}}",
                    data: {
                    id: id,
                    type: type
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {     
                        if (data.status == 1) {
                            Toast.fire({
                            icon: 'success',
                                    title: data.message
                            });
                            if (data.type == 'cover') {
                            $('#pdt_cover_image').html('<div id="cover-photo-upload" class="add">+</div>' +
                                    '<div id="cover_photo_loader" class="loader" style="display: none;"><img src="' + data.loader_image + '" alt=""></div>' +
                                    '<div class="preview-image-container" style="display: none;" id="cover_photo_image_preview">' +
                                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                                    '<button type="button" class="scrn-img-close" data-type="cover" data-id="' + data.id + '">' +
                                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                                    '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                                    '</div></div>' +
                                    '<script>' +
                                    '$("#cover-photo-upload").click(function (e) {' +
                                    '$("#cover_photo").click();});<\/script>');
                            }
                            else
                            {
                                $("#pdt_image_val_" + data.id).remove();
                            }
                        } else {
                            Toast.fire({
                            icon: 'error',
                                    title: data.message
                            });
                        }
                    }
            });
            },
                No: function () {
                console.log('cancelled');
                }
            }
    });
    }
    
    
    
    $("button[data-dismiss-modal=modal2]").click(function () {
    $('#image-modal').modal('hide');
    });
</script>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });

    $(".add_price_btn_simple").click(function () {
        var cnt = $(this).attr("data-id");
        var div_cnt = cnt + 1;
        $(".cls_outer_div1").append('<div class="row ml-0 cls_content_div' + div_cnt + '"><div class="row"><div class="col-md-11"> <div class="row"><div class="col-md-4">' +
                '<input class="form-control form-white" placeholder="Enter license title" type="text" name="license_title[]" value="" id="license_' + div_cnt + '"/></div>' +
                '<div class="col-md-4"><div class="input-group date" id="datepicker"><input class="form-control" type="text" name="expire_in[]" id="expire_in_' + div_cnt + '" placeholder="Select date" autocomplete="off"><span class="input-group-addon"><span class="fa fa-calendar icon-style"></span></span></div></div>'+
                '<div dmt_photoclass="col-md-4"><div class="row"><div class="col-md-12">'+
                '<div id="photos_loader" class="loader" style="display: none;"><img src="{{ asset("assets/images/loader.gif") }}" alt=""></div>'+
                '<div class="cover-photo" id="cr_image_' + div_cnt + '"><div class="cr-copy" id="crcopy_' + div_cnt + '"><div class="add">+</div></div></div></div>'+
                '<input type="file" class="cr-file" id="crcopy_' + div_cnt + '" name="cr_copy" style="display: none;" data-id="{{ $menu_id }}" data-imgw="800" data-imgh="800"/>'+                     
                '</div></div></div></div>' +
                '<div class="col-md-1 text-left"><button class="btn btn-sm btn-danger text-white btn_remove" title="Remove Attributes">' +
                '<i class="fa fa-minus"></i></button></div></div></div>');
        cnt++;
        $('#add_price_simple').attr('data-id', cnt);
    });
    $('body').on('click', '.btn_remove', function () {
        $(this).parent().parent().remove();
//        cnt--;
    });




    $('.tab_back').on('click', function () {
        var menu_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('item_tabs')}}",
            data: {'activeTab': 'BASIC INFO', menu_id: menu_id},
            success: function (result) {
                $('#item_tabs a[href="#basic_info"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
    $("#frm_create_photo").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
           
        },
        messages: {
           
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $('button:submit').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{route('save_photos')}}",
                data: $('#frm_create_photo').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#item_tabs a[href="#attributes"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });

</script>
