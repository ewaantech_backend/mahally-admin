<div class="modal-header">
    <h4 class="modal-title" id="name_change"><strong>{{ __('messages.menu_items.sort_item') }}</strong></h4>     
    <a href="{{route('menu_items.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6">
            <label class="control-label">{{ __('messages.menu_items.form.sort_items.menu_category') }} <span class="text-danger">*</span></label><br/>
            <select class="form-control" name="sort_menu_category" id="sort_menu_category">
                <option value="">{{ __('messages.menu_items.form.sort_items.sel_category') }}</option>   
            @foreach($menuCategory as $category)
            <option  value="{{ $category->id }}" >{{ $category->name }}</option>
            @endforeach
        </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
                <div class="table-responsive">
                <table  id="table" class="table table-bordered table-striped item-table">
                    <thead>
                        <tr>
                            <th>{{ __('messages.menu_items.form.sort_items.sl_no') }}</th>
                            <th>{{ __('messages.menu_items.form.sort_items.menu_item_en') }}</th>
                            <th>{{ __('messages.menu_items.form.sort_items.menu_item_ar') }}</th>
                            <th style="display: none;">{{ __('messages.menu_items.form.sort_items.menu_cate') }}</th>
                        </tr>
                    </thead>
                    <tbody id="tablecontents">
                        @if (count($menuSortItems) > 0)
                        @php
                        $i = 1;
                        @endphp

                        @foreach ($menuSortItems as $row_data)
                        <tr class="row1" data-id="{{ $row_data->id }}">
                            <td class="pl-3"><i class="fa fa-sort"></i>  {{ $i++ }}</td>
                            <td>{{ $row_data->lang[0]->name}}</td>
                            <td>{{ $row_data->lang[1]->name}}</td>  
                            <td style="display: none;">{{ $row_data->menu_cate_id}}</td>      
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No records found!</td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div> 
    </div>    
</div>
<style>
    .modal-dialog {
    margin: 30px auto;
    position: relative;
    top: 50%;
    transform: translateY(-50%) !important;
    width: 51%;
}
</style>
<script type="text/javascript">
     var table =  $('#table').DataTable({
            "paging": false,
            "bInfo": false,
        });
      

        $( "#tablecontents" ).sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index,element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index+1
            });
          });

          $.ajax({
            type: "POST", 
            dataType: "json", 
            url: '{{route("menu_items.sortable")}}',
                data: {
              order: order,
              _token: token
            },
            success: function(response) {
                if (response.status == "success") {
                  console.log(response);
                } else {
                  console.log(response);
                }
            }
          });
        } 
        $('#sort_menu_category').on('change', function () {
                    table.columns(3).search( this.value ).draw();
        } ); 
        
    </script>
