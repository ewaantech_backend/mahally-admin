<form id="frm_create_attributes" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body">  
          <div class="row attributes">
            <div class="col-md-4"><label class="control-label">{{ __('messages.menu_items.form.attributes.attri') }}</label><br/></div>
             @if(count($MenuAttr) > 0)
            <div class="col-md-4"><label class="control-label attr_val_label">{{ __('messages.menu_items.form.attributes.attribute_val') }}</label><br/></div>
            @else 
            <div class="col-md-4"><label class="control-label attr_val_label" style="display: none;">{{ __('messages.menu_items.form.attributes.attribute_val') }}</label><br/></div>
            @endif
            <div class="col-md-2"></div>
            @if(count($MenuAttr) > 0)
              @foreach ($MenuAttr as $key => $row_data)
                @php
                  $cnt = ++$key;  
                @endphp
                <div class="col-md-4" id="div_attr_{{ $cnt}}">   
                    <select class="form-control sel_attr" name="attr[]" id={{ $cnt}} data-type="edit">
                        <option  value=" ">{{ __('messages.menu_items.form.attributes.sel_attribute') }}</option>
                        @foreach($attributes as $item)
                        <option  value="{{ $item->id }}" {{ (isset($row_data['attribute_id']) &&  $item->id  == $row_data['attribute_id']) ? 'selected' : '' }}>{{ $item->lang[0]->name }}</option>
                        @endforeach
                    </select>
                </div>
                @php
                    $menuAttrValue = \App\Models\RestaurantAttributesValues::with('lang')->where('rest_attribute_id', $row_data['attribute_id'])->get();
                @endphp
                <div class="col-md-4" id="div_attr_value_{{ $cnt}}">   
                    <select class="form-control" name="attribute_values[]" id="value_{{$cnt}}">
                        <option value="">Nil</option>
                        @foreach($menuAttrValue as $item)
                        <option  value="{{ $item->id }}" {{ (isset($row_data['attribute_value_id']) &&  $item->id  == $row_data['attribute_value_id']) ? 'selected' : '' }}>{{ $item->lang[0]->name }}</option>
                        @endforeach
                    </select>
                </div>
                 <div class="col-md-2 text-left" id="div_remove_{{ $cnt}}">
                    <button class="btn btn-sm btn-danger text-white att_remove" type="button" title="Remove Attributes" data-id="{{ $cnt}}" data-type="edit"><i class="fa fa-minus"></i></button>
                </div>
              @endforeach
              <div class="col-md-4" id="div_attr_{{ ++$cnt}}">   
                <select class="form-control sel_attr" name="attr[]" id="{{$cnt}}" data-type="edit">
                    <option  value=" ">{{ __('messages.menu_items.form.attributes.sel_attribute') }}</option>
                    @foreach($attributes as $item)
                    <option  value="{{ $item->id }}">{{ $item->lang[0]->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4" id="div_attr_value_{{ $cnt}}"></div>
            <div class="col-md-2" id="div_remove_{{ $cnt}}"></div>   
            @else
            <div class="col-md-4" id="div_attr_1">   
                <select class="form-control sel_attr" name="attr[]" id="1" data-type="add">
                    <option  value=" ">{{ __('messages.menu_items.form.attributes.sel_attribute') }}</option>
                    @foreach($attributes as $item)
                    <option  value="{{ $item->id }}">{{ $item->lang[0]->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4" id="div_attr_value_1"></div>
            <div class="col-md-2"></div>  
            <div class="col-md-4" id="div_attr_2">   
                <select class="form-control sel_attr" name="attr[]" id="2" style="display: none;" data-type="add">
                    <option  value=" ">{{ __('messages.menu_items.form.attributes.sel_attribute') }}</option>
                    @foreach($attributes as $item)
                    <option  value="{{ $item->id }}">{{ $item->lang[0]->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4" id="div_attr_value_2"></div>
             <div class="col-md-2 text-left" id="div_remove_2">
                    
            </div>  
            <div class="col-md-4" id="div_attr_3">   
                <select class="form-control sel_attr" name="attr[]" id="3" style="display: none;" data-type="add">
                    <option  value=" ">{{ __('messages.menu_items.form.attributes.sel_attribute') }}</option>
                    @foreach($attributes as $item)
                    <option  value="{{ $item->id }}">{{ $item->lang[0]->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4" id="div_attr_value_3"></div>
             <div class="col-md-2 text-left" id="div_remove_3">
                  
            </div>  
            <div class="col-md-4" id="div_attr_4">   
                <select class="form-control sel_attr" name="attr[]" id="4" style="display: none;" data-type="add">
                    <option  value=" ">{{ __('messages.menu_items.form.attributes.sel_attribute') }}</option>
                    @foreach($attributes as $item)
                    <option  value="{{ $item->id }}">{{ $item->lang[0]->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4" id="div_attr_value_4"></div>
             <div class="col-md-2 text-left" id="div_remove_4">
                    
            </div> 
            @endif 
          </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" id="menu_id" name="menu_id" value="{{ $menu_id }}">
            <input type="hidden" id="submit_action" value="" />
            @if(!empty($menu_id))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                 {{ __('messages.menu_items.form.save') }}
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                 {{ __('messages.menu_items.form.save_and_continue') }}
            </button>
            <a class="btn btn-default waves-effect tab_back" data-id='{{ $menu_id }}' href="javascript:void(0);"> {{ __('messages.menu_items.form.back') }}</a>
        </div>
    </div>
</form>
<script>
    $(".save-and-continue").on('click', function () {
        $("#submit_action").val('continue');
    });
    $(".save-btn").on('click', function () {
        $("#submit_action").val('save');
    });

    $('.tab_back').on('click', function () {
          $("#frm_create_attributes")[0].reset();
        var menu_id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('item_tabs')}}",
            data: {'activeTab': 'PHOTOS', menu_id: menu_id},
            success: function (result) {
                $('#item_tabs a[href="#photos"]').tab('show');
                $('.tab-content').html(result);
            }
        });
    });
     $(".sel_attr").on('change', function () {
        id = $(this).attr('id');
        // alert($(this).attr('data-type')) ;
        // alert(id) ;
        $(".attr_val_label").show();
        cnt = id;
        if($(this).attr('data-type') == 'add'){
            cnt++;
        }
        sel_attr_id =$("#"+id).val();
        $.ajax({
            type: "GET",
            url: "{{route('getAttributeValue')}}",
            data: {attr_id: sel_attr_id},
            success: function (result) {
                $("#div_attr_value_" + id).empty();
                $("#div_remove_"+id).empty();
                $("#div_attr_value_" + id).append('<select class="form-control" name="attribute_values[]" id="value_'+ id+'"></select>');
                if(id != '1'){
                $("#div_remove_"+id).append('<button class="btn btn-sm btn-danger text-white att_remove" type = "button" title="Remove Attributes" data-id="'+id+'" data-type="add"><i class="fa fa-minus"></i></button>'+
              '<script>' +
                    '$(".att_remove").click(function(e) {' +
                   'delete_attributes(' + id + ', "add");' +
                    '});<\/script>')
                }
                $("#value_"+id).empty();
                $("#value_"+id).append('<option  value=" ">Nil</option>');
                $.each( result.attribute_value, function( key, value ) {
                   $("#value_"+id).append('<option  value="'+ value.id +'">'+ value.lang[0].name +'</option>');
                });
                $("#div_attr_"+cnt).show(); 
            }
        });
    });

    $("#frm_create_attributes").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            // "attr[]": {
            //     required: true,
            // },
            // "attribute_values[]": {
            //     required: true,
            // },
        },
        messages: {
            "attr[]": {
                required: 'Attributes is required',
            },
            "attribute_values[]": {
                required: 'Attributes is value is required',
            },
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $('button:submit').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{route('save_attributes')}}",
                data: $('#frm_create_attributes').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#item_tabs a[href="#related_items"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $("#rest_tab").tabs({cache: true});
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
    $(".att_remove").on('click',function(){
        var type = $(this).attr('data-type');
        var id = $(this).attr('data-id');
        delete_attributes(id, type);
     })
     
     function delete_attributes(id, type){
         if(type == 'add'){
            $("#div_attr_value_"+id).empty();
             $("#div_remove_"+id).empty();
             $("#"+ id +" option[value=' ']").prop("selected", true);

        }else{
             $("#div_attr_value_"+id).remove();
             $("#div_remove_"+id).remove();
             $("#div_attr_"+id).remove();
        }
     }

</script>