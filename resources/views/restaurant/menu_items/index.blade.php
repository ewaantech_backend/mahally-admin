@extends('layouts.master')

@section('content-title')
{{ __('messages.menu_items.menu_items') }}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="sort_items">
    <i class="ti-plus"></i> {{ __('messages.menu_items.sort_item') }}
</button>
{{-- <button  class="btn btn-info menu_modal_btn" id="sort_items">
    <i class="ti-plus"></i> Add New Item
</button> --}}
<a href="javascript:void(0);" class="btn btn-info menu_modal_btn">
    <font style="vertical-align: inherit;"><i class="ti-plus"></i> {{ __('messages.menu_items.add_menu_item') }}
    </font>
</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">                 
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('messages.menu_items.table_header.sl_no') }}</th>
                                <th>{{ __('messages.menu_items.table_header.menu_item_en') }}</th>
                                <th>{{ __('messages.menu_items.table_header.menu_item_ar') }}</th>
                                <th>{{ __('messages.menu_items.table_header.menu_cate') }}</th>
                                <th>{{ __('messages.menu_items.table_header.price') }}</th>
                                <th>{{ __('messages.menu_items.table_header.dis_price') }}</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($menuItems) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($menuItems as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->lang[1]->name ?? '' }}</td>
                                @php 
                                    $cateData = \App\Models\MenuCategory::with('lang')->where('id', $row_data->menu_cate_id)->first();
                                @endphp
                                <td>{{ $cateData->lang[0]->name  ?? '' }}</td>
                                <td>{{ $row_data->price ?? '' }}</td>
                                <td>{{ $row_data->discount_price ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white menu_modal_btn" title="Edit Restaurant" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white item_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $menuItems->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Sort item Popup --}}
{{-- <div class="modal fade" id="menu_sort_item_popup" tabindex="-1" role="dialog" aria-labelledby="sortModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" id="sort">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>Sort Items</strong></h4>     
                <a href="{{route('menu_items.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <label class="control-label">Menu Category <span class="text-danger">*</span></label><br/>
                        <select class="form-control" name="sort_menu_category" id="sort_menu_category">
                         <option value="">Select Category</option>   
                        @foreach($menuCategory as $category)
                        <option  value="{{ $category->id }}" >{{ $category->name }}</option>
                        @endforeach
                    </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                            <div class="table-responsive">
                            <table  id="table" class="table table-bordered table-striped item-table">
                                <thead>
                                    <tr>
                                        <th>S.No.</th>
                                        <th>Menu Items (EN)</th>
                                        <th>Menu Items (AR)</th>
                                        <th style="display: none;">Menu Category</th>
                                    </tr>
                                </thead>
                                <tbody id="tablecontents">
                                    @if (count($menuSortItems) > 0)
                                    @php
                                    $i = 1;
                                    @endphp

                                    @foreach ($menuSortItems as $row_data)
                                    <tr class="row1" data-id="{{ $row_data->id }}">
                                        <td class="pl-3"><i class="fa fa-sort"></i>  {{ $i++ }}</td>
                                        <td>{{ $row_data->lang[0]->name}}</td>
                                        <td>{{ $row_data->lang[1]->name}}</td>  
                                        <td style="display: none;">{{ $row_data->menu_cate_id}}</td>      
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="8" class="text-center">No records found!</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div> 
                </div>    
            </div>
        </div>
    </div>
</div> --}}
{{-- popup --}}
<div class="modal fade" id="rest-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog mw-100" role="document">
        <div class="modal-content">
            
          
        </div>
    </div>
</div>
<!-- End Modal -->


@endsection
@push('css')
{{-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }
    .modal {
    
}
.dataTables_filter{
    color: #333;
    display: none;
}

</style>

@endpush
@push('scripts')
{{-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> --}}
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        

        $('.menu_modal_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('create_menu_item')}}",
            data: {
                id: id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#rest-popup').modal('show');
            }
        });
    });
    $('.view_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "products/details/" + id,
            success: function(data) {
                $('.modal-content').html(data);
                $('#rest-popup').modal('show');
            }
        });
    });
    $('#sort_items').on('click', function() {
        $('#name_change').html('Sort Items');
        // $('#menu_sort_item_popup').modal({
        //     show: true
        // });
        $.ajax({
            type: "GET",
            url: "{{route('sort_item')}}",
            data: {
                
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#rest-popup').modal('show');
            }
        });
    })
    $('.change-status').on('click', function() {
        var rest_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' menu item',
            content: 'Are you sure to ' + act_value + ' the menu item?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('menu_items.status.update')}}",
                        data: {
                            id: rest_id,
                            status:act_value,
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("menu_items.get")}}';
                                }, 1000);
                                //                    window.location.reload();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                                 window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

   $('.item_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Menu Item? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
           
                    $.ajax({
                        url: "{{route('menu_items.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("menu_items.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });
      
    </script>
    {{-- <script type="text/javascript">
     var table =  $('#table').DataTable({
            "paging": false,
            "bInfo": false,
        });
      

        $( "#tablecontents" ).sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index,element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index+1
            });
          });

          $.ajax({
            type: "POST", 
            dataType: "json", 
            url: '{{route("menu_items.sortable")}}',
                data: {
              order: order,
              _token: token
            },
            success: function(response) {
                if (response.status == "success") {
                  console.log(response);
                } else {
                  console.log(response);
                }
            }
          });
        } 
        $('#sort_menu_category').on('change', function () {
                    table.columns(3).search( this.value ).draw();
        } ); 
        
    </script> --}}
    
@endpush