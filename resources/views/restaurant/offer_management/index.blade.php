@extends('layouts.master')

@section('content-title')
{{ __('messages.offer_management.offer_management') }}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="offer_add_btn">
    <i class="ti-plus"></i> {{ __('messages.offer_management.add_offer_management') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>    
                                <th>{{ __('messages.offer_management.table_header.offer_title_en') }}</th>
                                <th>{{ __('messages.offer_management.table_header.item') }}</th>
                                <th>{{ __('messages.offer_management.table_header.dis_type') }}</th>
                                <th>{{ __('messages.offer_management.table_header.dis_value') }}</th>
                                <th>{{ __('messages.offer_management.table_header.offer_expire_in') }}</th>
                                <th>{{ __('messages.offer_management.table_header.coupon_code') }}</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($offer) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($offer as $row_data)
                            <tr>   
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                @php 
                                    $itemData = \App\Models\MenuItems::with('lang')->where('id', $row_data->menu_item_id)->first();
                                @endphp
                                <td>{{ $itemData->lang[0]->name ?? '' }}</td>
                                <td>{{$row_data->discount_type ?? ''}}</td>
                                <td>{{$row_data->discount_value ?? ''}}</td>
                                <td>{{date('d-m-yy', strtotime($row_data->expire_in)) ?? ''}} </td>
                                <td>{{$row_data->coupon_code ?? ''}}</td>
                                <td class="text-center">
                                     <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit offer_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a> 
                                     <a class="btn btn-sm btn-danger text-white" title="Delete Addons" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                               
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $offer->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="offer-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"></h4>
                 <a href="{{route('offer_management')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="offer-form" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>{{ __('messages.offer_management.form.offer_title_en') }} <span class="text-danger">*</span></label>
                            <input type="text" placeholder="{{ __('messages.offer_management.placeholder.etr_offer_title_en') }}" class="form-control form-white" id="title_en" name="title_en" value="{{ old('title_en') }}">
                            <label class="title_en_error error-msg"></label>
                        </div> 
                         <div class="col-md-6">
                            <label>{{ __('messages.offer_management.form.offer_title_ar') }} <span class="text-danger">*</span></label>
                             <input type="text" placeholder="{{ __('messages.offer_management.placeholder.etr_offer_title_ar') }}" class="form-control form-white text-right" id="title_ar" name="title_ar" value="{{ old('title_ar') }}">
                            <label class="title_ar_error error-msg"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.offer_management.form.sel_item') }} <span class="text-danger">*</span></label><br/>
                            <select class="form-control" name="menu_item_id" id="menu_item_id">
                                @foreach($menuItemList as $item)
                                <option  value="{{ $item->id }}" {{ (isset($row_data['menu_item_id']) &&  $item->id  == $row_data['menu_item_id']) ? 'selected' : '' }}>{{ $item->lang[0]->name }}</option>
                                @endforeach
                            </select>
                            <label class="menu_item_id_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.offer_management.form.expire_in') }} <span class="text-danger">*</span></label>
                            <div class='input-group date' id='datepicker'>
                                <input class="form-control" type="text" name="expire_in" id="expire_in" placeholder="{{ __('messages.offer_management.placeholder.sel_date') }}" autocomplete="off">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                            <label class="expire_in_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.offer_management.form.dis_type') }} <span class="text-danger">*</span></label><br>
                            <input class="radio-btn dis-type" type="radio" name="discount_type" value="Percentage" id="discount_type" checked>Percentage
                            <input class="radio-btn dis-type" type="radio" name="discount_type" value="Value" id="discount_type">Value
                        </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-6">
                            <label class="control-label">{{ __('messages.offer_management.form.dis_value') }} <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="{{ __('messages.offer_management.placeholder.etr_value') }}" type="text" id="discount_value" name="discount_value" />
                            <label class="discount_value_error"></label>
                        </div>
                        <div class="col-md-6">
                             <label class="control-label">{{ __('messages.offer_management.form.coupon_code') }} </label>
                            <input class="form-control form-white" placeholder="{{ __('messages.offer_management.placeholder.coupon_code') }}" type="text" id="coupon_code" name="coupon_code" />
                            <label class="discount_value_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label>{{ __('messages.offer_management.form.img') }}</label>
                            <div class ="imgup">
                                <div class="scrn-link">
                                    <button type="button" class="scrn-img-close delete-img-en" data-id="" onclick="removesrc_en()" data-type="app_image" style="display: none;">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img id="previewimage_en" class="cover-photo" onclick="$('#uploadFile_en').click();" src="{{ asset('assets/images/upload.png') }}" />
                                </div>
                                <input type="file" id="uploadFile_en" name="upload_image_en" style="visibility: hidden;" accept="image/*" value="" />
                                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                                <input type="hidden" name="delete_image_en" id="delete_image_en" value="0">
                            </div>
                            <span class="error"></span>
                            <div class="catimg">
                                <p class="small">Max file size: 1MB</p><br>
                                <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p><br>
                                <p class="small" style="margin-top:-30px">File dimension: 1200 x 360 pixels</p>
                            </div>
                            <label class="upload_image_en_error"></label>
                        </div> 
                        <div class="col-md-6">
                            <label>{{ __('messages.offer_management.form.img') }}</label>
                            <div class ="imgup">
                                <div class="scrn-link">
                                    <button type="button" class="scrn-img-close delete-img-ar" data-id="" onclick="removesrc_ar()" data-type="app_image" style="display: none;">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img id="previewimage_ar" class="cover-photo" onclick="$('#uploadFile_ar').click();" src="{{ asset('assets/images/upload.png') }}" />
                                </div>
                                <input type="file" id="uploadFile_ar" name="upload_image_ar" style="visibility: hidden;" accept="image/*" value="" />
                                <input type="hidden" name="hidden_image_ar" id="hidden_image_ar">
                                <input type="hidden" name="delete_image_ar" id="delete_image_ar" value="0">
                            </div>
                            <span class="error"></span>
                            <div class="catimg">
                                <p class="small">Max file size: 1MB</p><br>
                                <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p><br>
                                <p class="small" style="margin-top:-30px">File dimension: 1200 x 360 pixels</p>
                            </div>
                             <label class="upload_image_ar_error"></label>
                        </div> 
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                         {{ __('messages.form.save') }}
                                    </button>
                                    
                                <a href="{{route('offer_management')}}" class="btn btn-default reset_style">  {{ __('messages.form.cancel') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

{{-- End Popup --}}

@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('input[name="expire_in"]').daterangepicker({
    "singleDatePicker": true,
    // "autoUpdateInput": false,
    "autoApply": true,
    'changeMonth': true,
    'changeYear': true,
    locale: {
        format: 'DD-M-YYYY'
       
    }
});
</script>
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});


        $('#offer_add_btn').on('click',function(e){
            $('#offer-form').trigger("reset")
            $('#exampleModalLabel').html('Add Offer');
             $("#offer-form")[0].reset();
            $('#offer-popup').modal({
                show:true
            })
        })

        
         $('.save_page').on('click',function(e){
            $("#offer-form").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },
                    title_ar  : {        
                        required: true,         
                    },
                    menu_item_id  : {        
                        required: true,         
                    },
                    expire_in  : {        
                        required: true,         
                    },
                    discount_value  : {        
                        required: true,
                        number:true,         
                    },
                    // upload_image_en  : {        
                    //     required: true,         
                    // }, 
                    // upload_image_ar  : {        
                    //     required: true,         
                    // },  
                },
                messages: {               
                    title_en: {
                        required: @json(__('messages.offer_management.validation.title_en') ),     
                    },
                    title_ar: {
                        required: @json(__('messages.offer_management.validation.title_ar') ), 
                    },
                    menu_item_id  : {        
                        required: @json(__('messages.offer_management.validation.menu_item_id') ),        
                    },
                    expire_in  : {        
                        required: @json(__('messages.offer_management.validation.expire_in') ),         
                    },
                    discount_value  : {        
                        required: @json(__('messages.offer_management.validation.discount_value') ),
                        number: @json(__('messages.offer_management.validation.discount_value1') ),        
                    },
                    //  upload_image_en  : {        
                    //     required: @json(__('messages.offer_management.validation.img_en') ),         
                    // },
                    //  upload_image_ar  : {        
                    //     required: @json(__('messages.offer_management.validation.img_ar') ),         
                    // },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "menu_item_id" ) {
                        $(".menu_item_id_error").html(error);
                    }
                    if (element.attr("name") == "expire_in" ) {
                        $(".expire_in_error").html(error);
                    }
                    if (element.attr("name") == "discount_value" ) {
                        $(".discount_value_error").html(error);
                    }
                    // if (element.attr("name") == "upload_image_en" ) {
                    //     $(".upload_image_en_error").html(error);
                    // }
                    // if (element.attr("name") == "upload_image_ar" ) {
                    //     $(".upload_image_ar_error").html(error);
                    // }
                    
                },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    let edit_val=$('#id_pg').val();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('offer_management.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#offer-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }      
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('offer_management.store')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#offer-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    }) ;
                    }
                
                }
            });
            
           
        });
         $('.offer_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Offer');

            page = $(this).data('id')
            var url = "offer_management/edit/";
        
            $.get(url  + page, function (data) {
                console.log(data.offer.lang[1].image_path);
                $('#title_en').val(data.offer.lang[0].name);
                $('#title_ar').val(data.offer.lang[1].name);
                $('#menu_item_id').val(data.offer.menu_item_id);
                $('#expire_in').val(formatDate(data.offer.expire_in));
                $('#discount_value').val(data.offer.discount_value);
                $('input[name="discount_type"][value="' + data.offer.discount_type + '"]').prop("checked", true);
                $('#coupon_code').val(data.offer.coupon_code);
                // $('#uploadFile_en').val(data.offer.lang[0].image_path);
                // $('#uploadFile_ar').val(data.offer.lang[1].image_path);
                $('#id_pg').val(data.offer.id)
                $('#previewimage_en').val(data.offer.lang[0].image_path);
                $('#previewimage_ar').val(data.offer.lang[1].image_path);
                var uploadsUrlEn = "<?php echo asset('/uploads/restaurant/offer_management/en') ?>";
                var uploadsUrlAr = "<?php echo asset('/uploads/restaurant/offer_management/ar') ?>";
                var imgurlEn = uploadsUrlEn + '/' + data.offer.lang[0].image_path;
                var imgurlAr = uploadsUrlAr + '/' + data.offer.lang[1].image_path;
                if (data.offer.lang[0].image_path != "") {
                    $('#previewimage_en').attr("src", imgurlEn);
                    $('#hidden_image_en').val(data.offer.lang[0].image_path);
                    $(".delete-img-en").show();
                }
                if (data.offer.lang[1].image_path != " "){
                    $('#previewimage_ar').attr("src", imgurlAr);
                    $('#hidden_image_ar').val(data.offer.lang[1].image_path);
                    $(".delete-img-ar").show();
                }
                $('#offer-popup').modal({
                    show: true

                });
            }) 
        })

       
        $('.change-status').on('click', function(e) {
            var id = $(this).data('id');
            var act_value = $(this).data('status');
        
            $.confirm({
                title: act_value + ' Offer',
                content: 'Are you sure to ' + act_value + ' the offer?',
                buttons: {
                    Yes: function() {
            $.ajax({
                type: "POST",
                url: "{{route('offer_management.status.update')}}",
                data: {
                    id: id,
                    status: act_value
                },

                success: function(data) {
                                if (data.status == 1) {
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("offer_management")}}';
                                    }, 1000);

                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        window.location.reload();
                    }
                }
            });
        });

        function deleteUser(id) {
            $.confirm({
                title: false,
                content: 'Are you sure to delete this offer? <br><br>You wont be able to revert this',
                buttons: {
                    Yes: function() {
                        $.ajax({
                            type: "POST",
                            url: "{{route('offer_management.delete')}}",
                            data: {
                                id: id
                            },
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function(data) {
                                if (data.status == 1) {
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("offer_management")}}';
                                    }, 1000);
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        console.log('cancelled');
                    }
                }
            });
        }

         function removesrc_en(){
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#previewimage_en').val('');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
        $(".delete-img-en").hide();
        $('#delete_image_en').val('1'); 
    };

    document.getElementById("uploadFile_en").onchange = function(e) {
        var focusSet = false;
        var reader = new FileReader();
        var fileUpload = document.getElementById("uploadFile_en");
        if (fileUpload!= '') {
            // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            var filePath = fileUpload.value; 
            var allowedExtensions =  /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
            var file_size = $('#uploadFile_en')[0].files[0].size;
            
                if (!allowedExtensions.exec(filePath)) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg or png </div>");
                    $(".delete-img-en").hide();
                }
                else if(file_size > 1024000) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 1MB in size </div>");
                    $(".delete-img-en").hide();
                }
            else if(fileUpload!= ''){
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

                    var fileUpload = document.getElementById("uploadFile_en");
                    var reader = new FileReader();

                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {

                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;

                            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                            var reader = new FileReader();
                        
                            document.getElementById("previewimage_en").src = e.target.result;
                        
                            reader.readAsDataURL(fileUpload.files[0]);
                            $(".delete-img-en").show();
                        }           
                    }
                }
                else{  
                $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    var reader = new FileReader();
                    reader.onload = function(e) {
                    document.getElementById("previewimage_en").src = e.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                }
                 $(".delete-img-en").show();
                 $('#delete_image_en').val('0');
                
        }else{
            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
        
        }
    };

    function removesrc_ar(){
        $('#previewimage_ar').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#previewimage_ar').val('');
        $('#hidden_image_ar').remove();
        $("#uploadFile_ar").val("");
        $(".delete-img-ar").hide();
        $('#delete_image_ar').val('1'); 
    };

    document.getElementById("uploadFile_ar").onchange = function(e) {
        var focusSet = false;
        var reader = new FileReader();
        var fileUpload = document.getElementById("uploadFile_ar");
        if (fileUpload!= '') {
            // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            var filePath = fileUpload.value; 
            var allowedExtensions =  /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
            var file_size = $('#uploadFile_ar')[0].files[0].size;
            
                if (!allowedExtensions.exec(filePath)) {
                    $("#uploadFile_ar").parent().next(".validation").remove(); // remove it
                    $('#previewimage_ar').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_ar').remove();
                    $("#uploadFile_ar").val("");
                    $("#uploadFile_ar").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg or png </div>");
                    $(".delete-img-ar").hide();
                }
                else if(file_size > 1024000) {
                    $("#uploadFile_ar").parent().next(".validation").remove(); // remove it
                    $('#previewimage_ar').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_ar').remove();
                    $("#uploadFile_ar").val("");
                    $("#uploadFile_ar").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 1MB in size </div>");
                    $(".delete-img-ar").hide();
                }
            else if(fileUpload!= ''){
                    $("#uploadFile_ar").parent().next(".validation").remove(); // remove it

                    var fileUpload = document.getElementById("uploadFile_ar");
                    var reader = new FileReader();

                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {

                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;

                            $("#uploadFile_ar").parent().next(".validation").remove(); // remove it
                            var reader = new FileReader();
                        
                            document.getElementById("previewimage_ar").src = e.target.result;
                        
                            reader.readAsDataURL(fileUpload.files[0]);
                            $(".delete-img-ar").show();
                        }           
                    }
                }
                else{  
                $("#uploadFile_ar").parent().next(".validation").remove(); // remove it
                    var reader = new FileReader();
                    reader.onload = function(e) {
                    document.getElementById("previewimage_ar").src = e.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                }
                 $(".delete-img-ar").show();
                $('#delete_image_ar').val('0');
                
        }else{
            $("#uploadFile_ar").parent().next(".validation").remove(); // remove it
        
        }
    };
    $('.cancel-btn').on('click',function(){
          $('#id_pg').val('');
         $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
         $('#previewimage_ar').attr('src', '{{ asset('assets/images/upload.png') }}');
            $("#offer-form")[0].reset();        
    });
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;

        return [day, month, year].join('-');
    }
    $('.dis-type').click(function() {
   if($('.dis-type').is(':checked')) { 
      $('#discount_value').val(''); 
   }
});
    </script>
@endpush