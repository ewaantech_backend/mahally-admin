@extends('layouts.master')
@section('content-title')
{{ __('messages.menu_category.menu_category') }}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="sort_items">
    <i class="ti-plus"></i> {{ __('messages.menu_category.sort_item') }}
</button>
<button class="btn btn-primary" id="add_new_category">
    <i class="ti-plus"></i> {{ __('messages.menu_category.add_menu_category') }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('messages.menu_category.table_header.sl_no') }}</th>
                                <th>{{ __('messages.menu_category.table_header.menu_cate_en') }}</th>
                                <th>{{ __('messages.menu_category.table_header.menu_cate_ar') }}</th>
                                <th>{{ __('messages.menu_category.table_header.available_in') }}</th>
                                {{-- <th>{{ __('messages.menu_category.table_header.calory') }}</th> --}}
                                <th width="25%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($menus) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($menus as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->lang[0]->name}}</td>
                                <td>{{ $row_data->lang[1]->name}}</td>
                                <td>
                                @php
                                    foreach ($row_data->available_time as $key => $value) {
                                       $timeData = \App\Models\Timing::select('start_time','end_time')->where('id', $value->time_id)->where('restuarants_id', $rest_id)->first();
                                       if(!empty($timeData)){
                                            echo date('h:i A', strtotime($timeData->start_time)) .' to '. date('h:i A', strtotime($timeData->end_time)) .' ,   ' ;
                                       }     
                                    }
                               @endphp
                                </td>
                                 {{-- <td>{{ $row_data->calories}}</td> --}}
                                <td class="text-left">
                                    <button type="button" class="change-status btn btn-sm btn-toggle ml-0 {{$row_data->status}}" data-toggle="button" data-id="{{ $row_data->id }}" data-status="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" aria-pressed="true" autocomplete="off">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn edit_menu_category" title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white" title="Delete Staff" onclick="deleteUser({{ $row_data->id }})"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $menus->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Sort item Popup --}}
<div class="modal fade" id="menu_sort_item_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        {{ __('messages.menu_category.sort_item') }}
                    </strong></h4>
                {{-- <button type="button" class="close cancel-btn sort-cancel" data-dismiss="modal" aria-hidden="true">&times;</button> --}}
                <a href="{{route('menu_categories.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            {{-- <form id="category_form" action="javascript:;" method="POST"> --}}
                 <div class="modal-body">
                     <div class="table-responsive">
                    <table  id="table-sort" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>{{ __('messages.menu_category.table_header.sl_no') }}</th>
                                <th>{{ __('messages.menu_category.table_header.menu_cate_en') }}</th>
                                <th>{{ __('messages.menu_category.table_header.menu_cate_ar') }}</th>
                            </tr>
                        </thead>
                        <tbody id="tablecontents">
                            @if (count($menus) > 0)
                            @php
                            $i = 1;
                            @endphp

                            @foreach ($menus as $row_data)
                            <tr class="row1" data-id="{{ $row_data->id }}">
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td class="pl-3"><i class="fa fa-sort"></i>  {{ $i++ }}</td>
                                <td>{{ $row_data->lang[0]->name}}</td>
                                <td>{{ $row_data->lang[1]->name}}</td>      
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                 </div>
                {{-- <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-table" id="save_data">
                        ADD
                    </button>
                    <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">Cancel</button>
                </div> --}}
            {{-- </form> --}}
        </div>
    </div>
</div>
{{-- Pop Up --}}
<div class="modal fade" id="menu_cate_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="name_change"><strong>
                        Add Menu Category
                    </strong></h4>
                <button type="button" class="close cancel-btn" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <form id="category_form" action="javascript:;" method="POST">
                <div class="modal-body">
                    <div class="msg_div"></div>
                    <div class="row">
                        <input type="hidden" name="unique_id" id='unique_id'>
                        @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>{{ __('messages.menu_category.form.menu_cate_en') }} <span class="text-danger">*</span></label>
                            <input type="text" placeholder="{{ __('messages.menu_category.placeholder.etr_menu_cate_en') }}" class="form-control form-white" id="title_en" name="title_en" value="{{ old('title_en') }}">
                            <label class="title_en_error error-msg"></label>
                        </div> 
                         <div class="col-md-6">
                            <label>{{ __('messages.menu_category.form.menu_cate_ar') }} <span class="text-danger">*</span></label>
                             <input type="text" placeholder="{{ __('messages.menu_category.placeholder.etr_menu_cate_ar') }}" class="form-control form-white text-right" id="title_ar" name="title_ar" value="{{ old('title_ar') }}">
                            <label class="title_ar_error error-msg"></label>
                        </div>
                        <div class="col-md-6" style="display: none;">
                            <label>{{ __('messages.menu_category.form.calories') }} <span class="text-danger">*</span></label>
                             <input type="text" placeholder="{{ __('messages.menu_category.placeholder.calories') }}" class="form-control form-white" id="calories" name="calories" value="calories">
                            <label class="calories_error error-msg"></label>
                        </div>
                         <div class="col-md-6"></div>
                        <div class="col-md-12">
                            <label class="control-label">{{ __('messages.menu_category.form.available_in') }}<span class="text-danger">*</span></label><br/>
                             {{-- @if($timing == '')
                                    <a href="{{route('timing.get')}}"><button type="button" class="btn btn-default">Add New Timing</button></a>
                            @else  
                            <select class="select2_multiple" name="available_time[]" multiple="multiple" id="available_time">
                                @foreach($timing as $row_val)
                                <option  value="{{ $row_val->id }}">{{ $row_val->start_time }} to {{ $row_val->end_time }}</option>
                                @endforeach  
                            </select>
                             @endif   --}}
                            <select class="select2_multiple" name="available_time[]" multiple="multiple" id="available_time">
                                @foreach($timing as $row_val)
                                <option  value="{{ $row_val->id }}">{{ $row_val->start_time }} to {{ $row_val->end_time }}</option>
                                @endforeach  
                            </select>
                            <label class="control-label available_time_error" ></label>
                        </div>
                        {{-- <div class="col-md-6">
                            <label>{{ __('messages.menu_category.form.image') }}</label>
                            <div class ="imgup">
                                <div class="scrn-link">
                                    <button type="button" class="scrn-img-close delete-img" data-id="" onclick="removesrc_en()" data-type="app_image" style="display: none;">
                                        <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                                    </button>
                                    <img id="previewimage_en" class="cover-photo" onclick="$('#uploadFile_en').click();" src="{{ asset('assets/images/upload.png') }}" />
                                </div>
                                <input type="file" id="uploadFile_en" name="upload_image" style="visibility: hidden;" accept="image/*" value="" />
                                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
                            </div>
                            <span class="error"></span>
                            <div class="catimg">
                                <p class="small">Max file size: 1MB</p><br>
                                <p class="small" style="margin-top:-30px">Supported formats: jpeg,png</p><br>
                                <p class="small" style="margin-top:-30px">File dimension: 1200 x 360 pixels</p>
                            </div>
                        </div>  --}}
                        
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info waves-effect waves-light save-table" id="save_data">
                        {{ __('messages.form.add') }}
                    </button>
                    <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">{{ __('messages.form.cancel') }}</button>
                </div>
            </form>
        </div>
    </div>
</div>
{{-- end popup --}}


@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
</style>
@endpush
@push('scripts')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $('#add_new_category').on('click', function() {
        $('#name_change').html('Add Menu Category');
        $('#save_data').text('Add').button("refresh");
        $("#category_form")[0].reset();
        $('#menu_cate_popup').modal({
            show: true
        });
    })
     $('#sort_items').on('click', function() {
        $('#name_change').html('Sort Items');
        $('#save_data').text('Add').button("refresh");
        $('#menu_sort_item_popup').modal({
            show: true
        });
    })
    $(".select2_multiple").select2({
        placeholder: @json(__('messages.menu_category.placeholder.sel_available_in') ),
        allowClear: true
    });
     $(".select2_multiple").on('select2:select',function (e){
        $("#available_time_error").text('');
    });

    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "770"});
  
    $('.save-table').on('click', function(e) {
        $("#category_form").validate({
            rules: {
               title_en  : {        
                        required: true,         
                    },
                title_ar  : {        
                    required: true,         
                },
                calories  : {        
                    required: true,      
                }, 
                "available_time[]":{
                    required: true,
                },  
            },
            messages: {
                 title_en: {
                        required:  @json(__('messages.menu_category.validation.title_en') ),
                        
                    },
                title_ar: {
                    required: @json(__('messages.menu_category.validation.title_ar') ),
                    
                },
                calories  : {        
                    required: @json(__('messages.menu_category.validation.calories') ),    
                },
                "available_time[]":{
                    required: @json(__('messages.menu_category.validation.available_time') ),
                },
                    
            },
            errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "calories" ) {
                        $(".calories_error").html(error);
                    }
                    if (element.attr("id") == "available_time") {
                        error.insertAfter(".available_time_error");
                    } else {
                        error.insertAfter(element);
                    }
                    
                },
            submitHandler: function(form) {
                unique_id = $("#unique_id").val();
                $('.loading_box').show();
                $('.loading_box_overlay').show();
                $('button:submit').attr('disabled', true);
                if (unique_id) {
                    $.ajax({
                        type:"POST",
                        url: "{{route('menu_categories.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#category_form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }      
                    });

                } else {
                    $.ajax({
                        type:"POST",
                        url: "{{route('menu_categories.store')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#category_form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    }) ;

                }
            }
        })
    });

    $('.edit_menu_category').on('click', function(e) {
        page = $(this).data('id')
        $('#name_change').html('Edit Menu Category');
        $('#save_data').text('Save').button("refresh");
        var url = "menu_categories/edit/";
        $.get(url + page, function(data) {
            $('#title_en').val(data.menu.lang[0].name);
            $('#title_ar').val(data.menu.lang[1].name);
            $('#calories').val(data.menu.calories); 
            timing = data.menu.available_time;
            selectedValues = new Array();
            $.each(timing, function( index, value ) {
                selectedValues.push(value.time_id);  
            });
            $("#available_time").select2().val(selectedValues).trigger("change");
            $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "770"});
            $('#unique_id').val(data.menu.id)
            $('#previewimage_en').val(data.menu.image_path)
            var uploadsUrl = "<?php echo asset('/uploads/restaurant/menu_category/resturant_') ?>";
            var imgurl = uploadsUrl +data.menu.restuarants_id +'/' + data.menu.image_path;
            if (data.menu.image_path) {
                $('#previewimage_en').attr("src", imgurl);
                $('#hidden_image_en').val(data.menu.image_path);
                $(".delete-img").show();
            }
            $('#menu_cate_popup').modal({
                show: true

            });
        });
    });
    
    $('.change-status').on('click', function(e) {

        var id = $(this).data('id');
        var act_value = $(this).data('status');
     
        $.confirm({
            title: act_value + ' Menu Category',
            content: 'Are you sure to ' + act_value + ' the menu category?',
            buttons: {
                Yes: function() {
        $.ajax({
            type: "POST",
            url: "{{route('menu_categories.status.update')}}",
            data: {
                id: id,
                status: act_value
            },

            success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("menu_categories.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
   
    
    function deleteUser(id) {
        $.confirm({
            title: false,
            content: 'Are you sure to delete this Menu Category? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('menu_categories.delete')}}",
                        data: {
                            id: id
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("menu_categories.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    }
</script>
<script>
    function removesrc_en(){
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
         $(".delete-img").hide();
    };

    document.getElementById("uploadFile_en").onchange = function(e) {
        var focusSet = false;
        var reader = new FileReader();
        var fileUpload = document.getElementById("uploadFile_en");
        if (fileUpload!= '') {
            // var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(.jpg|.png|.jpeg)$");
            var filePath = fileUpload.value; 
            var allowedExtensions =  /(\.jpg|\.jpeg|\.png|\.gif)$/i; 
            var file_size = $('#uploadFile_en')[0].files[0].size;
            
                if (!allowedExtensions.exec(filePath)) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be of the format jpeg or png </div>");
                    $(".delete-img").hide();
                }
                else if(file_size > 1024000) {
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
                    $('#hidden_image_en').remove();
                    $("#uploadFile_en").val("");
                    $("#uploadFile_en").parent().after("<div class='validation' style='color:red;font-size: 12px;  font-weight: 400;'>The image must be less than 1MB in size </div>");
                    $(".delete-img").hide();
                }
            else if(fileUpload!= ''){
                    $("#uploadFile_en").parent().next(".validation").remove(); // remove it

                    var fileUpload = document.getElementById("uploadFile_en");
                    var reader = new FileReader();

                    //Read the contents of Image File.
                    reader.readAsDataURL(fileUpload.files[0]);
                    reader.onload = function (e) {

                        //Initiate the JavaScript Image object.
                        var image = new Image();

                        //Set the Base64 string return from FileReader as source.
                        image.src = e.target.result;

                        //Validate the File Height and Width.
                        image.onload = function () {
                            var height = this.height;
                            var width = this.width;

                            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                            var reader = new FileReader();
                        
                            document.getElementById("previewimage_en").src = e.target.result;
                        
                            reader.readAsDataURL(fileUpload.files[0]);
                            $(".delete-img").show();
                        }           
                    }
                }
                else{  
                $("#uploadFile_en").parent().next(".validation").remove(); // remove it
                    var reader = new FileReader();
                    reader.onload = function(e) {
                    document.getElementById("previewimage_en").src = e.target.result;
                    };
                    reader.readAsDataURL(this.files[0]);
                }
                 $(".delete-img").show();
                
        }else{
            $("#uploadFile_en").parent().next(".validation").remove(); // remove it
        
        }
    };

    $('.cancel-btn').on('click',function(){
        $("#unique_id").val('');
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $("#category_form")[0].reset(); 
        window.location.href = '{{route("menu_categories.get")}}';       
    });
</script>
<script type="text/javascript">
      $(function () {
        $("#table-sort").DataTable({
            "paging": false,
            "bInfo": false,
            bFilter: false,
            "columns": [
                { "searchable": false },
                null,
                null,
            ],
            columnDefs: [{
            "defaultContent": "-",
            "targets": "_all"
             }],
        });

        $( "#tablecontents" ).sortable({
          items: "tr",
          cursor: 'move',
          opacity: 0.6,
          update: function() {
              sendOrderToServer();
          }
        });

        function sendOrderToServer() {
          var order = [];
          var token = $('meta[name="csrf-token"]').attr('content');
          $('tr.row1').each(function(index,element) {
            order.push({
              id: $(this).attr('data-id'),
              position: index+1
            });
          });

          $.ajax({
            type: "POST", 
            dataType: "json", 
            url: '{{route("menu_categories.sortable")}}',
                data: {
              order: order,
              _token: token
            },
            success: function(response) {
                if (response.status == "success") {
                  console.log(response);
                } else {
                  console.log(response);
                }
            }
          });
        }
      });
      $('.cancel-btn').on('click',function(){
        $("#table_unique_id").val('');
        window.location.reload();
    });
    </script>
@endpush