<html>
   <head>
      <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="format-detection" content="date=no" />
      <meta name="format-detection" content="address=no" />
      <meta name="format-detection" content="telephone=no" />
      <meta name="x-apple-disable-message-reformatting" />
      <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,700,700i" rel="stylesheet" />
      <title>Order</title>
   </head>
   <body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important;  -webkit-text-size-adjust:none; text-align: center;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#fff" align="center" style=" background-image:url({{asset('images/Bg.png')}}); background-repeat: no-repeat;">
         <tr>
            <td align="center" valign="top">
               <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">
                  <tr>
                     <td class="td container" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; margin:0; font-weight:normal; padding:55px 0px;">
                        <repeater>
                           <!-- Intro -->
                           <layout label='Intro'>
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                 <tr>
                                    <td style="padding-bottom: 10px;">
                                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                          <tr>
                                             <td class="tbrr p30-15" style="padding: 40px 30px; border-radius:26px 26px 0px 0px;" >
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                   <tr>
                                                      <td class="img m-center" style="font-size:0pt; line-height:30pt;padding-bottom:30px; text-align:center;"><img src="{{asset('images/logo.png')}}" width="129" height="203" editable="true" border="0" alt=""  style="    border-radius: 0px;"></td>
                                                   </tr>
                                                   <tr>
                                                      <td class="h1 pb25" style="color:#000; font-family:'Muli', Arial,sans-serif; font-size:30px; line-height:30px; text-align:center; padding-bottom:5px;">
                                                         <multiline>
                                                         Thanks for choosing Mahally, {{$name}}!
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td class="h1 pb25" style="color:#000000; font-family:'Muli', Arial,sans-serif; font-size:20px; line-height:30px; text-align:center; padding-bottom:5px;font-weight: bold;">
                                                         <multiline>
                                                        
Here are your order details
                                                      </td>
                                                   </tr>
                                      
                                                </table>
                                             </td>
                                          </tr>
                                       </table>
									   <table width="780" cellpadding="0" align="center" cellspacing="0" style="    ;">
                                          <tbody>
                                             <tr>
                                                <td 
                                                   style="border-top:0;border-right:0; border-bottom:0px solid #dee1e2; border-left:0;">
                                                </td>
                                             </tr>
                                          </tbody>
                                       </table>
									    <table  width="780" border="0"  cellpadding="0" align="center" cellspacing="0" style="    padding-bottom: 30px;  padding-top: 30px; font-family:'Muli', Arial,sans-serif;   padding-left: 0px;     padding-right: 0px; ">
                                          <tbody>
                                             <tr>
											<td style="background-color: #fff; border-radius:20px 20px 0px 0px; padding: 30px; color: #000; font-size: 25px;font-weight: bold;">
										<table width="100%">
											<tr>
                                                @php
                                                    if($order_status == 1){
                                                        $status = 'Order Receaved' ;
                                                    }else if($order_status == 2 )
                                                    {
                                                        $status = 'Order Accepted' ;
                                                    }else{
                                                        $status = 'Order Delivered' ;
                                                    }
                                                @endphp
                                                    
                                                
												<td style="color: #000; font-size: 25px;font-weight: bold;"  valign="middle"> {{$status}} <img src="{{asset('images/tick.png')}}" width="40" height="39" alt=""/></td>
												<td style="text-align: right">
                                                    @if($order->restaurant->logo)
                                                        <img src="{{asset('/uploads/'. $order->restaurant->logo)}}" width="90" height="92" alt=""/>
                                                    @else
                                                        <img src="{{ asset('images/burger.png') }}" width="60" height="60">
                                                        @endif
                                                </td>
											</tr>
											<tr>
											<td style="color: #383a50; font-size: 20px;font-weight: bold;"  valign="middle">
												# {{$order->order_id}}
											</td>
											<td style="color: #010101; font-size: 20px;font-weight: normal; text-align: right"  valign="middle">
												@php
                                                    $rest_name = $order->restaurant->lang[0]->name ?? '';
                                                @endphp
                                                {{ $rest_name }}
											</td>
											</tr>
										<tr>
										  <td style="color: #383a50; font-size: 20px;font-weight: bold;"  valign="middle">
                                            {{\Carbon\Carbon::parse($order->created_at)->format('d F Y')}}
											</td>
											<td style="color: #5d5d5d; font-size: 20px;font-weight: normal; text-align: right"  valign="middle">
											<img src="{{asset('images/locicon.png')}}" width="20" height="27" alt=""/>{{ $order->restaurant->location}}</td>
										</tr>
										<tr>
											<td colspan="2">
												<hr>
											</td>
										</tr>
										</table>
											
											</td>
										  
											  </tr>
										  <tr>
										  <td style="padding: 30px; background-color:#fff; border-radius:0px 0px 20px 20px;">
										  
										  <table border="0" width="100%">
										  	<tr>
										  		<td colspan="4" style="font-size: 18px; padding: 10px; font-weight: bold; color: #000;">Order Summary</td>
										  		
										  	</tr>
										  	<tr>
										  		<td colspan="2" style="font-size: 18px; padding: 10px; color: #888888;">ITEM </td>
										  		<td style="font-size: 18px; padding: 10px; color: #888888; text-align: center">QTY</td>
                                                  <td style="font-size: 18px; padding: 10px; color: #888888; text-align: center">ITEM PRICE</td>
										  		<td style="font-size: 18px; padding: 10px; color: #888888; text-align: center">COST</td>
										  	</tr>
                                              @foreach ($order->order_item as $item)
                                              <tr>
                                                
                                                  {{-- images/items.png --}}
                                                <td width="85px"  style="padding: 15px 0px;">
                                                    @if($item->menu_items->cover_photo)
                                                <img src="{{ asset('/uploads/'.$item->menu_items->cover_photo) }}" style="border-radius: 10px;" width="70" height="72" alt=""/>
                                                @else
                                                <img src="{{ asset('images/items.png') }}" style="border-radius: 10px;" width="70" height="72" alt=""/>
                                                @endif 
                                                    </td>
                                                @php
                                                    $item_name = $item->menu_items->lang[0]->name ?? '';
                                                @endphp
                                                <td style="font-size: 18px; padding: 10px; color: #000;">{{$item_name}}</td>
                                                <td style="font-size: 18px; padding: 10px; color: #000; text-align: center">{{$item->item_count}}</td>
                                                <td style="font-size: 18px; padding: 10px; color: #000; text-align: center">{{$item->item_price}}</td>
                                                <td style="font-size: 18px; padding: 10px; color: #000; text-align: center">{{$item->grant_total}}</td>
                                              </tr>
                                            
                                                @if($item->addon)
                                                @foreach ($item->addon as $addon)
                                                <tr>
                                                    <td></td>
                                                    <td width="85px"  style="padding: 15px 0px;">
                                                        @if(is_file( public_path() . '/uploads/' .$addon->resturant_addons->image_path ))
                                                    <img src="{{ url('/uploads/'.$addon->resturant_addons->image_path) }}" style="border-radius: 10px;" width="70" height="72" alt=""/>
                                                    @else
                                                    <img src="{{ asset('images/items.png') }}" style="border-radius: 10px;" width="70" height="72" alt=""/>
                                                    @endif 
                                                        </td>
                                                    @php
                                                        $addon_name = $addon->resturant_addons->lang[0]->name ?? '';
                                                    @endphp
                                                    <td style="font-size: 18px; padding: 10px; color: #000;">{{$addon_name}}<br>
                                                        <span style="color:#6a6a6a;font-size: 20px;  font-weight: bold;">X {{$addon->item_count}}</span>
                                                    </td>
                                                    <td style="font-size: 18px; padding: 10px; color: #000; text-align: center">{{$addon->item_price}}</td>
                                                    <td style="font-size: 18px; padding: 10px; color: #000; text-align: center">{{$addon->grant_total}}</td>
                                                </tr>
                                                @endforeach
                                                   
                                                @endif
                                              @endforeach
										  		
			
										  	<tr>
										  		<td colspan="4" style="border-bottom: dashed 1px #5d5d5d;">&nbsp;
										  			
										  		</td>
										  	</tr>
										  	<tr>
										  		<td colspan="4">
										  			 <table border="0" width="100%">
										  <tr>
										  <td style="width: 120px;"></td>
										  <td width="33%">&nbsp;</td>
										  	<td  style="font-size: 18px; font-weight: bold;">Sub Total </td>
										  	<td style="font-size: 18px; font-weight: bold; text-align: right">
										  		SAR {{$order->sub_total}}
										  	</td>
										  </tr>
										  
										    <tr>
										  <td style="width: 120px;"></td>
										  <td></td>
                                @if($order->promo_code)
                                    {{-- @php
                                        if($order->promo_type)

                                    @endphp --}}
										  	<td  style="font-size: 18px; font-weight: bold;">Discount </td>
										  	<td style="font-size: 18px; font-weight: bold; text-align: right">
                                    
                                   
										  		SAR 0
										  	</td>
                                   @endif
										  </tr>
										  
										    <tr>
										  <td style="width: 120px;"></td>
										  <td></td>
										  	<td  style="font-size: 20px; font-weight: bold; color:#5a2364 ">Total </td>
										  	<td style="font-size: 20px; font-weight: bold; text-align: right; color:#5a2364 ">
										  		SAR {{$order->grand_total}}
										  	</td>
										  </tr>
										  	
										  </table>
										  		</td>
										  	</tr>
										 
										  </table>
										  
												 </td>
                                             </tr>
                                          </tbody>
                                       </table>
                                       
                                    
                                         
                                             <table width="100%" cellpadding="10" cellspacing="0" border="0"style="background-color:#fff;color: #404041; font-family:'Muli', Arial,sans-serif;  padding-bottom:10px; border-radius: 20px">
                                          <tr>
                                             <td class="dark-blue-button2 text-button" style=" color:#939393; font-family:'Muli', Arial,sans-serif; font-size:14px; line-height:18px; padding:35px 30px; text-align:center;  font-weight:bold;">
                                                <multiline><a href="#" target="_blank" class="link" style="color:#939393; text-decoration:none;"><span class="link" style="color:#939393; text-decoration:none;">Call us at <b>1-800-672-4399</b> or reply to this email</span></a></multiline>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td>
                                                <table width="95%" cellspacing="10" border="0" style="font-size: 13px;color: #404041; " align="center">
                                                   <tr>
                                                      <td width="25%" style="text-align:center;vertical-align:top">
                                                         <div style="height:42px; padding-bottom:20px;">
                                                            <img src="{{asset('images/24-hours.png')}}" width="60px">
                                                         </div>
                                                      </td>
                                                   
                                                      <td width="25%" style="text-align:center;vertical-align:top">
                                                         <div style="height:42px; padding-bottom:20px;">
                                                            <img src="{{asset('images/img_458847.png')}}"  width="60px" >
                                                         </div>
                                                      </td>
                                                      <td width="25%" style="text-align:center;vertical-align:top">
                                                         <div style="height:42px; padding-bottom:20px;">
                                                            <img src="{{asset('images/easy-return.png')}}" width="65px" >
                                                         </div>
                                                      </td>
                                                   </tr>
                                                   <tr>
                                                      <td width="25%" style="text-align:center;vertical-align:top">
                                                         <div style="line-height:22px;text-align:center ;   
                                                            font-weight: 700;">
                                                           Customer Service
                                                         </div>
                                                      </td>
                                                    
                                                      <td width="25%" style="text-align:center;vertical-align:top">
                                                         <div style="line-height:22px;text-align:center;    
                                                            font-weight: 700;">
                                                            Satisfaction guaranteed
                                                         </div>
                                                      </td>
                                                      <td width="25%" style="text-align:center;vertical-align:top">
                                                         <div style="line-height:22px;text-align:center;    
                                                            font-weight: 700;">
                                                            Quality Service
                                                         </div>
                                                      </td>
                                                   </tr>
                                                     <tr>
                                          	<td colspan="3" style="border-top: solid 1px #5d5d5d;border-bottom: solid 1px #5d5d5d; padding: 25px; text-align: center;">
                                          		<a href="www.mahallyapp.com" style="color: #5e2e66; font-size: 20px; text-decoration: none">www.mahallyapp.com</a>
                                          	</td>
                                          </tr>
                                                <tr >
                                       <td colspan="3" class="text-footer1 pb10" style="color:#9b9b9b; font-family:'Muli', Arial,sans-serif; font-size:16px; line-height:20px; text-align:center; padding-bottom:10px;">
                                          <multiline>Copyright ©2021 All rights reserved</multiline>
                                       </td>
                                    </tr>
                                                </table>
                                             </td>
                                             
                                          </tr>
                                        
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </layout>
                           <!-- END Two Columns -->
                        </repeater>
                        <!-- Footer -->
            
                        <!-- END Footer -->
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
         	<td align="center"><img src="{{asset('images/footer.png')}}" width="500" height="162" alt=""/></td>
         </tr>
      </table>
   </body>
</html>