<div class="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="float-left">
                    <div class="hamburger sidebar-toggle">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="float-right">
                    <ul>
                        @if(Auth::guard('restaurant')->check())
                        @php
                        $lang1= app()->getLocale() == 'en' ? 'ar' : 'en';
                        $display = $lang1 == 'en' ? 'English' : 'العربية';
                        @endphp
                        {{-- <li class="header-icon dib"><a href="{{ url('setlocale', $lang1) }}" class="user-avatar">{{ $display }}</a></li> --}}
                        @endif
                        <li class="header-icon dib"><span class="user-avatar">
                            @if(Auth::guard('restaurant')->check()){{ __('messages.admin.my_account') }} @else 
                            My Account
                            @endif
                            <i class="ti-angle-down f-s-10"></i></span>
                            <div class="drop-down dropdown-profile">
                                <div class="dropdown-content-body">
                                    <ul>
                                        @if(Auth::guard('restaurant')->check())
                                        <li><a href="{{route('resturant.password.new')}}"><i class="ti-user"></i> <span>{{ __('messages.admin.change_password') }}</span></a></li>
                                        <li><a href="{{route('restaurant.logout')}}"><i class="ti-power-off"></i> <span>{{ __('messages.admin.logout') }}</span></a></li>
                                        @else
                                        <li><a href="{{route('user.edit')}}"><i class="ti-user"></i> <span>Edit Profile</span></a></li>
                                        <li><a href="{{route('user.logout')}}"><i class="ti-power-off"></i> <span>Logout</span></a></li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>