@extends('layouts.master')

@section('content-title')
PROMO CODES
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="promo_add_btn">
    <i class="ti-plus"></i> Add New Promo Code
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('promocode.get')}}" id="search-promocode-form" method="get">
                            <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search by promo code">
                            <input type="hidden" name="promo_select" id="promo_select">
                        </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-promocode-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('promocode.get')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Promo Code</th>
                                <th>Discount Type</th>
                                <th>Value</th>
                                <th>Expires In</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($promo) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($promo as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->promocode ?? '' }}</td>
                                <td>{{ $row_data->type ?? '' }}</td>
                                <td>{{ $row_data->amount ?? '' }}</td>
                                <td>{{ \Carbon\Carbon::parse($row_data->expire_in)->format('d-m-Y')}}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit promo_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white promo_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $promo->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="promo-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add FAQ</h4>
                <a href="{{route('promocode.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="promo-form" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    <div class="row mb-3">
                         <div class="col-md-6">
                            <label class="control-label">Promo Code <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter promocode" type="text" id="promocode" name="promocode" />
                            <label class="promocode_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Discount Type <span class="text-danger">*</span></label>
                            <select class="form-control" name="type" id="type">
                                @foreach($discount_type as $item)
                                <option value="{{$item}}">{{$item}}</option>
                                @endforeach
                            </select>
                            <label class="type_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Value (SAR) <span class="text-danger">*</span></label>
                            <input class="form-control form-white" placeholder="Enter amount" type="text" id="amount" name="amount" />
                            <label class="amount_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Expires In <span class="text-danger">*</span></label>
                            <div class='input-group date' id='datepicker'>
                                <input class="form-control" type="text" name="expire_in" id="expire_in" placeholder="Select date" autocomplete="off">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                            <label class="expire_in_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Maximum number of usage </label>
                            <input class="form-control form-white" placeholder="Unlimited usage" type="text" id="max_usage" name="max_usage" />
                            <label class="usage_error"></label>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Maximum number of usage per user </label>
                            <input class="form-control form-white" placeholder="Unlimited usage" type="text" id="max_usage_per_user" name="max_usage_per_user" />
                            <label class="usage_per_user_error"></label>
                        </div>
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                   
                                    <a href="{{route('promocode.get')}}" class="btn btn-default reset_style">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
    $('input[name="expire_in"]').daterangepicker({
    "singleDatePicker": true,
    // "autoUpdateInput": false,
    "autoApply": true,
    'changeMonth': true,
    'changeYear': true,
    locale: {
        format: 'DD-MM-YYYY'
       
    }
});
</script>
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        

        $('#promo_add_btn').on('click',function(e){
            $('#promo-form').trigger("reset"); 
            $('#exampleModalLabel').html('Add Promo Code');
            $('#promo-popup').modal({
                show:true
            })
        })

        $('.promo_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Promo Code');

            page = $(this).data('id')
            var url = "promocode/edit/";
        
            $.get(url  + page, function (data) {
                $('#promocode').val(data.page.promocode);
                $('#type').val(data.page.type);
                $('#amount').val(data.page.amount);
                $('#expire_in').val(formatDate(data.page.expire_in));
                $('#max_usage').val(data.page.max_usage);
                $('#max_usage_per_user').val(data.page.max_usage_per_user);
                $('#id_pg').val(data.page.id)
                $('#promo-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
            $("#promo-form").validate({
                ignore: [],
                rules: {
                    promocode  : {        
                        required: true,         
                    },
                    type: {          
                        required: true,
                    },
                    amount: {          
                        required: true,
                        number: true,
                    },
                    expire_in: {          
                        required: true,
                    },
                    max_usage: {          
                        number: true,
                    },
                    max_usage_per_user: {          
                        number: true,
                    },
                },
                messages: {               
                    promocode: {
                        required:"Promocode required",
                    },
                    type: {
                        required: "Discount type required",
                    },
                    amount: {
                        required: "Amount required",
                        number: "Accept only numeric value",
                    },
                    expire_in: {
                        required: "Date required",
                    },
                    max_usage: {
                        number: "Accept only numeric value",
                    },
                    max_usage_per_user: {
                        number: "Accept only numeric value",
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "expire_in" ) {
                        $(".expire_in_error").html(error);
                    }
                    if (element.attr("name") == "promocode" ) {
                        $(".promocode_error").html(error);
                    }
                    if (element.attr("name") == "type" ) {
                        $(".type_error").html(error);
                    }
                    if (element.attr("name") == "amount" ) {
                        $(".amount_error").html(error);
                    }
                    if (element.attr("name") == "max_usage" ) {
                        $(".usage_error").html(error);
                    }
                    if (element.attr("name") == "max_usage_per_user" ) {
                        $(".usage_per_user_error").html(error);
                    }
                         
                 },
                 submitHandler: function(form) {
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                        $.ajax({
                            type:"POST",
                            url: "{{route('promocode.update')}}",
                            data:{ 
                                promocode:$('#promocode').val(),
                                type:$('#type').val(),
                                amount:$('#amount').val(),
                                expire_in:$('#expire_in').val(),
                                max_usage:$('#max_usage').val(),
                                max_usage_per_user:$('#max_usage_per_user').val(),
                                id:edit_val
                    
                            },
                            success: function (data) {
                                if (data.status == 1) {
                                    Toast.fire({
                                    icon: 'success',
                                            title: data.message
                                    });
                                    window.setTimeout(function () {
                                    window.location.href = '{{route("promocode.get")}}';
                                    }, 1000);
                                    $("#promo-form")[0].reset();
                                } else {
                                    Toast.fire({
                                    icon: 'error',
                                            title: data.message
                                    });
                                }
                                $('button:submit').attr('disabled', false);
                            },
                            error: function (err) {
                                $('button:submit').attr('disabled', false);
                            }     
                        });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('promocode.store')}}",
                        data:{ 
                            promocode:$('#promocode').val(),
                            type:$('#type').val(),
                            amount:$('#amount').val(),
                            expire_in:$('#expire_in').val(),
                            max_usage:$('#max_usage').val(),
                            max_usage_per_user:$('#max_usage_per_user').val(),
                        },
                        success: function (data) {
                                if (data.status == 1) {
                                    Toast.fire({
                                    icon: 'success',
                                            title: data.message
                                    });
                                    window.setTimeout(function () {
                                    window.location.href = '{{route("promocode.get")}}';
                                    }, 1000);
                                    $("#promo-form")[0].reset();
                                } else {
                                    Toast.fire({
                                    icon: 'error',
                                            title: data.message
                                    });
                                }
                                $('button:submit').attr('disabled', false);
                            },
                            error: function (err) {
                                $('button:submit').attr('disabled', false);
                            }
                        }) ;
                    } 
                }
            });    
        });

        $('.change-status').on('click',function(){    
            var id = $(this).data("id");
            var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Promo code',
            content: 'Are you sure to ' + act_value + ' the Promo code?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type:"POST",
                        url: "{{route('promocode.status.update')}}",
                        data:{ 
                           status:act_value,
                           id: id
                   
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("promocode.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
        $('.promo_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Promo code? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
           
                    $.ajax({
                        url: "{{route('promocode.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("promocode.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('promocode.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#promo_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

    $('.cancel-btn').on('click',function(){
          $('#id_pg').val('');   
    });
    function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join('-');
}

    </script>
@endpush