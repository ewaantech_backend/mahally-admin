@extends('layouts.master')
@section('content-title')
ADD RESTAURANTS UNDER "{{ $user->name}}"
@endsection
@section('add-btn')
<a href="{{route('user-list.get')}}" class="btn btn-default">
    « Back to list
</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="link_form" action="javascript:;" method="POST">
                    <div class="modal-body">
                        <div class="msg_div"></div>
                        <div class="row">
                            <input type="hidden" name="user_unique" id='user_unique' value={{ $user->id }}>
                            <div class="col-md-12">
                                <select class="select2_multiple" name="restaurants[]" multiple="multiple" id="restaurants">
                                    @foreach($restaurants as $row_val)
                                    <option  value="{{ $row_val->id }}" {{ !empty($sel_rest) && in_array($row_val->id, $sel_rest) ? 'selected' : '' }}>{{$row_val->lang[0]->name}}</option>
                                    @endforeach
                                </select>
                                <label class="control-label" id="restaurants_error"></label>
                            </div>
                            
                           
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect waves-light save-restaurant" id="save_data">
                            ADD
                        </button>
                        <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">Cancel</button>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>

{{-- end popup --}}
@endsection
@push('css')
<style>
    .reset_style {
        margin-left: 15px;
    }
     .select2-container{
        width: 100% !important;
    }
</style>
@endpush
@push('scripts')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(".select2_multiple").select2({
        placeholder: "Select Multiple Options",
        allowClear: true
    });
     $(".select2_multiple").on('select2:select',function (e){
        $(".restaurants_error").text('');
    });

     $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "100%"});
    
    $('.save-restaurant').on('click', function(e) {  
        $("#link_form").validate({
            rules: {
                "restaurants[]":{
                    required: true,
                },
            },
            messages: {
               "restaurants[]":{
                    required: "Restaurant  required.",
                },
                
            },
            submitHandler: function(form) {
                $('.loading_box').show();
                $('.loading_box_overlay').show();
                $('button:submit').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: "{{route('user-list.add_resturants')}}",
                    data: {
                        user_id: $('#user_unique').val(),
                        restaurants : $('#restaurants').val(),
                    },
                    success: function(data) {
                        if (data.status == 1) {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            window.setTimeout(function() {
                                     window.location.href = "{{route('user-list.get')}}";
                            }, 1000);
                           
                            $("#link_form")[0].reset();
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: data.message
                            });
                        }
                        $('.loading_box').hide();
                        $('.loading_box_overlay').hide();
                        $('button:submit').attr('disabled', false);
                    }
                });
            }
        })
    });

    

     
</script>
@endpush