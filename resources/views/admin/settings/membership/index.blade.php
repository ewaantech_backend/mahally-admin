@extends('layouts.master')

@section('content-title')
MEMBERSHIPS
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="member_add_btn">
    <i class="ti-plus"></i> Add New Membership
</button>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Memberships (EN)</th>
                                <th>Memberships (AR)</th>
                                <th>Price Monthly</th>
                                <th>Price Yearly</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($membership) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($membership as $row_data)
                            <tr>
                                <td>{{ $row_data->lang[0]->membership_name }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->membership_name }}</td>
                                <td>{{ $row_data->price_monthly}}</td>
                                <td>{{ $row_data->price_yearly}}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit membership_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-sm btn-danger text-white member_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $membership->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="membership-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Add Commission Category</h4>
                <a href="{{route('membership.get')}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="add-membership" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    {{-- @method('PUT') --}}
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Memberships (EN) <span class="text-danger">*</span></label>
                            <input type="text" placeholder="Enter Membership" class="form-control form-white" id="title_en"
                                                    name="title_en" value="{{ old('title_en') }}">
                            <label class="title_en_error"></label>                        
                        </div>
                        <div class="col-md-6">
                            <label>Memberships (AR) <span class="text-danger">*</span></label>
                            <input type="text" placeholder="أدخل سؤالك" class="form-control form-white text-right" id="title_ar"
                                                    name="title_ar" value="{{ old('title_ar') }}">
                            <label class="title_ar_error"></label>                        
                        </div>
                        <div class="col-md-6">
                            <label>Price Monthly <span class="text-danger">*</span></label>
                            <input type="text" placeholder="Enter monthly price" class="form-control form-white" id="price_monthly"
                                                    name="price_monthly" value="{{ old('price_monthly') }}">
                            <label class="price_monthly_error"></label>                        
                        </div>
                        <div class="col-md-6">
                            <label>Price Yearly <span class="text-danger">*</span></label>
                            <input type="text" placeholder="Enter yearly price" class="form-control form-white" id="price_yearly"
                                                    name="price_yearly" value="{{ old('price_yearly') }}">
                            <label class="price_yearly_error"></label>                        
                        </div>
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    <a href="{{route('membership.get')}}" class="btn btn-default reset_style">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>


<script>
   

        $('#member_add_btn').on('click',function(e){
            $('#add-membership').trigger("reset")
            $('#exampleModalLabel').html('Add Membership');
            $('#membership-popup').modal({
                show:true
            })
        })

        $('.membership_edit').on('click',function(e){
            e.preventDefault();
            $('#exampleModalLabel').html('Edit Membership');

            member = $(this).data('id')
            var url = "membership/edit/";
        
            $.get(url  + member, function (data) {
                $('#title_en').val(data.member.lang[0].membership_name);
                $('#title_ar').val(data.member.lang[1].membership_name);
                $('#price_monthly').val(data.member.price_monthly	);
                $('#price_yearly').val(data.member.price_yearly);
                $('#id_pg').val(data.member.id)
                
                $('#membership-popup').modal({
                    show: true

                });
            }) 
        })

        $('.save_page').on('click',function(e){
            console.log("inside")
            $("#add-membership").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },
                    title_ar: {          
                        required: true,
                    },
                    price_monthly  : {        
                        required: true,
                        number: true,         
                    },
                    price_yearly: {          
                        required: true,
                        number: true, 
                    },
                },
                messages: {               
                    title_en: {
                        required:"Membership (EN) required",
                        
                        },
                    title_ar: {
                        required: "Membership (AR) required",
                        },
                    price_monthly: {
                        required:"Monthly price required",
                        number:"Accept only numberic value",
                        },
                    price_yearly: {
                        required: "Yearly price required",
                        number:"Accept only numberic value",
                        },    
                 },
                 errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    if (element.attr("name") == "price_monthly" ) {
                        $(".price_monthly_error").html(error);
                    }
                    if (element.attr("name") == "price_yearly" ) {
                        $(".price_yearly_error").html(error);
                    }   
                 },
                 submitHandler: function(form) {
                    let edit_val=$('#id_pg').val();
                    var test = new Array();
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();  
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('membership.update')}}",
                        data:{ 
                            "_token": "{{ csrf_token() }}",
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            price_monthly:$('#price_monthly').val(),
                            price_yearly:$('#price_yearly').val(),
                            id:edit_val
                        },
                        success: function(result){
                            console.log(result.msg)
                            $('#membership-popup').modal('hide')
                                Toast.fire({
                                icon: 'success',
                                title: 'Membership updated successfully'
                                });
                            window.location.reload();
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();
                         }       
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('membership.store')}}",
                        data:{
                            "_token": "{{ csrf_token() }}",
                            title_en:$('#title_en').val(),
                            title_ar:$('#title_ar').val(),
                            price_monthly:$('#price_monthly').val(),
                            price_yearly:$('#price_yearly').val(),
                        },
    
                        success: function(result){
                            console.log(result.msg)
                            $('#membership-popup').modal('hide')
                            if(result.msg ==="success")
                            {
                                Toast.fire({
                                icon: 'success',
                                title: 'Membership added successfully'
                                });
                                window.location.reload();
                            }else{
                                Toast.fire({
                                icon: 'error',
                                title: 'some errors'
                                });
                            }
                            $('.loading_box').hide();
                            $('.loading_box_overlay').hide();      
                        }
                    }) ;
                    }
                
                }
            });
            
           
        });

        $('.change-status').on('click',function(){    
            var id = $(this).data("id");
            var act_value = $(this).data("activate");
            $.confirm({
                title: act_value + ' Membership',
                content: 'Are you sure to ' + act_value + ' the Membership?',
                buttons: {
                    Yes: function() {
                        $.ajax({
                            type:"POST",
                            url: "{{route('membership.status.update')}}",
                            data:{ 
                            "_token": "{{ csrf_token() }}",
                            status:act_value,
                            id: id
                    
                            },
                            success: function(data) {
                                if (data.status == 1) {
                                    Toast.fire({
                                        icon: 'success',
                                        title: data.message
                                    });
                                    window.setTimeout(function() {
                                        window.location.href = '{{route("membership.get")}}';
                                    }, 1000);

                                } else {
                                    Toast.fire({
                                        icon: 'error',
                                        title: data.message
                                    });
                                }
                            }
                        });
                    },
                    No: function() {
                        window.location.reload();
                    }
                }
            });
        });

       
        $('.member_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this membership? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
           
                    $.ajax({
                        url: "{{route('membership.delete')}}" ,
                        type: 'POST',
                        data:{
                            "_token": "{{ csrf_token() }}",
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("membership.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });
    $('.cancel-btn').on('click',function(){
          $("#id_pg").val('');
    });
    </script>
@endpush