@extends('layouts.master')

@section('content-title')
Settlement history for restaurant "{{ucfirst($restaurant->lang[0]->name)}}"
@endsection
@section('add-btn')
<a href="{{ route('restaurants.payment_settlement', $restaurant->id) }}" class="btn btn-default">
    &laquo; Back to payment settlement page
</a>
@endsection
@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Settlement Date</th>
                            <th>Settlement Amount</th>
                            <th>Settlement Type</th>
                            <th>File</th>
                            <th class="cls_last_child"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($payment_settlement)> 0)
                    @php
                    $i = 1;
                    @endphp
                    @foreach($payment_settlement as $row_data)
                    <tr>
                        <th>{{ $i++ }}</th>
                        <td>{{date('j F Y', strtotime($row_data->settlement_date))}}</td>
                        <td>{{$row_data->settlement_amount}}</td>
                        <td>{{$transaction_type[$row_data->type] ?? ''}}</td>
                        <td><a href="{{ route('settlement.excel-download', ['id' => $row_data->id])}}" class="btn btn-sm btn-link"><i class="fa fa-download"></i> Click here to download</a></td>
                        <td class="text-center"><a class="btn btn-sm btn-success text-white view_btn"  title="View" data-id="{{ $row_data->id }}"><i class="fa fa-eye"></i></a></td>
                    </tr>
                    @endforeach
                    @else
                    <tr><td colspan="8" class="text-center">No records found!</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center d-flex justify-content-center mt-3">
                {{ $payment_settlement->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>  
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
            <div class="modal-content">

            </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('scripts')
<script>
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
       
        $('.view_btn').on('click', function() {
            var id = $(this).data("id");
            $.ajax({
                type: "GET",
                url: "{{route('settlement.history_details')}}",
                data: {'id': id},
                success: function(data) {
                   $('.modal-content').html(data);
                   $('#formModal').modal('show');
                }
            });
        });
</script>
@endpush
