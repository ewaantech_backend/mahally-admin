@extends('layouts.master')

@section('content-title')
Payment settlement for restaurant "{{ucfirst($restaurant->lang[0]->name)}}"
@endsection
@section('add-btn')
<a href="{{route('settlement.history', $restaurant->id)}}" class="btn btn-default">
    Settlement History
</a>
<a href="{{route('restaurants.get')}}" class="btn btn-default">
    &laquo; Back to list
</a>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="frm_settlement_filter">
                    <div class="row">
                        <div class="col-md-4 ml-0">
                            <select class="form-control" name="restaurant_details">
                                <option value="1">Payment Settlement</option>
                            </select>
                        </div>
                        <div class="col-md-4 ml-0">
                            <div class='input-group' id='datepicker'>
                                <input type="text" class="form-control" name="dates" placeholder="By Date" id="dates">
                                <span class="input-group-addon">
                                    <span class="fa fa-calendar icon-style"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-4 text-left">
                            <button type="button" class="btn btn-info save-btn" id="settle_filter">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('restaurants.payment_settlement', $restaurant->id) }}" class="btn btn-default">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row" id="settlement_result">
    @include('admin.payment_settlement.details')
</div>
<!-- Modal pooup -->
<div class="modal fade none-border" id="formModal">
    <div class="modal-dialog modal-lg">
        <form id="frm_create_settlement" action="javascript:;" method="POST">
            <div class="modal-content">

            </div>
        </form>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
    .cancel_style{
        margin-left: 20px;
    }
    #site-error{
        float:right;
        margin-right: 260px;
        margin-top: 10px;

    }
    .area{
        height: 80px;
    }
    .modal {

    }

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}});
$('input[name="dates"]').daterangepicker({
        "autoUpdateInput": false,
        startDate: "-2m",
        endDate: moment().endOf('month'), 
        maxDate: new Date()
});
$('input[name="dates"]').val();
 $('input[name="dates"]').on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
});
$("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});
$('#settle_now').click(function(){
    var c_percent = $('#commission_percentage').val();
    var restaurant_id = $('#restaurant_id').val();
    var arr = $('.check_value:checked').map(function(){
        return this.value;
    }).get();
    $.ajax({
        type: "GET",
        url: "{{route('restaurants.payment')}}",
        data: {
            order_ids: arr,
            c_percent: c_percent,
            restaurant_id: restaurant_id,
        },
        success: function (data) {
           $('.modal-content').html(data);
           $('#formModal').modal('show');
        }
    });
});
 $("#frm_create_settlement").validate({
    normalizer: function(value) {
    return $.trim(value);
    },
    rules: {
    cust_name: {
    required: true,
    },
        settlement_date: {
        required: true,
        validDate:true,
        },
        settlement_amount: {
        required: true,
        },
        trans_type: {
        required: true,
        },
    },
    messages: {
        settlement_date: {
        required: 'Date is required.'
        },
        settlement_amount: {
        required: 'Amount is required.',
        },
        trans_type: {
        required: 'Type is required.',
        }
    },
    submitHandler: function(form) {
    $.ajax({
    type: "POST",
        url: "{{route('restaurants.save_settlement')}}",
        data: $('#frm_create_settlement').serialize(),
        dataType: "json",
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
        if (data.status == 1) {
        Toast.fire({
        icon: 'success',
        title: data.message
        });
        window.setTimeout(function () {
        window.location.reload();
        }, 1000);
        $("#frm_create_settlement")[0].reset();
        } else {
        Toast.fire({
        icon: 'error',
                title: data.message
        });
        }
        $('button:submit').attr('disabled', false);
        },
        error: function(err) {
        $('button:submit').attr('disabled', false);
        }
    });
    return false;
    }
    });
    $.validator.addMethod("validDate", function(value, element) {
    return this.optional(element) || moment(value, "DD-MM-YYYY").isValid();
    }, "Please enter a valid date in the format DD-MM-YYYY");
    
    $(".ord_check").click(function() {
    if($(this).is(":checked")) {
        $("#settle_now").show();
    } else {
        $("#settle_now").hide();
    }
});

$("#settle_filter").click(function(){
    $('.loading_box').show();
    $('.loading_box_overlay').show();
    var start_date = $('#dates').data('daterangepicker').startDate.format('YYYY-MM-DD');
    var end_date = $('#dates').data('daterangepicker').endDate.format('YYYY-MM-DD');
    var restaurant_id = $('#restaurant_id').val();
    $.ajax({
        type: "GET",
        url: "{{route('restaurants.settlement_details')}}",
        data: {
            start_date: start_date,
            end_date: end_date,
            id : restaurant_id
        },
        success: function (data) {
           $('#settlement_result').html(data);
           $('.loading_box').hide();
           $('.loading_box_overlay').hide();
        }
    });
});
</script>
@endpush