
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6 ml-0">
                    <h6>Restaurant Name</h6>
                    <p>{{ucfirst($restaurant->lang[0]->name)}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Phone</h6>
                    <p>{{$restaurant->contact[0]->phone}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Restaurant Code</h6>
                    <p>{{$restaurant->unique_id}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Email</h6>
                    <p>{{$restaurant->contact[0]->email}}</p>
                </div>
                <div class="col-md-6 ml-0">
                    <h6>Commission Percentage</h6>
                    <p>{{$c_perecent}} %</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-8 ml-0">
                    <label>Total number of orders:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{$total_orders}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total number of order(Online):</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{$total_order_online->online_count}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total number of order(At Counter):</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>{{$total_order_at_counter->online_count}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total Sale:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{ $total_sale ?? 0}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total Commission amount:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{$commmission ?? 0}}</h6>
                </div>
                <div class="col-md-8 ml-0">
                    <label>Total revenue for restaurant:</label>
                </div>
                <div class="col-md-4 ml-0">
                    <h6>SAR {{$total_revenue}}</h6>
                </div>
                @if(count($del_order)> 0)
                <div class="col-md-12 ml-0 text-center">
                    <button type="button" class="btn btn-info save-btn" id="settle_now" style="display:none">SETTLE NOW</button>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width:5%" class="text-center"><input type="checkbox" id="checkAll" class="ord_check"></th>
                            <th>S. No.</th>
                            <th>Order ID</th>
                            <th>Customer</th>
                            <th>Sale Amount</th>
                            <th>Commission</th>
                            <th class="cls_last_child">Restaurant Revenue</th>
                        </tr>
                    </thead>
                    <tbody>
                    <input type="hidden" id="commission_percentage" value="{{$c_perecent}}">
                    <input type="hidden" id="restaurant_id" value="{{$restaurant->id}}">
                    @if(count($del_order)> 0)
                    @php
                    $i = 1;
                    @endphp
                    @foreach($del_order as $row_data)
                    @php
                    $commission = $row_data->grand_total * $c_perecent / 100;
                    $final_amount = $row_data->grand_total - $commission;
                    @endphp
                    <tr>
                        <th class="text-center"><input type="checkbox" name="settle_ids[]" id="settle_ids" value="{{ $row_data->id }}" class="ord_check check_value"></th>
                        <th>{{ $i++ }}</th>
                        <td>{{ $row_data->order_id }}</td>
                        <td>{{ $row_data->customer[0]->name }}</td>
                        <td>{{ $row_data->grand_total }}</td>
                        <td>{{$commission}}</td>
                        <td class="cls_last_child">{{$final_amount}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr><td colspan="8" class="text-center">No records found!</td></tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="text-center d-flex justify-content-center mt-3">
                {{ $del_order->appends(request()->input())->links() }}
            </div>
        </div>
    </div>
</div>
@if($from=='filter')
<script>
   $("#checkAll").click(function(){
    $('input:checkbox').not(this).prop('checked', this.checked);
});
$(".ord_check").click(function() {
    if($(this).is(":checked")) {
        $("#settle_now").show();
    } else {
        $("#settle_now").hide();
    }
 });
 $('#settle_now').click(function(){
    var c_percent = $('#commission_percentage').val();
    var restaurant_id = $('#restaurant_id').val();
    var arr = $('.check_value:checked').map(function(){
        return this.value;
    }).get();
    $.ajax({
        type: "GET",
        url: "{{route('restaurants.payment')}}",
        data: {
            order_ids: arr,
            c_percent: c_percent,
            restaurant_id: restaurant_id,
        },
        success: function (data) {
           $('.modal-content').html(data);
           $('#formModal').modal('show');
        }
    });
});
</script>
@endif