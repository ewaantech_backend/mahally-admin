@extends('layouts.master')

@section('content-title')
{{ __('messages.review.reviews') }}
@endsection
@section('add-btn')

@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('reviews') }}">
                    <div class="row tablenav top text-left">
                        <div class="col-md-5 ml-0">
                           <select class="form-control" id="customer" name="customer">
                                <option value="">By customer</option>
                                @foreach($customer as $customers)
                                    <option  value="{{ $customers->id }}" >{{ $customers->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">{{ __('messages.search') }}</font>
                            </button>
                            <a href="{{ route('reviews') }}" class="btn btn-default reset_style">{{ __('messages.reset') }}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
                         
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr >    
                                <th >Reviewed By</th>
                                <th>Phone Number</th>
                                <th>Rating</th>
                                <th>Reviewed Restaurant</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($reviews) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($reviews as $row_data)
                            <tr>   
                                <td>{{ $row_data->customer[0]->name ?? 'N/A' }}</td>
                                <td>{{ $row_data->customer[0]->phone_number ?? '' }}</td>
                                <td style="text-align: left">
                                   @for($i=0;$i<$row_data->rating;$i++)
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                   @endfor
                                </td>
                                <td>{{ $row_data->restaurant[0]->lang[0]->name ?? ''}}</td>
                                <td>
                                    <span style="float: right;"> <a class="btn btn-sm btn-success text-white view_btn" title="View Order Details" data-id="{{ $row_data->order_id }}"><i class="fa fa-eye"></i></a></span>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $reviews->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    .reset_style {
        margin-left: 15px;
    }
    
</style>

@endpush
@push('scripts')
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
    $('#customer').select2();

    $('.view_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
        type: "GET",
            url: "order/delivered_order_details/" + id,
                success: function(data) {
                $('.modal-content').html(data);
                $('#formModal').modal('show');
                }
        });
    });
</script>
@endpush