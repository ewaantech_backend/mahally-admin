<div class="modal-header">
    <h4 class="modal-title"><strong>
        CUSTOMER
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="pdt_tab">
        <li class="nav-item"> <a class="nav-link active" data-id="{{$customer['id']}}" href="#pdt_info" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-shopping-cart"></i></span> <span class="hidden-xs-down">BASIC INFO</span></a> </li>
        <li class="nav-item disabled"> <a class="nav-link" href="#orders" data-id="{{$customer['id']}}" role="tab" data-toggle="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">ORDERS</span></a> </li>
        
    </ul>
</div>
<div class="tab-content">
    @include('admin.customers.customer_info_view')
</div>
<script>
   $('.nav-tabs a').on('click', function (e) {
       e.preventDefault()
       $(this).tab('show')
   });
   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e)
   {    var id = $(this).data("id");
       var x = $(e.target).text();
       $.ajax({
           type: "GET",
           url: "{{route('customerview_tabs')}}",
           data: {'activeTab': x,
           id:id
                  },
           success: function (result) {
               $('.tab-content').html(result);
           }
       });
   });

</script>