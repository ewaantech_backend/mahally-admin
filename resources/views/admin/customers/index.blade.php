@extends('layouts.master')

@section('content-title')
CUSTOMERS
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                    <div class="col-md-6 ml-0">
                        <form action="{{route('customers.get')}}" id="search-customer-form" method="get">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search Customers">
                            <input type="hidden" name="customer_select" id="customer_select">
                        </form>
                       
                    </div>
                    <div class="col-md-6 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-customer-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('customers.get')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div>                  
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>City</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($customers) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($customers as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <th>{{ $row_data->name ?? 'N/A' }}</th>
                                <th>{{ $row_data->phone_number}}</th>
                                <th>{{ $row_data->email ?? 'N/A'}}</th>
                                <th>{{ $row_data->city ?? 'N/A'}}</th>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_customer">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white view_btn"  title="View" data-id="{{ $row_data->id }}"><i class="fa fa-eye"></i></a>
                                    <a class="btn btn-sm btn-danger text-white customer_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $customers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="customer-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
          
        </div>
    </div>
</div>
<!-- End Modal -->
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
       
        $('.view_btn').on('click', function() {
            var id = $(this).data("id");
            $.ajax({
                type: "GET",
                url: "customers/details/" + id,
                success: function(data) {
                    $('.modal-content').html(data);
                    $('#customer-popup').modal('show');
                }
            });
        });

        $('.change-status').on('click',function(){    
            var id = $(this).data("id");
            var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' Customer',
            content: 'Are you sure to ' + act_value + ' the Customer?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type:"POST",
                        url: "{{route('customers.status.update')}}",
                        data:{ 
                           status:act_value,
                           id: id
                   
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("customers.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
        $('.customer_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Customer? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
           
                    $.ajax({
                        url: "{{route('customers.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("customers.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

        $( "#search_field" ).autocomplete({
            source: function( request, response ) {
        $.ajax( {
          url: "{{route('customers.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#customer_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

    $('.cancel-btn').on('click',function(){
          $('#id_pg').val('');   
    });

    </script>
@endpush