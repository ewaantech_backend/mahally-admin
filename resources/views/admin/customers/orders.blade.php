<div class="card">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Order ID</th>
                        <th>Restaurant</th>
                        <th>Order Date</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th class="text-left">Payment Type</th>
                    </tr>
                </thead>
                <tbody>
                    @if (count($orders) > 0)
                    @php
                    $i = 1;
                    @endphp
                    @foreach ($orders as $row_data)
                    <tr>
                        <th>{{ $i++ }}</th>
                        <th>{{ $row_data->order_id }}</th>
                        <th>{{$row_data->restaurant->lang[0]->name ?? ''}}</th>
                        <th>{{ \Carbon\Carbon::parse($row_data->created_at)->format('d-m-Y')}}</th>
                        <th>{{ $row_data->grand_total}}</th>
                        <th>{{ $order_status[$row_data->order_status] }}</th>
                        
                        <th>{{ $row_data->payment_mode}}</th>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="8" class="text-center">No records found!</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <div class="text-center d-flex justify-content-center mt-3">
            {{ $orders->links() }}
        </div>
    </div>
</div>