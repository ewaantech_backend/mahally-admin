@extends('layouts.master')

@section('content-title')
{{ $arrayData['mainHeading'] }}
@endsection
@section('add-btn')
<button  class="btn btn-info create_btn" id="autosuggest_add_btn">
    <i class="ti-plus"></i> Add New {{ $arrayData['title'] }}
</button>
@endsection
@section('content')
<div class="row">
    <div class="col-lg-12">
        @if($arrayData['is_search'])
            <div class="card">
                <div class="card-body">
                    <div class="row tablenav top text-right">
                        <div class="col-md-6 ml-0">
                            <form action="{{route('autosuggest',['title' => $arrayData['keyValue']])}}" id="search-faq-form" method="get">
                                <input type="text" value ="{{$search_field ?? ''}}" class="form-control" id="search_field" name="search_field" placeholder="Search {{ $arrayData['title'] }}">
                                <input type="hidden" name="autosuggest_select" id="autosuggest_select">
                                <input type="hidden" name="title" id="title" value ="{{$arrayData['keyValue'] ?? ''}}">
                            </form>
                        
                        </div>
                        <div class="col-md-6 text-left">
                            <button type="button" onclick="event.preventDefault(); 
                            document.getElementById('search-faq-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                            <a href="{{route('autosuggest',['title' => $arrayData['keyValue']])}}" class="btn btn-default cancel_style">Reset</a>
                            
                        </div>
                    </div>    
                </div>
            </div>
        @endif                    
            <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                @if($arrayData['is_slno'])
                                    <th>S. No.</th>
                                @endif    
                                <th>{{ $arrayData['title']}} (EN)</th>
                                <th>{{ $arrayData['title']}} (AR)</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($autosuggestData) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($autosuggestData as $row_data)
                            <tr>
                                @if($arrayData['is_slno'])
                                    <th>{{ $i++ }}</th>
                                @endif    
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td class="right-align">{{ $row_data->lang[1]->name ?? '' }}</td>
                                <td class="text-center">
                                    @if($arrayData['is_status'])
                                        <button
                                            type="button"
                                            class="change-status btn btn-sm btn-toggle mr-md-4 ml-0 {{ $row_data['status']}}"
                                            data-toggle="button"
                                            data-id="{{ $row_data->id }}"
                                            data-activate="{{ $row_data['status']}}"
                                            aria-pressed="true"
                                            autocomplete="off"
                                            id="active_faq">
                                            <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                        </button>
                                    @endif    
                                    <a class="btn btn-sm btn-success text-white edit_btn page_edit autosuggest_edit"  title="Edit" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>
                                    @if($arrayData['is_delete'])
                                    <a class="btn btn-sm btn-danger text-white autosuggest_id_del" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $autosuggestData->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="autosuggest-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel"></h4>
                <input hidden id="title_hidden" value="{{ $arrayData['title'] ?? ''}}" >
                 <input hidden id="key_value_hidden" value="{{ $arrayData['keyValue'] ?? ''}}" >
                 <a href="{{route('autosuggest',['title' => $arrayData['keyValue']])}}" class="btn close cancel-btn"> <span aria-hidden="true">&times;</span></a>
                {{-- <button type="button" class="close cancel-btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button> --}}
            </div>
            <div class="modal-body" data-no-padding="no-padding">
                <form id="autosuggest-form" method="POST" action="#">
                    <input type="hidden" id="id_pg" name="id_pg">
                    @csrf
                    <div class="row mb-3">
                        <input type="hidden" name="type" id="type" value ="{{$arrayData['keyValue'] ?? ''}}">
                        <div class="col-md-6">
                            <label>{{ $arrayData['title']}} (EN) <span class="text-danger">*</span></label>
                            <input type="text" placeholder="Enter  {{ $arrayData['title'] }}" class="form-control form-white" id="title_en"
                                                    name="title_en" value="{{ old('title_en') }}">
                            <label class="title_en_error error-msg"></label>
                        </div> 
                         <div class="col-md-6">
                            <label>{{ $arrayData['title']}} (AR) <span class="text-danger">*</span></label>
                             <input type="text" placeholder="Enter  {{ $arrayData['title'] }}" class="form-control form-white text-right" id="title_ar"
                                                    name="title_ar" value="{{ old('title_ar') }}">
                            <label class="title_ar_error error-msg"></label>
                        </div>
                        @if($arrayData['is_picture'])
                        <div class="col-md-6">
            <div class="mb-2">Add Image</div>
            <div class="cover-photo" id="ad_en_image">
                <div id="ad-en-upload" class="add" onclick="$('#uploadFile_en').click();">+</div>
                <div id="ad_en_photo_loader" class="loader" style="display: none;">
                    <img src="{{ asset('assets/images/loader.gif') }}" alt="">
                </div>
                <div class="preview-image-container" id="ad_en_photo_image_preview" style="display:none">
                     <div class="scrn-link" style="position: relative;top: -20px;">
                        <button type="button" class="scrn-img-close delete-cover-en">
                            <i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i>
                        </button>
                         <img class="scrn-img" style="max-width: 200px" id="previewimage_en" src="" alt="">
                    </div>
                </div>
            </div>
            <div class="control-fileupload" style="display: none;">
                <label for="ad_en_photo" data-nocap="1">Select cover photo:</label>
                <input type="file" id="uploadFile_en" name="upload_image" data-imgw="1200" data-imgh="360" data-type="path_en"/>
                <input type="hidden" name="hidden_image_en" id="hidden_image_en">
            </div>
            <div class="mt-2 small">
                <p>Max file size: 1 MB<br />
                    Supported formats: jpeg, png<br />
                    File dimension: 1200 x 360 pixels<br />
                </p>
                <span style="display: none;" class="error" id="ad_en_photo-error">Error</span>
            </div>
        </div>
                        @endif
                        <div class="col-lg-12">
                            <div class="row 5">
                                <div class="col-md-12 text-md-left">
                                    <button type="submit"  class="btn btn-info waves-effect waves-light save_page">
                                        Save
                                    </button>
                                    {{-- <button type="button" class="btn btn-default waves-effect cancel-btn" data-dismiss="modal">
                                        Cancel
                                    </button> --}}
                                     {{-- <a href="{{route('autosuggest',['title' => $arrayData['keyValue']])}}" class="btn btn-info waves-effect waves-light save_page">Done</a> --}}
                                <a href="{{route('autosuggest',['title' => $arrayData['keyValue']])}}" class="btn btn-default reset_style">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->
<div class="modal fade none-border" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Crop Image</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="crop-images">
                <div class="row">
                    <div class="col-md-8">
                        <img id="cropper" src="" alt="" style="max-width: 100%">
                    </div>
                    <div class="col-md-4">
                        <div class="preview"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="crop" type="button" class="btn btn-primary">Crop & Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<style>
    .reset_style {
        margin-left: 15px;
    }
    .search_wid{
        width: 234px;
    }
   
    .btn-fea{
        background-color: #87b23e;
    }
    .validation
    {
      color: red;
     
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="//cdn.ckeditor.com/4.14.0/basic/ckeditor.js"></script>
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});


        $('#autosuggest_add_btn').on('click',function(e){
            $('#autosuggest-form').trigger("reset")
            heading = 'Add ' + $('#title_hidden').val();
            $('#exampleModalLabel').html(heading);
             $("#autosuggest-form")[0].reset();
            $('#autosuggest-popup').modal({
                show:true
            })
        })

         $('.save_page').on('click',function(e){
           
            $("#autosuggest-form").validate({
                ignore: [],
                rules: {
                    title_en  : {        
                        required: true,         
                    },
                    title_ar  : {        
                        required: true,         
                    },   
                },
                messages: {               
                    title_en: {
                        required: $('#title_hidden').val() +" (EN) required",
                        
                    },
                     title_ar: {
                        required: $('#title_hidden').val() +" (AR) required",
                        
                    },
                },
                errorPlacement: function(error, element) {
                    if (element.attr("name") == "title_en" ) {
                        $(".title_en_error").html(error);
                    }
                    if (element.attr("name") == "title_ar" ) {
                        $(".title_ar_error").html(error);
                    }
                    
                },
                 submitHandler: function(form) {
                    $('.loading_box').show();
                    $('.loading_box_overlay').show();
                    let edit_val=$('#id_pg').val();
                    $('button:submit').attr('disabled', true);
                    if(edit_val){
                    $.ajax({
                        type:"POST",
                        url: "{{route('autosuggest.update')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#autosuggest-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }      
                    });
                }else{
                    $.ajax({
                        type:"POST",
                        url: "{{route('autosuggest.store')}}",
                        data: new FormData(form),
                        mimeType: "multipart/form-data",
                        dataType: 'JSON',
                        contentType: false,
                        processData: false,
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                                $("#autosuggest-form")[0].reset();
                            } else {
                                $('.loading_box').hide();
                                $('.loading_box_overlay').hide();
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    }) ;
                    }
                
                }
            });
            
           
        });

         $('.autosuggest_edit').on('click',function(e){
            e.preventDefault();
            heading = 'Edit ' + $('#title_hidden').val();
            $('#exampleModalLabel').html(heading);

            page = $(this).data('id')
            var url = "edit/";
        
            $.get(url  + page, function (data) {
                console.log(data);
                $('#title_en').val(data.page.lang[0].name);
                $('#title_ar').val(data.page.lang[1].name);
                $('#id_pg').val(data.page.id)
                $('#previewimage_en').val(data.page.image_path)
                var uploadsUrl = "<?php echo asset('/uploads') ?>";
                var imgurl = uploadsUrl + '/' + data.page.image_path;
                if (data.page.image_path) {
                    $('#ad_en_photo_image_preview').css('display', 'block');
                    $('#previewimage_en').attr("src", imgurl);
                    $('#hidden_image_en').val(data.page.image_path);
                }
                $('#autosuggest-popup').modal({
                    show: true

                });
            }) 
        })

       
          $('.change-status').on('click',function(){    
            activate = $(this).data('activate');
            $.ajax({
                        type:"POST",
                        url: "{{route('autosuggest.status.update')}}",
                        data:{ 
                           status:activate,
                           id: $(this).data('id')
                   
                        },
                        success: function(result){                        
                                    Toast.fire({
                                    icon: 'success',
                                    title: 'Status updated successfully'
                                    });
                                    window.location.reload();
                                   
                        }
                })
        });

            $('.autosuggest_id_del').on('click',function(){
           
            Swal.fire({  
                title: 'Are you sure to delete?',  
                text: "You won't be able to revert this!",  
                icon: 'warning',  
                showCancelButton: true,  
                confirmButtonColor: '#3085d6',  
                cancelButtonColor: '#d33',  
                confirmButtonText: 'Yes, delete it!'
            })
            .then((result) => {  
                if (result.value) {    
                    $.ajax({
                        url: "{{route('autosuggest.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:$(this).data('id')
                        },
                        success: function(data) {
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Deleted successfully'
                                    });
                                    window.location.reload();
                        }
                    });
                }
            })
        });

         function removesrc_en(){
        $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
        $('#hidden_image_en').remove();
        $("#uploadFile_en").val("");
         $(".delete-img").hide();
    };

    $('#uploadFile_en').on('change', function () {
    var id =$('#id_pg').val();
    var imgw = $(this).data('imgw');
    var imgh = $(this).data('imgh');
    var img_type = $(this).data('type');
    const file = $(this)[0].files[0];
    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;
    var _URL = window.URL || window.webkitURL;
    img.src = _URL.createObjectURL(file);
    img.onload = function() {
    imgwidth = this.width;
    imgheight = this.height;
    if (imgwidth >= imgw && imgheight >= imgh){
    readUrl(file, img_type, uploadFile, imgw, imgh, id);
    } else{
    $('input[type="file"]').val('');
    Toast.fire({
    icon: 'error',
            title: 'Image size must be greater or equal to ' + imgw + ' X ' + imgh,
    });
    }
    }
    });
    function uploadFile(file, type, id) {
    var formData = new FormData();
    formData.append('photo', file);
    formData.append('id', id);
    formData.append('type', type);
    $.ajax({
    type: "POST",
            url: "{{route('admin.upload_image')}}",
            dataType: "json",
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false, // tell jQuery not to set contentType
            beforeSend: function() {
            if(type == 'path_en'){
            $(`#ad_en_photo_loader`).show();
            }else{
            $(`#ad_ar_photo_loader`).show();
            }
            $('#image-modal').modal('hide');
            },
            success: function(data) {
            if (data.status == 1) {
            Toast.fire({
            icon: 'success',
                    title: data.message
            });
            $('#id_pg').val(data.id);
            $('#ad_en_image').html('<div id="ad-en-upload" class="add">+</div>' +
                    '<div id="ad_en_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                    '<div class="preview-image-container" id="ad_en_photo_image_preview">' +
                    '<div class="scrn-link" style="position: relative;top: -20px;">' +
                    '<button type="button" class="scrn-img-close delete-cover-en" data-type="path_en" data-id="' + data.id + '">' +
                    '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                    '<img class="scrn-img" style="max-width: 200px" src="' + data.path + '" alt="">' +
                    '</div></div>' +
                    '<script>' +
                    '$(".delete-cover-en").click(function(e) {' +
                    'delete_image(' + data.id + ');' +
                    '});<\/script>');
            $('input[type="file"]').val('');
            } else {
            Toast.fire({
            icon: 'error',
                    title: data.message
            });
            $(`ad_en_photo_loader`).hide();
            $(`#ad_ar_photo_loader`).hide();
            }
            }
    });
    }

    $('.cancel-btn').on('click',function(){
          $('#id_pg').val('');
         $('#previewimage_en').attr('src', '{{ asset('assets/images/upload.png') }}');
            $("#autosuggest-form")[0].reset();        
    });

    $("#search_field").autocomplete({
        source: function( request, response ) {
        $.ajax( {
          url: "{{route('autosuggest.search')}}",
          method:'post',
          data: {
            search: request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 1,
      select: function( event, ui ) {
        $('#search_field').val(ui.item.label); // display the selected text
        $('#autosuggest_select').val(ui.item.value); // save selected id to input
           return false;
      }
    });

   $('.delete-cover-en').click(function(e) {
    var id =$('#id_pg').val();
    delete_image(id);
    });
    
    function delete_image(id){
        $.confirm({
            title: '<span class="small">Are you sure to delete this image?</span>',
            content: 'You wont be able to revert this',
            buttons: {
            Yes: function () {
            $.ajax({
            type: "POST",
                    url: "{{route('admin.detete_img')}}",
                    data: {
                    id: id,
                    },
                    dataType: "json",
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        
                    if (data.status == 1) {
                    Toast.fire({
                    icon: 'success',
                            title: data.message
                    });
                    $('#ad_en_image').html('<div id="ad-en-upload" class="add">+</div>' +
                            '<div id="ad_en_photo_loader" class="loader" style="display: none;"><img src="" alt=""></div>' +
                            '<div class="preview-image-container" style="display: none;" id="ad_en_photo_image_preview">' +
                            '<div class="scrn-link" style="position: relative;top: -20px;">' +
                            '<button type="button" class="scrn-img-close" data-type="path_en" data-id="' + data.id + '">' +
                            '<i class="ti-close" style="position: absolute; top: 5px; right: 5px;"></i></button>' +
                            '<img class="scrn-img" style="max-width: 200px" src="" alt="">' +
                            '</div></div>' +
                            '<script>' +
                            '$("#ad-en-upload").click(function (e) {' +
                            '$("#uploadFile_en").click();});<\/script>'); 
                    } else {
                    Toast.fire({
                    icon: 'error',
                            title: data.message
                    });
                    }
                    }
            });
            },
                    No: function () {
                    console.log('cancelled');
                    }
            }
    });
    }
    </script>
@endpush