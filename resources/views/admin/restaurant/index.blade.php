@extends('layouts.master')

@section('content-title')
RESTAURANTS
@endsection
@section('add-btn')
<a href="javascript:void(0);" class="btn btn-info rest_modal_btn">
    <font style="vertical-align: inherit;"><i class="ti-plus"></i> Add New Restaurant
    </font>
</a>
@endsection
@section('content')
{{-- @include('admin.cms.faqs.popup') --}}
<div class="row">
    <div class="col-lg-12">
        {{-- <div class="card">
            <div class="card-body">
                <div class="row tablenav top text-right">
                     <form action="{{route('restaurants.get')}}" id="search-promocode-form" method="get">
                    <div class="col-md-6 ml-0">
                        <input type="text" value ="{{$search_field ?? ''}}"class="form-control" id="search_field" name="search_field" placeholder="Search by restaurant name">
                        <input type="hidden" name="rest_select" id="rest_select">  
                    </div>
                    <div class="col-md-4 ml-0">
                            <select class="form-control search_val" name="select_role" id="select_role">
                                <option value="">By Role</option>
                                @foreach($role as $rolelist)
                                <option value="{{$rolelist->id}}" {{ $rolelist->id ==  $role_id  ? 'selected' : '' }}>{{$rolelist->name}}</option>
                                @endforeach
                            </select>
                    </div>
                    </form>
                    <div class="col-md-2 text-left">
                        <button type="button" onclick="event.preventDefault(); 
                        document.getElementById('search-promocode-form').submit();" class="btn btn-info"><font style="vertical-align: inherit;">Search</font></button>
                        <a href="{{route('restaurants.get')}}" class="btn btn-default cancel_style">Reset</a>
                        
                    </div>
                </div>
            </div>
        </div> --}}
        <div class="card">
            <div class="card-body">
                <form id="posts-filter" method="get" action="{{ route('restaurants.get') }}">
                    <div class="row tablenav top text-right">
                        <div class="col-md-5 ml-0">
                            <input class="form-control" type="text" name="search_field" value="{{$search_field ?? ''}}" placeholder="Search by Restaurant ID /Restaurant Name">
                        </div>
                        <div class="col-md-4 ml-0">
                            <select class="form-control search_val" name="select_city" id="select_city">
                                <option value="">By City</option>
                                @foreach($city as $citylist)
                                <option value="{{$citylist->id}}" {{ $citylist->id ==  $city_id  ? 'selected' : '' }}>{{$citylist->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 text-left">
                            <button type="submit" class="btn btn-info">
                                <font style="vertical-align: inherit;">Search</font>
                            </button>
                            <a href="{{ route('restaurants.get') }}" class="btn btn-default reset_style">Reset</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>                  
        <div class="card">
            <div class="card-body">
                <div id="msgDiv"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>S.No.</th>
                                <th>Restaurant ID</th>
                                <th>Restaurant Name</th>
                                <th>Restaurant Phone/Email</th>
                                <th>Contact Person Name/Phone</th>
                                <th>City</th>
                                <th width="20%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($restaurant) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($restaurant as $row_data)
                            <tr>
                                <th>{{ $i++ }}</th>
                                <td>{{ $row_data->unique_id }}</td>
                                <td>{{ $row_data->lang[0]->name ?? '' }}</td>
                                <td>{{ $row_data->contact[0]->phone ?? '' }}<br>{{ $row_data->contact[0]->email ?? '' }}</td>
                                <td>{{ $row_data->contact[0]->contact_person ?? '' }}<br>{{ $row_data->contact[0]->phone ?? '' }}</td>
                                <td>{{ $row_data->city[0]->name ?? '' }}</td>
                                <td class="text-center">
                                    <button
                                        type="button"
                                        class="change-status btn btn-sm btn-toggle ml-0 {{ $row_data['status']}}"
                                        data-toggle="button"
                                        data-id="{{ $row_data->id }}"
                                        data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}"
                                        aria-pressed="true"
                                        autocomplete="off"
                                        id="active_faq">
                                        <div class="handle" data-toggle="tooltip" data-placement="top" title="Activate / Deactivate"></div>
                                    </button>
                                    <a class="btn btn-sm btn-success text-white rest_modal_btn" title="Edit Restaurant" data-id="{{ $row_data->id }}"><i class="fa fa-edit"></i></a>

                                    <a class="btn btn-sm btn-success text-white rest_modal_btn" title="Options" href="{{ route('restaurants.payment_settlement', $row_data->id) }}"><i class="fa fa-diamond" aria-hidden="true"></i></a>

                                    <a class="btn btn-sm btn-danger text-white restaurant_id_del" data-id="{{ $row_data->id }}" data-activate="{{ $row_data['status'] == 'active' ? 'Deactivate' : 'Activate' }}" title="Delete"><i class="fa fa-trash"></i></a>
                                    <a class="btn btn-sm btn-warning text-white welcome_send" title="Sent welcome mail" data-id="{{ $row_data->id }}"><i class="fa fa-envelope"></i></a>
                                    <a href="{{route('admin.restaurant-qrcode',$row_data->id)}}"class="btn btn-sm btn-danger waves-effect" title="print restaurant QR code"  target="_blank"><i class="fa fa-print"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $restaurant->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- popup --}}
<div class="modal fade" id="rest-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog mw-100" role="document">
        <div class="modal-content">
            
          
        </div>
    </div>
</div>
<!-- End Modal -->
@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
<link rel="stylesheet" href="{{ asset('assets/css/lib/cropper/cropper.min.css') }}">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<style>
.cancel_style{
    margin-left: 20px;
}
#site-error{
float:right;
margin-right: 260px;
margin-top: 10px;

}
.area{
        height: 80px;
    }
    .modal {
    
}

</style>

@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="{{ asset('assets/js/lib/cropper/cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/lib/cropper/jquery-cropper.min.js') }}"></script>
<script src="{{ asset('assets/js/custom_crop.js') }}"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
 
<script>
   
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        

        $('.rest_modal_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "{{route('create_restaurant')}}",
            data: {
                id: id
            },
            success: function(data) {
                $('.modal-content').html(data);
                $('#rest-popup').modal('show');
            }
        });
    });
    $('.view_btn').on('click', function() {
        var id = $(this).data("id");
        $.ajax({
            type: "GET",
            url: "products/details/" + id,
            success: function(data) {
                $('.modal-content').html(data);
                $('#rest-popup').modal('show');
            }
        });
    });
    $('.change-status').on('click', function() {
        var rest_id = $(this).data("id");
        var act_value = $(this).data("activate");
        $.confirm({
            title: act_value + ' restaurant',
            content: 'Are you sure to ' + act_value + ' the restaurant?',
            buttons: {
                Yes: function() {
                    $.ajax({
                        type: "POST",
                        url: "{{route('restaurants.status.update')}}",
                        data: {
                            id: rest_id,
                            status:act_value,
                        },
                        dataType: "json",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("restaurants.get")}}';
                                }, 1000);
                                //                    window.location.reload();
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                                 window.setTimeout(function() {
                                    window.location.reload();
                                }, 1000);
                            }
                        }
                    });
                },
                No: function() {
                    window.location.reload();
                }
            }
        });
    });

   $('.restaurant_id_del').on('click',function(){
            var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Restaurant? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
           
                    $.ajax({
                        url: "{{route('restaurants.delete')}}" ,
                        type: 'POST',
                        data:{
                            id:id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("restaurants.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });
      $("#search_field").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{route('restaurants.auto')}}",
                method: 'post',
                data: {
                    search: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $('#search_field').val(ui.item.label); // display the selected text
            $('#rest_select').val(ui.item.value); // save selected id to input
            return false;
        }
    });
  $('.welcome_send').on('click', function(e) {
        e.preventDefault();
        var id = $(this).data("id");
        $.confirm({
            title: 'Confirmation',
            content: 'Are you sure you want to send wlcome mail?',
            buttons: {
                Yes: function() {
           
                    
                    $.ajax({
                        url: "{{route('restaurants.sentWelcome')}}",
                        type: 'POST',
                        data: {
                            id: id
                        },
                        success: function(data) {
                           
                            if (data.status == 1) {
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                                window.setTimeout(function() {
                                    window.location.href = '{{route("restaurants.get")}}';
                                }, 1000);

                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                
                },
         
                No: function() {
                    window.location.reload();
                }
            }
        });
    });
    </script>
@endpush