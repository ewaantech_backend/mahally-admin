<div class="modal-header">
    <h4 class="modal-title"><strong>
            {{( isset($row_data['id']) && $row_data['id'] != '' ) ? 'Edit' : 'Add' }}
            Restaurant
        </strong></h4>
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
</div>
<div class="col-sm-12">
    <ul class="nav nav-tabs" role="tablist" id="rest_tab">
        <li class="nav-item w-25 "> <a class="nav-link active" href="#basic_info" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">BASIC INFO</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#contact_info" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">CONTACT INFO</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#dmt_photo" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">DOCUMENTS & PHOTOS</span></a> </li>
        <li class="nav-item w-25  disabled"> <a class="nav-link" href="#fcty_terms" data-id="{{ isset($row_data['id']) ? $row_data['id']  : '' }}" role="tab"><span class="hidden-sm-up"><i class="ti-folder"></i></span> <span class="hidden-xs-down">FACILITIES & TERMS</span></a> </li>
    </ul>
</div>
<div class="tab-content">
    @include('admin.restaurant.basic_info')
</div>


<style type="text/css">
    img {
        display: block;
        max-width: 100%;
    }
    .preview {
        overflow: hidden;
        width: 160px;
        height: 160px;
        margin: 10px;
        border: 1px solid red;
    }
    #formModal { overflow-y:auto; }
 
</style>

<script>
    $("button[data-dismiss=modal]").click(function () {
        location.reload();
    });
    $('.nav-tabs a').on('click', function (e) {
        var id = $(this).data("id");
        var x = $(e.target).text();
        var location = $(this).attr('href');
        if (id != '') {
            $.ajax({
                type: "GET",
                url: "{{route('rest_tabs')}}",
                data: {'activeTab': x, rest_id: id},
                success: function (result) {
                $('#rest_tab a[href="'+location+'"]').tab('show');
                    $('.tab-content').html(result);
                }
            });
        }
    });
</script>