<form id="frm_create_restaurant" action="javascript:;" method="POST">
    <div class="tab-pane active" id="pdt_info" role="tabpanel">
        <div class="modal-body" id="tab-body">
            <div class="row">
                <div class="col-md-6">
                    <label class="control-label">Restaurant Name (EN) <span class="text-danger">*</span></label>
                    <input class="form-control form-white" placeholder="Enter restaurant name (EN)" type="text" name="name_en" value="{{ isset($row_data['id']) ? $row_data->lang[0]->name : ''}}"/>
                </div>
                <div class="col-md-6 text-right">
                    <label class="control-label">Restaurant Name (AR) <span class="text-danger">*</span></label>
                    <input class="form-control form-white text-right" placeholder="Enter restaurant name (AR)" type="text" name="name_ar" value="{{ isset($row_data['id']) ? $row_data->lang[1]->name : ''}}"/>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Restaurant Type <span class="text-danger">*</span></label><br/>
                    <select class="select2_multiple" name="restaurant_type[]" multiple="multiple" id="restaurant_type">
                        @foreach($restaurant_type as $row_val)
                        <option  value="{{ $row_val->id }}" {{ !empty($rt_tp_ids) && in_array($row_val->id, $rt_tp_ids) ? 'selected' : '' }}>{{ $row_val->lang[0]->name }}</option>
                        @endforeach
                    </select>
                    <label class="control-label" id="error_chk"></label>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Membership <span class="text-danger">*</span></label>
                    <select class="form-control" name="membership_id" id="membership_id">
                        <option value=""> Select membership </option>
                        @foreach($membership_data as $membership_data_val)
                        <option value="{{ $membership_data_val->membership_id  }}" {{ (isset($row_data['membership_id']) &&  $membership_data_val->membership_id  == $row_data['membership_id']) ? 'selected' : '' }}>{{ $membership_data_val->membership_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">Commission Category <span class="text-danger">*</span></label>
                    <select class="form-control" name="commission_category_id" id="commission_category_id">
                        <option value=""> Select commission category </option>
                        @foreach($commission_data as $commission_data_val)
                        <option value="{{ $commission_data_val->id  }}" {{ (isset($row_data['commission_category_id']) &&  $commission_data_val->id  == $row_data['commission_category_id']) ? 'selected' : '' }}>{{ $commission_data_val->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label">City <span class="text-danger">*</span></label>
                    <select class="form-control" name="city_id" id="city_id">
                        <option value=""> Select city </option>
                        @foreach($city_data as $city_val)
                        <option value="{{ $city_val->id  }}" {{ (isset($row_data['city_id']) &&  $city_val->id  == $row_data['city_id']) ? 'selected' : '' }}>{{ $city_val->name }}</option>
                        @endforeach
                    </select>
                </div>
               <div class="col-md-3">
                    <label class="control-label">Opening Time <span class="text-danger">*</span></label>
                    <input class="form-control" type="time" id="opening_time" name="opening_time" value="{{ isset($row_data['id']) ?  optional($row_data->opening_time)->format('H:i:s') : '00:00'}}" />
                    <label class="opening_time_error"></label>
                </div>
                <div class="col-md-3">
                    <label class="control-label">Closing Time <span class="text-danger">*</span></label>
                     <input class="form-control" type="time" id="closing_time" name="closing_time" value="{{ isset($row_data['id']) ?  optional($row_data->closing_time)->format('H:i:s') : '00:00'}}" />
                    <label class="closing_time_error"></label>
                </div>
                 <div class="col-md-6"></div>
                <div class="col-md-12 mb-2">
                    @if(isset($row_data['vat_includes']) &&  $row_data['vat_includes'] == 'yes')
                        <input type="checkbox" name="vat_includes" id="vat_includes" value="yes" checked> VAT Includes 
                    @else
                        <input type="checkbox" name="vat_includes" id="vat_includes" value="no" > VAT Includes  
                    @endif
                </div>
                <div class="col-md-4">
                    <label class="control-label">VAT Percentage <span class="text-danger vat-check">*</span></label>
                    <input class="form-control form-white" placeholder="Enter VAT Percentage" type="number" name="vat_percentage" id="vat_percentage" value="{{ isset($row_data['id']) ? $row_data['vat_percentage'] : ''}}"  min="0" max="100"/>
                    <span class="vat_error" id="vat_percentage_error"></span>
                </div>
                <div class="col-md-4">
                    <label class="control-label">VAT Register Number <span class="text-danger vat-check">*</span></label>
                    <input class="form-control form-white" placeholder="Enter VAT Register Number" type="text" id="vat_reg_number" name="vat_reg_number" value="{{ isset($row_data['id']) ? $row_data['vat_reg_number'] : ''}}"/>
                    <span class="vat_error" id="vat_reg_number_error"></span>
                </div>
                
                <div class="col-md-12">
                    <label>Location</label>
                    <input 
                        id="location" 
                        class="form-control" 
                        name="location" 
                        placeholder="Enter your location" 
                        value="{{ isset($row_data['id']) ? $row_data['location'] : old('locations','Riyadh Saudi Arabia')}}"
                    />
                    @error('location')
                    <span class="error">{{ $message }}</span>
                    @enderror
                </div>
                <div class="col-md-12" style="">
                    <div id="map"></div>
                </div>
                <input id="latitude" type="hidden" value="{{ isset($row_data['id']) ? $row_data['latitude'] : old('latitude', 24.7136)}}" name="latitude">
                <input id="longitude" type="hidden" value="{{ isset($row_data['id']) ? $row_data['longitude'] : old('longitude', 46.6753)}}"  name="longitude">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Description (EN) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" id="description_en" name="description_en"placeholder="Enter description">{{ isset($row_data['id']) ? $row_data['lang'][0]['description'] : ''}}</textarea>
                    <label class="description_en_error"></label>
                </div> 
                <div class="col-md-6">
                    <label>Description (AR) <span class="text-danger">*</span></label>
                    <textarea class="form-control area" id="description_ar" name="description_ar" placeholder="أدخل سؤالك" style="text-align:right !important">{{ isset($row_data['id']) ? $row_data['lang'][1]['description'] : ''}}</textarea>
                    <label class="description_ar_error"></label>
                </div> 
            </div>
            
        </div>
        <div class="modal-footer">
            <input type="hidden" id="rest_id" name="rest_id" value="{{ isset($row_data['id']) ? $row_data['id'] : '' }}">
            @if(isset($row_data['id']))
            <button type="submit" class="btn btn-info waves-effect waves-light save-btn">
                Save
            </button>
            @endif
            <button type="submit" class="btn btn-info waves-effect waves-light save-and-continue">
                Save & Continue
            </button>

            <input type="hidden" id="submit_action" value="" />
            <a class="btn btn-default waves-effect" href="{{ route('restaurants.get') }}">Back To Listing</a>
            {{-- <div id="pageloader">
   <img src="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.16.1/images/loader-large.gif" alt="processing..." />
</div> --}}
        </div>
    </div>
</form>
<style>
   
      /* Optional: Makes the sample page fill the window. */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;
      }

      #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
      }

      #map {
        
      width: 100%;
      height: 400px;
    }
      
      #title {
        color: #fff;
        background-color: #4d90fe;
        font-size: 25px;
        font-weight: 500;
        padding: 6px 12px;
      }

      #target {
        width: 345px;
      }
      .tabpanel{
    overflow-y: initial !important
}
#tab-body{
    height: 80vh;
    overflow-y: auto;
}
.area{
        height: 100px;
    }
.vat-check{
    display: none;
}
.vat_error{
    font-size: 12px;
    color: red;
}
.pac-container{
    z-index:1051!important;
}
</style>

<script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
 <script>
    let map;

    function initMap() {
        // The location of Uluru
        var lat =   $('#latitude').val();
        var long =   $('#longitude').val();
    
        if(lat.length === 0){
            var uluru = {lat: {{old('latitude', 24.7136) }} , lng: {{old('longitude', 46.6753)}} };
        }else{       
                var uluru = {lat: {{ old('latitude', 'JSON.parse(lat)') }}, lng: {{ old('longitude', 'JSON.parse(long)') }} };    
        }
        // The map, centered at Uluru
         map = new google.maps.Map(
            document.getElementById('map'), 
            {zoom: 10, center: uluru,
            gestureHandling: 'greedy'}
        );
        // The marker, positioned at Uluru
        marker = new google.maps.Marker({
            position: uluru, 
            map: map,
            draggable: true,
        });

        marker.addListener('dragend', setLatLng);

        const input = document.getElementById('location');
        let autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.bindTo('bounds', map);

       
        autocomplete.addListener('place_changed', function() {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

            let address = place.formatted_address.split(',');
            let formattedAddress = address.length > 1
                ? address[0] + ', ' +address[address.length - 1]
                : address[0];

            $('#location').val(formattedAddress);
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        });
    }

    function setLatLng(event) {
        const lat = event.latLng.lat();
        const lng = event.latLng.lng();
        $('#latitude').val(lat);
        $('#longitude').val(lng);

        setAddress(lat, lng)
    }

    function setAddress(lat, lng) {
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({'location': {lat, lng}}, function(results, status) {
            if (status === 'OK' && results[0]) {
                let address = results[0].formatted_address.split(',');
                let formattedAddress = address.length > 1
                    ? address[0] + ', ' +address[address.length - 1]
                    : address[0];

                $('#location').val(formattedAddress);
            } 
        });
    }
    
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?libraries=places,geocoder,drawing&key=AIzaSyBG_t-XkmJAtPKIpyhOoiXz6QuXphkaBQI&callback=initMap">
</script>  

<script>
     $("#vat_includes").on('click', function (e) {
      if($(this).val() == 'no'){
         $("#vat_includes").val('yes');
         $(".vat-check").show(); 
      }
      else{
          $("#vat_includes").val('no');
           $(".vat-check").hide();
           $('#vat_percentage_error').text(" ");
           $('#vat_reg_number_error').text(" ");  
      }
    });

    $(".select2_multiple").select2({
        placeholder: "Select Multiple Restaurant Type",
        allowClear: true
    });
     $(".select2_multiple").on('select2:select',function (e){
        $("#restaurant_type-error").text('');
    });
//    $(".select2-selection--multiple").addClass("form-control");
    $(".select2-selection--multiple").css({"border": "1px solid #ccc", "border-radius": "0", "padding": "3px 0px 9px 0px", "height": "auto", "width": "360"});
    $(".save-and-continue").on('click', function (e) {
        $("#submit_action").val('continue');

        if($("#vat_includes").val() == 'yes'){
           
            if( $("#vat_percentage").val() == ''){
                $('#vat_percentage_error').text("VAT Percentage required");
                 e.preventDefault();
            }
            if( $("#vat_reg_number").val() == ''){
                $('#vat_reg_number_error').text("VAT Register number required");
                 e.preventDefault();
            } 
        }
        else{
           $('#vat_percentage_error').text(" ");
           $('#vat_reg_number_error').text(" "); 
        }
    });
    $(".save-btn").on('click', function (e) {
        $("#submit_action").val('save');
        if($("#vat_includes").val() == 'yes'){
           
            if( $("#vat_percentage").val() == ''){
                $('#vat_percentage_error').text("VAT Percentage required");
                 e.preventDefault();
            }
            if( $("#vat_reg_number").val() == ''){
                $('#vat_reg_number_error').text("VAT Register number required");
                 e.preventDefault();
            } 
        }
        else{
           $('#vat_percentage_error').text(" ");
           $('#vat_reg_number_error').text(" "); 
        }
    });
    $("#vat_percentage").keyup(function(){
        if($('#vat_percentage').val() != "") {
            var value = $('#vat_percentage').val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
            var intRegex = /^\d+$/;
            if(!intRegex.test(value)) {
                $('#vat_percentage_error').text("Accept only numeric value");
                success = false;
            }
        } else {
            $('#vat_percentage_error').text("")
            success = false;
        }
    });

    $("#frm_create_restaurant").validate({
        normalizer: function (value) {
            return $.trim(value);
        },
        rules: {
            name_en: {
                required: true,
            },
            name_ar: {
                required: true,
            },
            "restaurant_type[]":{
              required: true,
            },
            membership_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            commission_category_id: {
                required: true,
            }, 
            opening_time: {
                required: true,

            }, 
            closing_time: {
                required: true,
                greaterThan: '#opening_time'
            }, 
            description_en: {
                required: true,
            },
            description_ar: {
                required: true,
            },    
        },
        messages: {
            name_en: {
                required: 'Restaurant name(EN)  required.'
            },
            name_ar: {
                required: 'Restaurant name(AR)  required.'
            },
            "restaurant_type[]":{
              required: "Restaurant type  required.",
            },
            membership_id: {
                required: "Membership  required.",
            },
            city_id: {
                required: "City  required",
            },
            commission_category_id: {
                required: "Commission category required",
            },
            opening_time: {
                required: "Opening time required",
            }, 
            closing_time: {
                required: "Closing time required",
                // greaterThan: 'Closing time is always geater then opening time.'
            },
            description_en: {
                required: "Description (EN) required",
            },
            description_ar: {
                required: "Description (AR) required",
            }, 
        },
        errorElement: "label",
        errorPlacement: function (error, element) {
            if (element.attr("id") == "restaurant_type") {
                error.insertAfter("#error_chk");
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {
            $('.loading_box').show();
            $('.loading_box_overlay').show();
            $.ajax({
                type: "POST",
                url: "{{route('save_basic_info')}}",
                data: $('#frm_create_restaurant').serialize(),
                dataType: "json",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data.status == 1) {
                        if ($("#submit_action").val() == 'continue') {
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                            $('#rest_tab a[href="#contact_info"]').tab('show');
                            $('.tab-content').html(data.result);
                        } else {
                            
                            Toast.fire({
                                icon: 'success',
                                title: data.message
                            });
                        }
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: data.message
                        });
                        $("#rest_tab").tabs({cache: true});
                    }
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                },
                error: function (err) {
                    $('.loading_box').hide();
                    $('.loading_box_overlay').hide();
                    $('button:submit').attr('disabled', false);
                }
            });
            return false;
        }
    });
    $.validator.addMethod('greaterThan', function (value, element, param) {
        if (this.optional(element))
            return true;
        var i = parseInt(value);
        var j = parseInt($(param).val());
        return i > j;
    }, "Closing time is always geater then opening time.");
</script>
 
