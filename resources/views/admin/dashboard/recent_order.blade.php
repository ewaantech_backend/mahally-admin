 <div class="card">
    <div class="card-body"><div class="row">
    <div class="col-lg-12 ml-0">
        <div class="page-title">
            
            <div class="row">
                <div class="col-md-8">
                    <h5 style="margin-top: 2.25rem;">Recent Orders</h5>
                </div>
                <div class="col-md-4 " >
                    <select class="form-control restaurant_recent_order"> 
                        <option value="0">By Restaurant</option>
                        @foreach ($resturants as $item)
                            <option value="{{$item->id}}">{{$item->lang[0]->name}}</option> 
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        {{-- <div class="card">
            <div class="card-body"> --}}
                <div id="msgDiv"></div>
                <div class="table-responsive" id="recent_order">
                    <table class="table table-bordered color-table">
                        <thead>
                            <tr>
                                <th>S. No.</th>
                                <th>Order ID</th>
                                <th>Restaurant</th>
                                <th>Customer</th>
                                <th>Order Date</th>
                                <th>Amount</th>
                                <th >Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                          
                            @if(count($recent_order) > 0 )
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($recent_order as $item)
                            <tr> 
                                <td>{{$i++}}</td>
                                <td>{{$item->order_id ?? ''}}</td>
                                <td>{{$item->rest_name ?? ''}}</td>
                                <td>{{$item->customer_name ?? 'N/A'}}</td>
                                <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</td>
                                <td>SAR {{$item->grand_total ?? ''}}</td>
                                <td>
                                    @if($item->order_status=='2')
                                    <button class="button" style="background-color: #4CAF50;">{{$order_status[$item->order_status]}}</button>
                                   
                                     @elseif($item->order_status=='3')   
                                    <button class="button" style="background-color: #ffd400;">{{$order_status[$item->order_status]}}</button>

                                    @else
                                    <button class="button" style="background-color: #ff6a00;">{{$order_status[$item->order_status]}}</button>
                                    @endif
                                </td>
                                
                                <td class="text-center">
                                     <a class="btn btn-sm btn-success text-white view_btn" title="View Order Details" data-id="{{ $item->id }}"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                            @endforeach
                                
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>

                            @endif
                        </tbody>
                    </table>
                </div>
            {{-- </div>
        </div> --}}
    </div>
</div>
 </div>
</div>
<!-- Modal pooup -->
<div class="modal none-border" id="formModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" style="max-height:85%;  margin-top: 50px; margin-bottom:50px;max-width: 100%">
        <div class="modal-content">

        </div>
    </div>
</div>
<!-- END MODAL -->
@push('css')
<style>
.button {
  border: none;
  color: white;
  padding: 2px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 12px;
}
</style>
@endpush
@push('scripts')
<script>
    $('.view_btn').on('click', function() {
    var id = $(this).data("id");
    $.ajax({
    type: "GET",
            url: "admin/order/details/" + id,
            success: function(data) {
            $('.modal-content').html(data);
            $('#formModal').modal('show');
            }
    });
    });

    $('.restaurant_recent_order').on('change',function(){
     var rest_id = $(this).val();
    $.ajax({
    type: "POST",
        url: "{{route('dashboard.getRecentorders')}}",
        data:{
            rest_id:rest_id,
        },
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
             $("#recent_order").html(data);
        }
    });
 });
</script>
  @endpush