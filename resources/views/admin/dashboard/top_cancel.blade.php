<div class="col-lg-6 ml-0">
    <div class="col-md-8">
        <h5>Top Order Cancelling Restaurants</h5>
    </div>
    <div class="card">
        <div class="card-body">
            <div id="msgDiv"></div>
            <div class="table-responsive">
                <table class="table table-bordered color-table">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Restaurant Name</th>
                            <th>No .Of Orders</th>
                            <th>Total Sale</th>
                            <th>Total Mahally Revenue</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($top_order_cancelling_restaurant) > 0 )
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($top_order_cancelling_restaurant as $item)
                        @if(!empty($item->restaurant))
                        <tr> 
                            <td>{{$i++}}</td>
                            <td>{{$item->restaurant->lang[0]->name ?? ''}}</td>
                            <td>{{$item->total_order ?? ''}}</td>
                            <td>SAR {{$item->grand_total ?? ''}}</td>
                            @php
                                $revenue = 0;
                                if(isset($item->restaurant->commission_category->value) &&  isset($item->grand_total)){
                                     $revenue = ($item->grand_total* $item->restaurant->commission_category->value)/100;
                                } 
                                 
                            @endphp
                            <td>SAR {{$revenue ?? ''}}</td>
                        </tr>
                        @endif
                        @endforeach
                            
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No records found!</td>
                        </tr>

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>