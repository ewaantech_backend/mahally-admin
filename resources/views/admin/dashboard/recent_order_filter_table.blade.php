<table class="table table-bordered color-table" id="recent_order">
    <thead>
        <tr>
            <th>S. No.</th>
            <th>Order ID</th>
            <th>Restaurant</th>
            <th>Customer</th>
            <th>Order Date</th>
            <th>Amount</th>
            <th >Status</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        
        @if(count($recent_order) > 0 )
        @php
        $i = 1;
        @endphp
        @foreach ($recent_order as $item)
        <tr> 
            <td>{{$i++}}</td>
            <td>{{$item->order_id ?? ''}}</td>
            <td>{{$item->rest_name ?? ''}}</td>
            <td>{{$item->customer_name ?? 'N/A'}}</td>
            <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</td>
            <td>SAR {{$item->grand_total ?? ''}}</td>
            <td>
                @if($item->order_status=='2')
                <button class="button" style="background-color: #4CAF50;">{{$order_status[$item->order_status]}}</button>
                
                    @elseif($item->order_status=='3')   
                <button class="button" style="background-color: #ffd400;">{{$order_status[$item->order_status]}}</button>

                @else
                <button class="button" style="background-color: #ff6a00;">{{$order_status[$item->order_status]}}</button>
                @endif
            </td>
            
            <td class="text-center">
                    <a class="btn btn-sm btn-success text-white view_btn" title="View Order Details" data-id="{{ $item->id }}"><i class="fa fa-eye"></i></a>
            </td>
        </tr>
        @endforeach
            
        @else
        <tr>
            <td colspan="8" class="text-center">No records found!</td>
        </tr>

        @endif
    </tbody>
</table>