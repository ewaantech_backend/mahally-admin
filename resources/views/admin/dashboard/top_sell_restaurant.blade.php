<div class="col-lg-6 ml-0">
    <div class="col-md-6">
        <h5>Top Selling Restaurants</h5>
    </div>
    <div class="card">
        <div class="card-body">
            <div id="msgDiv"></div>
            <div class="table-responsive">
                <table class="table table-bordered color-table">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Restaurant Name</th>
                            <th>Total Sale</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($top_selling_restaurant) > 0 )
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($top_selling_restaurant as $item)
                        <tr> 
                            <td>{{$i++}}</td>
                            <td>{{$item->restaurant->lang[0]->name ?? ''}}</td>
                             <td>SAR {{$item->grand_total ?? ''}}</td>
                        </tr>
                        @endforeach
                            
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No records found!</td>
                        </tr>

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>