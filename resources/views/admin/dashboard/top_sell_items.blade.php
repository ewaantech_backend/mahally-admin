<div class="col-lg-6 ml-0">
    <div class="row"> 
        <div class="col-md-5">
            <h5>Top Selling Items</h5>
        </div>
        <div class="col-md-7 rest_sel" >
            <select class="form-control restaurant_top_selling"> 
                <option value="">By Restaurant</option>
                @foreach ($resturants as $item)
                    <option value="{{$item->id}}">{{$item->lang[0]->name}}</option> 
                @endforeach
            </select>
        </div>   
    </div>
    <div class="card">
        <div class="card-body">
            <div id="msgDiv"></div>
            <div class="table-responsive" id="top_selling_item">
                <table class="table table-bordered color-table">
                    <thead>
                        <tr>
                            <th>S. No.</th>
                            <th>Item Name</th>
                            <th>Restaurant Name</th>
                            <th class="left-align">Total Sale</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($top_selling_item) > 0 )
                        @php
                        $i = 1;
                        @endphp
                        @foreach ($top_selling_item as $item)
                        <tr> 
                            <td>{{$i++}}</td>
                            <td>{{$item->menu_items->lang[0]->name ?? ''}}</td>
                            <td>{{$item->order->restaurant->lang[0]->name ?? ''}}</td>
                             <td>{{$item->item_count ?? ''}}</td>
                        </tr>
                        @endforeach
                            
                        @else
                        <tr>
                            <td colspan="8" class="text-center">No records found!</td>
                        </tr>

                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@push('css')
<style>
    .rest_sel{
        margin-top: -0.77rem;
    }
</style>
@endpush
@push('scripts')
<script>
    $('.restaurant_top_selling').on('change',function(){
     var rest_id = $(this).val();
    $.ajax({
    type: "POST",
        url: "{{route('dashboard.getTopSellingItems')}}",
        data:{
            rest_id:rest_id,
        },
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
             $("#top_selling_item").html(data);
        }
    });
 });
</script>
  @endpush