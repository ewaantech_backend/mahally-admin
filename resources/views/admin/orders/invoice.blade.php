<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
   </head>
   <table  bgcolor="#ffffff" cellpadding="0"  cellspacing="0">
            <tbody>
               <tr>
                  <td  width="77%" >
                     <h4  face="arial" style="font-size: 20px; color:#716e6e;"> {{ __('messages.orders.order_info.order_details') }}</h4>
                  </td>
               </tr>
            </tbody>
         </table>
         <table width="100%"  bgcolor="#ffffff" cellpadding="0"  cellspacing="0" >
            <tbody>
               <tr>
                 <td width="33%">
                     <b>{{ __('messages.orders.order_info.order_id') }}</b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{ $order['order_id'] }}</p>
               </td>
                <td width="33%">
                    <b>{{ __('messages.orders.order_info.order_date') }} </b> 
                     <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">  {{ \Carbon\Carbon::parse($order['created_at'])->format('d-m-Y')}}</p>
                </td>
                 <td width="33%">
                     <b>{{ __('messages.orders.order_info.payment_type') }} </b> 
                    <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{ $order['payment_mode'] }}</p>
                </td>
            </tr>
            <tr>
                <td width="33%">
                     <b>{{ __('messages.orders.order_info.customer') }} </b> 
                      <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{ $order->customer[0]->name }}</p>
                </td>
                <td width="33%">
                     <b>{{ __('messages.orders.order_info.status') }}</b> 
                      <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{ $order_status[$order->order_status] }}</p>
                </td>
                <td width="33%">
                     <b>{{ __('messages.orders.order_info.status') }}</b> 
                      <p style="margin-top: 2px;
                        margin-bottom: 20px;
                        font-size: 14px;">{{ $order_status[$order->order_status] }}</p>
                </td>
            </tr>
             </tbody>
         </table>
         <table width="100%"  bgcolor="#ffffff" cellpadding="0"  cellspacing="0" >
            <tbody>
               <tr>
                  <td height="20 "colspan="4"
                     style="border-top:0;border-right:0;border-bottom:1px solid #aaa;border-left:0;">
                  </td>
               </tr>
            </tbody>
         </table>
        <table width="100%" bgcolor="#ffffff" cellpadding="0"  cellspacing="0">
            <thead>
               <tr face="arial">
                  <td width="15%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500; ">{{ __('messages.orders.order_info.item') }}</td>
                <td width="15%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500; ">{{ __('messages.orders.order_info.quantity') }}</td>
                </td>
                <td width="15%" style="padding-bottom: 10px;font-size: 16px;
                     font-weight: 500; ">{{ __('messages.orders.order_info.price') }}</td>
                </td>
            </tr>
            @php 
                $total = 0;
                $discount = 0;
            @endphp

            @foreach ($order->order_item as $item)
                @if($item->menu_items)
                <tr>
                    <td width="15%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">{{$item->menu_items->lang[0]->name}}</td>
                    <td width="15%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">{{$item->item_count}}</td>
                    <td width="15%" style="padding-bottom: 10px;font-size: 14px;
                     font-weight: 500; ">
                        {{$item->menu_items->price}}
                        @if($item->menu_items->price)
                           @php 
                                 $total =  $total+ $item->menu_items->price;
                           @endphp
                        @endif
                        @if($item->menu_items->discount_price)
                           @php 
                                 $discount =  $discount+ $item->menu_items->discount_price;
                           @endphp
                        @endif
                    </td>
                </tr>
                @endif 
                @if($item->order_addons)
                <tr>
                    <td style="width: 10%"></td>
                    <td>
                        <span>{{$item->order_addons->resturant_addons->lang[0]->name}}</span>
                    </td>
                    <td>
                        {{$item->order_addons->item_count}}
                    </td>
                    <td>
                        {{$item->order_addons->resturant_addons->price}}
                        @if($item->order_addons->resturant_addons->price)
                           @php 
                                 $total =  $total+ $item->order_addons->resturant_addons->price;
                           @endphp
                        @endif
                        @if($item->order_addons->resturant_addons->discount_price)
                           @php 
                                 $discount =  $discount+ $item->order_addons->resturant_addons->discount_price;
                           @endphp
                        @endif
                    </td>
                </tr>
                @endif
            @endforeach
            <tr>
                   <td colspan="5"><hr></td>
               </tr>
            <tr>
                <td style="width: 30%"></td>
                <td>{{ __('messages.orders.order_info.subtotal') }}</td>
                <td>SAR {{$total }}</td>
            </tr>
            <tr>
                <td style="width: 30%"></td>
                <td>{{ __('messages.orders.order_info.discount') }}</td>
                <td>SAR {{$discount }}</td>
            </tr>
            <tr>
                <td style="width: 30%"></td>
                <td style="width: 50%"><hr></td>
            </tr>
            <tr>
                  <td colspan="1"></td>
                  <td style="padding-top: 10px;font-size: 16px;
                     font-weight: 500; "><strong>
                          {{ __('messages.orders.order_info.total_amt') }}</strong>
                </td>
                 <td style="padding-top: 10px;font-size: 18px;
                     font-weight: 500; "><b>SAR {{$total-$discount }}</b>
                </td>
            </tr>
       </table>       
   </body>
</html>