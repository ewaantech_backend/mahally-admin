@extends('layouts.master')

@section('content-title')
NOTIFICATIONS
@endsection
@section('content')
@include('user.flash-message')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <form method="POST" id="admin_notifications" action="{{route('admin-notification.store')}}">
                    @csrf
                    <div class="row mb-3">
                        <div class="col-md-6">
                            <label>Title (EN)</label>
                            <input class="form-control" name="title_en" placeholder="Enter Title" />
                            @error('title_en')
                            <span class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 arabic_label">
                            <label>Title (AR)</label>
                            <input class="form-control" name="title_ar" style="text-align:right !important" placeholder="أدخل الشروط" />
                            @error('title_ar')
                            <span class="error">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 english_label">
                            <label>Notification (EN)</label>
                            <textarea class="form-control area" id="notification_en" name="notification_en"></textarea>
                            @error('notification_en')
                            <span class="error">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="col-md-6 arabic_label">
                            <label>Notification (AR)</label>
                            <textarea class="form-control area" id="notification_ar" name="notification_ar" style="text-align:right !important"></textarea>
                            @error('notification_ar')
                            <span class="error">{{$message}}</span>
                            @enderror
                        </div>

                        {{-- </div> --}}
                        <div class="col-lg-12">
                            <div class="row justify-content-end pb-5">
                                <div class="col-md-12 filter-by-boat-jalbooth-butn01 text-md-left">
                                    <button type="submit" class="btn btn-info waves-effect waves-light">
                                        Send
                                    </button>
                                    <a href="{{route('admin-notification.get')}}" class="btn btn-default waves-effect">
                                        Reset
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <form id="search_form" action="{{route('admin-notification.get')}}" method="get">
                    <div class="row">

                        <div class="col-md-8" id="select_class">

                            <input type="text" class="form-control" id="search_note" value="{{$search}}" name="search_note" placeholder="Search for notification ">
                        </div>

                        <div class="col-md-4 add-jalbooth-butn-main text-left">
                            <button class="btn btn-info" id="search_datatable">
                                Search
                            </button>
                            <a href="{{route('admin-notification.get')}}" class="btn btn-default reset_style" id="reset_datatable">
                                Reset
                            </a>
                        </div>

                    </div>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>

                                <th width="25%">Title (EN) / Title (AR)</th>
                                <th width="25%">Notification (EN)</th>
                                <th width="25%">Notification (AR)</th>
                                <th width="15%">Sent At</th>
                                <th width="10%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($notification) > 0)
                            @php
                            $i = 1;
                            @endphp
                            @foreach ($notification as $row_data)
                            <tr>
                                {{-- <th>{{ $i++ }}</th> --}}
                                <td>{{ $row_data->lang[0]->title ?? '' }} / {{ $row_data->lang[1]->title ?? '' }}</td>
                                <td>{!! \Illuminate\Support\Str::limit($row_data->lang[0]->content ?? '', 100, '...') !!}</td>                                <td class="right-align">{!! \Illuminate\Support\Str::limit($row_data->lang[1]->content ?? '', 100, '...') !!}</td>
                                
                                <td>{{ \Carbon\Carbon::parse($row_data->created_at)->format('d-m-Y h:i A')}}</td>
                                <td class="text-center">
                                    <a class="btn btn-sm btn-danger text-white delete_notification" data-id="{{ $row_data->id }}" title="Delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="8" class="text-center">No records found!</td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
                <div class="text-center d-flex justify-content-center mt-3">
                    {{ $notification->links() }}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@push('css')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style>
    input[type=checkbox],
    input[type=radio] {
        /* box-sizing: border-box;
        padding: 0;
        margin: 0 10px 0 5px; */
        margin: 10px;
    }

    .area {
        height: 120px;
    }

    .error {
        color: red;
    }

    .reset_style {
        margin-left: 10px;
    }
</style>
@endpush
@push('scripts')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    })
    $('.delete_notification').on('click', function(e) {
        var id = $(this).data("id");
            $.confirm({
            title: false,
            content: 'Are you sure to delete this Notification? <br><br>You wont be able to revert this',
            buttons: {
                Yes: function() {
                    $.ajax({
                        url: "{{route('admin-notification.delete')}}",
                        type: 'POST',
                        data: {

                            id: id
                        },
                        success: function(data) {
                            if (data.status == 1) {
                                window.setTimeout(function() {
                                    window.location.href = '{{route("admin-notification.get")}}';
                                }, 1000);
                                Toast.fire({
                                    icon: 'success',
                                    title: data.message
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: data.message
                                });
                            }
                        }
                    });
                },
                No: function() {
                    console.log('cancelled');
                }
            }
        });
    });

    $("#search_note").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "{{route('admin-notification.get')}}",
                method: 'post',
                data: {
                    search: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        minLength: 1,
        select: function(event, ui) {
            $('#search_note').val(ui.item.label); // display the selected text
            // $('#cancellation_search').val(ui.item.value); // save selected id to input
            return false;
        }
    });
        $("#admin_notifications").validate({
            rules: {
                title_en: {        
                    required: true,         
                },
                title_ar: {          
                    required: true,
                },
                notification_ar: {        
                    required: true,  
                       
                },
                notification_en: {        
                    required: true,         
                },
                // location: {        
                //     required: true,         
                // },
            },
            messages: {               
                title_en: {
                      required:"Title (EN) Required",
                      },
               
                title_ar: {
                      required: "Title (Ar) Required",
                     
                      },
                notification_en: {
                      required: "Notification (EN) Required",
                      },
                notification_ar: {
                      required: "Notification (AR) Required",
                      },
                // location: {
                //       required: "Location Required",
                //       }
            },
           

    });
</script>
@endpush